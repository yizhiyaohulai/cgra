Cgra CMake build
====
This directory contains CMake files for building Cgra on Microsoft
Windows. [CMake](https://cmake.org) is a cross-platform tool that can
generate build scripts for multiple build systems, including Microsoft
Visual Studio.

**N.B.** We provide Linux build instructions primarily for the purpose of
testing the build. We recommend using the standard Bazel-based build on
Linux.

## Building with CMake
The CMake files in this directory can build the core cgra runtime, an example C++ binary.
### Prerequisites<br>
* CMake version 3.5 or later.<br>
* Git<br>
* graphviz<br>

Step-by-step Windows build
==========================
1. Install the prerequisites detailed above, and set up your environment.

   * The following commands assume that you are using the Windows Command
     Prompt (`cmd.exe`). You will need to set up your environment to use the
     appropriate toolchain, i.e. the 64-bit tools. (Some of the binary targets
     we will build are too large for the 32-bit tools, and they will fail with
     out-of-memory errors.) The typical command to do set up your
     environment is:
     ```
     vs2015: "c:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
     vs2017: "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat"  x86_x64 -vcvars_ver=14.0
     ```
   * We assume that `cmake` and `git` are installed and in your `%PATH%`. If
     for example `cmake` is not in your path and it is installed in
     `C:\Program Files (x86)\CMake\bin\cmake.exe`, you can add this directory
     to your `%PATH%` as follows:

     ```
2. Clone the TensorFlow repository and create a working directory for your
   build:

   ```   
   D:\temp> git clone https://gitlab.com/yizhiyaohulai/cgra.git
   D:\temp> cd cgra\cgra\contrib\cmake
   D:\temp\tensorflow\tensorflow\contrib\cmake> mkdir build
   D:\temp\tensorflow\tensorflow\contrib\cmake> cd build
   D:\temp\tensorflow\tensorflow\contrib\cmake\build>
3. Invoke CMake to create Visual Studio solution and project files.

   **N.B.** This assumes that `cmake.exe` is in your `%PATH%` environment
   variable. The other paths are for illustrative purposes only, and may
   be different on your platform. The `^` character is a line continuation
   and must be the last character on each line.

   ```   
   D:\...\build> cmake .. -G "Visual Studio 14 2015" -Thost=x64  -A x64 -DCMAKE_BUILD_TYPE=Debug ^
   -Dcgra_ENABLE_DISTRIBUTED=OFF
#ifndef CGRA_CORE_DIRECT_SESSION_H
#define CGRA_CORE_DIRECT_SESSION_H

#include "cgra/core/framework/session_factory.h"
#include "cgra/core/framework/session.h"
#include "cgra/core/framework/session_option.h"

namespace cgra {

class DirectSession : public Session {

public:
	explicit DirectSession(SessionOptions& option);
	~DirectSession() = default;
	void Run();

private:
	SessionOptions option;
};

}  //namespace cgra

#endif

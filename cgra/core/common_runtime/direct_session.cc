#include "cgra/core/common_runtime/direct_session.h"
#include "cgra/core/framework/session_factory.h"
#include "cgra/core/graph/dot_parser.h"
#include "cgra/core/ilp/gurobi.h"
#include "cgra/core/lib/core/errors.h"
#include "cgra/core/platform/default/mutex.h"
#include <string>
#include <tinyprocess/install/include/process.hpp>

namespace cgra {

std::vector<std::string> split(std::string strtem, char a) {
  std::vector<std::string> strvec;

  std::string::size_type pos1, pos2;
  pos2 = strtem.find(a);
  pos1 = 0;
  while (std::string::npos != pos2) {
    strvec.push_back(strtem.substr(pos1, pos2 - pos1));

    pos1 = pos2 + 1;
    pos2 = strtem.find(a, pos1);
  }
  strvec.push_back(strtem.substr(pos1));
  return strvec;
}

class DirectSessionFactory : public SessionFactory {
public:
  explicit DirectSessionFactory() {}

  Session *NewSession(SessionOptions &option) override {
    static DirectSession *session = new DirectSession(option);
    {
      mutex_lock l(sessions_lock_);
      sessions_.push_back(session);
    }
    return session;
  }

  bool AcceptOption(SessionOptions &option) override {
    return option.type.empty();
  }

  void Deregister(const DirectSession *session) {
    mutex_lock l(sessions_lock_);
    sessions_.erase(std::remove(sessions_.begin(), sessions_.end(), session),
                    sessions_.end());
  }

private:
  mutex sessions_lock_;
  std::vector<DirectSession *> sessions_ GUARDED_BY(sessions_lock_);
};

class DirectSessionRegistrar {
public:
  DirectSessionRegistrar() {
    SessionFactory::Register("DIRECT_SESSION", new DirectSessionFactory());
  }
};
static DirectSessionRegistrar registrar;

DirectSession::DirectSession(SessionOptions &option) : option(option) {}

/*
        This Function sequential execution is  important
        first convert LLVM code to cgra code
        second delete non operation
        third remove necessary leaf node and extract Loop Info
*/
void DirectSession::Run() {

  // read xml file and analyze file, transfor cgra architectur file

  // optimize intermediate expression dfg file,export compress dfg file
  auto accelerate_num = option.config.accelerate_num();
  for (int i = 0; i < accelerate_num; i++) {
    std::unique_ptr<DotParser> parser(new DotParser);
    std::unique_ptr<Graph> graph =
        parser->ParseGraph(option.config.ir_dfg_file(i));
    parser->ConvertToCgraCode(graph);
    parser->ReplaceIcmpToSub(graph);
    parser->AccelerateInvailedOp(graph);
    parser->RemoveUnnecessaryLeafNodes(graph);
    parser->RemovePhiNodes(graph);
    parser->RemoveUnnecessaryLeafNodes(graph);
    parser->MergeSimilarConstStructs(graph);
    parser->ExtractConstImmediateValue(graph);
    parser->MergeRedundancyOp(graph);
    parser->UnfoldImmediateValue(graph);
    // parser->PrintNodeParameter(graph);
    // store external accelerate infor to json file to esay load
    parser->StoreJsonFile(graph, option.config.code_dir(), i + 1);
    auto dfg_file = option.config.dfg_file(i);
    parser->PrintDfgFileSystem(graph, dfg_file);

    using namespace TinyProcessLib;
    std::vector<std::pair<int, int>> mapper_f_result;
    std::vector<std::pair<int, int>> mapper_r_result;

    std::string mapper_name =
        "python ../../../mapper/fixed_mapping.py " +
        option.config.dfg_file(i);
    Process process2(
        mapper_name, "",
        [&mapper_f_result, &mapper_r_result](const char *bytes, size_t n) {
          auto string_n = std::string(bytes, n);
          std::cout << string_n << std::endl;
          if (string_n.find("F,") != std::string::npos) {
            auto split_str = cgra::split(string_n, '\n');
            std::for_each(
                split_str.begin(), split_str.end(),
                [&mapper_f_result, &mapper_r_result](std::string str) {
                  auto mapper_str = cgra::split(str, ',');
                  auto size = mapper_str.size();
                  if (size == 3) {
                    std::string::size_type sz;
                    int first = std::stoi(mapper_str[1], &sz);
                    int second = std::stoi(mapper_str[2], &sz);
                    if (mapper_str[0] == "F")
                      mapper_f_result.push_back(std::make_pair(first, second));
                    else if (mapper_str[0] == "R")
                      mapper_r_result.push_back(std::make_pair(first, second));
                  }
                });
          }
        },
        [](const char *, size_t) {}, true);
    auto exit_status = process2.get_exit_status();

    std::shared_ptr<GurobiMapper> gurobi_mapper =
        std::make_shared<GurobiMapper>(graph);
    gurobi_mapper->StoreMapperJsonFile(option.config.code_dir(), i + 1,
                                       mapper_f_result, mapper_r_result);

    std::string bit_name =
        option.config.code_dir() + "/input" + std::to_string(i + 1) + ".txt";
    std::string process_bit_cmd =
        "python ../../../mapper/code_emssion.py " +
        option.config.code_dir() + "> " + bit_name;

    Process process3(process_bit_cmd, "",
                     [](const char *bytes, size_t n) {
                       std::cout << std::string(bytes, n) << std::endl;
                     },
                     [](const char *, size_t) {}, true);
    exit_status = process3.get_exit_status();

    std::string assembly_name =
        "python ../../../assembly2machine/makefile.py " +
        bit_name + " " + option.config.code_dir() + "/data" +
        std::to_string(i + 1) + ".bin" + " " + "../../../assembly2machine";
    //std::cout << assembly_name << std::endl;

    Process process4(assembly_name, "",
        [](const char *bytes, size_t n) {
          std::cout << std::string(bytes, n) << std::endl;
        },
        [](const char *, size_t) {}, true);
    exit_status = process4.get_exit_status();
    std::cout << "Finish And Exit" << std::endl;
  }
}

} // namespace cgra

#ifndef CGRA_CORE_LIB_CORE_THREADPOOL_H_
#define CGRA_CORE_LIB_CORE_THREADPOOL_H_

#include <functional>
#include <memory>
#include "cgra/core/platform/default/integral_types.h"
#include "cgra/core/platform/env.h"

namespace cgra {
namespace thread {

class ThreadPool {
 public:
  ThreadPool(Env* env, const std::string& name, int num_threads);
  ThreadPool(Env* env, const std::string& name, int num_threads, bool low_latency_hint);
  ~ThreadPool();
  void Schedule(std::function<void()> fn);
  void ParallelFor(int64 total, int64 cost_per_unit,
                   std::function<void(int64, int64)> fn);
  int NumThreads() const;
  int CurrentThreadId() const;
  struct Impl;

 private:
  std::unique_ptr<Impl> impl_;
  void ParallelForWithWorkerId(
      int64 total, int64 cost_per_unit,
      const std::function<void(int64, int64, int)>& fn);
};

}  // namespace thread
}  // namespace cgra
#endif  // CGRA_CORE_LIB_CORE_THREADPOOL_H_

#ifndef CGRA_CORE_LIB_CORE_SIMPLETHREADPOOL_H
#define CGRA_CORE_LIB_CORE_SIMPLETHREADPOOL_H

#include <deque>
#include <mutex>
#include <vector>
#include "cgra/core/lib/core/threadenvironment.h"
#include "cgra/core/lib/core/threadpoolinterface.h"

namespace cgra {

template <typename Environment>
class SimpleThreadPoolTempl : public ThreadPoolInterface {
 public:
  // Construct a pool that contains "num_threads" threads.
  explicit SimpleThreadPoolTempl(int num_threads,
                                 Environment env = Environment())
      : env_(env), threads_(num_threads), waiters_(num_threads) {
    for (int i = 0; i < num_threads; i++) {
      threads_.push_back(env.CreateThread([this, i]() { WorkerLoop(i); }));
    }
  }

  // Wait until all scheduled work has finished and then destroy the
  // set of threads.
  ~SimpleThreadPoolTempl() {
    {  // Wait for all work to get done.
      std::unique_lock<std::mutex> l(mu_);
      while (!pending_.empty()) {
        empty_.wait(l);
      }
      exiting_ = true;

      // Wakeup all waiters.
      for (auto w : waiters_) {
        w->ready = true;
        w->task.f = nullptr;
        w->cv.notify_one();
      }
    }
    // Wait for threads to finish.
    for (auto t : threads_) {
      delete t;
    }
  }

  // Schedule fn() for execution in the pool of threads. The functions are
  // executed in the order in which they are scheduled.
  void Schedule(std::function<void()> fn) final {
    Task t = env_.CreateTask(std::move(fn));
    std::unique_lock<std::mutex> l(mu_);
    if (waiters_.empty()) {
      pending_.push_back(std::move(t));
    } else {
      Waiter* w = waiters_.back();
      waiters_.pop_back();
      w->ready = true;
      w->task = std::move(t);
      w->cv.notify_one();
    }
  }

  void Cancel() {
    for (size_t i = 0; i < threads_.size(); i++) {
      threads_[i]->OnCancel();
    }
  }

  int NumThreads() const final {
	  return static_cast<int>(threads_.size());
  }

  int CurrentThreadId() const final {
    const PerThread* pt = this->GetPerThread();
    if (pt->pool == this) {
      return pt->thread_id;
    } else {
      return -1;
    }
  }

 protected:
  void WorkerLoop(int thread_id) {
    std::unique_lock<std::mutex> l(mu_);
    PerThread* pt = GetPerThread();
    pt->pool = this;
    pt->thread_id = thread_id;
    Waiter w;
    Task t;
    while (!exiting_) {
      if (pending_.empty()) {
        w.ready = false;
        waiters_.push_back(&w);
        while (!w.ready) {
          w.cv.wait(l);
        }
        t = w.task;
        w.task.f = nullptr;
      } else {
        // Pick up pending work
        t = std::move(pending_.front());
        pending_.pop_front();
        if (pending_.empty()) {
          empty_.notify_all();
        }
      }
      if (t.f) {
        mu_.unlock();
        env_.ExecuteTask(t);
        t.f = nullptr;
        mu_.lock();
      }
    }
  }

 private:
  typedef typename Environment::Task Task;
  typedef typename Environment::EnvThread Thread;
  struct Waiter {
    std::condition_variable cv;
    Task task;
    bool ready;
  };
  struct PerThread {
    constexpr PerThread() : pool(NULL), thread_id(-1){};
    SimpleThreadPoolTempl* pool;
    int thread_id;
  };
  Environment env_;
  std::mutex mu_;
  std::vector<Thread*> threads_;
  std::vector<Waiter*> waiters_;
  std::deque<Task> pending_;
  std::condition_variable empty_;
  bool exiting_ = false;

  PerThread* GetPerThread() const {
    static thread_local PerThread per_thread;
    return &per_thread;
  }
};

typedef SimpleThreadPoolTempl<StlThreadEnvironment> SimpleThreadPool;

}  // NAMESPACE CGRA
#endif  // CGRA_CORE_LIB_CORE_SIMPLETHREADPOOL_H
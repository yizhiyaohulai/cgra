#include <assert.h>
#include <stdio.h>
#include "cgra/core/lib/core/status.h"

namespace cgra {

Status::Status(cgra::error::Code code, std::string msg) {
  assert(code != cgra::error::OK);
  state_ = std::unique_ptr<State>(new State);
  state_->code = code;
  state_->msg = msg;
}

void Status::Update(const Status& new_status) {
  if (ok()) {
    *this = new_status;
  }
}

void Status::SlowCopyFrom(const State* src) {
  if (src == nullptr) {
    state_ = nullptr;
  } else {
    state_ = std::unique_ptr<State>(new State(*src));
  }
}

const std::string& Status::empty_string() {
  static std::string* empty = new std::string;
  return *empty;
}

std::string Status::ToString() const {
  if (state_ == nullptr) {
    return "OK";
  } else {
    char tmp[30];
    const char* type;
    switch (code()) {
      case cgra::error::CANCELLED:
        type = "Cancelled";
        break;
      case cgra::error::UNKNOWN:
        type = "Unknown";
        break;
      case cgra::error::INVALID_ARGUMENT:
        type = "Invalid argument";
        break;
      case cgra::error::DEADLINE_EXCEEDED:
        type = "Deadline exceeded";
        break;
      case cgra::error::NOT_FOUND:
        type = "Not found";
        break;
      case cgra::error::ALREADY_EXISTS:
        type = "Already exists";
        break;
      case cgra::error::PERMISSION_DENIED:
        type = "Permission denied";
        break;
      case cgra::error::UNAUTHENTICATED:
        type = "Unauthenticated";
        break;
      case cgra::error::RESOURCE_EXHAUSTED:
        type = "Resource exhausted";
        break;
      case cgra::error::FAILED_PRECONDITION:
        type = "Failed precondition";
        break;
      case cgra::error::ABORTED:
        type = "Aborted";
        break;
      case cgra::error::OUT_OF_RANGE:
        type = "Out of range";
        break;
      case cgra::error::UNIMPLEMENTED:
        type = "Unimplemented";
        break;
      case cgra::error::INTERNAL:
        type = "Internal";
        break;
      case cgra::error::UNAVAILABLE:
        type = "Unavailable";
        break;
      case cgra::error::DATA_LOSS:
        type = "Data loss";
        break;
      default:
        snprintf(tmp, sizeof(tmp), "Unknown code(%d)",
                 static_cast<int>(code()));
        type = tmp;
        break;
    }
    std::string result(type);
    result += ": ";
    result += state_->msg;
    return result;
  }
}

void Status::IgnoreError() const {
  // no-op
}

std::ostream& operator<<(std::ostream& os, const Status& x) {
  os << x.ToString();
  return os;
}

std::string* CgraCheckOpHelperOutOfLine(const ::cgra::Status& v,
                                        const char* msg) {
  std::string r("Non-OK-status: ");
  r += msg;
  r += " status: ";
  r += v.ToString();
  // Leaks string but this is only to be used in a fatal error message
  return new std::string(r);
}

}  // namespace tensorflow

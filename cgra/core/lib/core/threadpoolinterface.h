#ifndef CORE_PLATFORM_THREADPOOL_THREADPOOLINTERFACE_H
#define CORE_PLATFORM_THREADPOOL_THREADPOOLINTERFACE_H

#include <functional>
namespace cgra {

class ThreadPoolInterface {
 public:
  virtual void Schedule(std::function<void()> fn) = 0;
  virtual void Cancel(){};
  // Returns the number of threads in the pool.
  virtual int NumThreads() const = 0;
  virtual int CurrentThreadId() const = 0;
  virtual ~ThreadPoolInterface(){};
};

};  // namespace GReP

#endif  // CORE_PLATFORM_THREADPOOL_THREADPOOLINTERFACE_H

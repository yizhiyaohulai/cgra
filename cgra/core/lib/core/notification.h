#ifndef CGRA_CORE_PLATFORM_DEFAULT_NOTIFICATION_H_
#define CGRA_CORE_PLATFORM_DEFAULT_NOTIFICATION_H_

#include <assert.h>
#include <atomic>

#include "cgra/core/platform/default/integral_types.h"
#include "cgra/core/platform/default/mutex.h"

namespace cgra {

class Notification {
 public:
  Notification() : notified_(0){};
  ~Notification() { mutex_lock l(mu_); }

  void Notify() {
    mutex_lock l(mu_);
    assert(!HasBeenNotified());
    notified_.store(true, std::memory_order_release);
    cv_.notify_all();
  }

  bool HasBeenNotified() { return notified_.load(std::memory_order_acquire); }

  void WaitForNotification() {
    if (!HasBeenNotified()) {
      mutex_lock l(mu_);
      while (!HasBeenNotified()) {
        cv_.wait(l);
      }
    }
  }

 private:
  friend bool WaitForNotificationWithTimeout(Notification* n,
                                             int64 timeout_in_us);
  bool WaitForNotificationWithTimeout(int64 timeout_in_us) {
    bool notified = HasBeenNotified();
    if (!notified) {
      mutex_lock l(mu_);
      do {
        notified = HasBeenNotified();
      } while (!notified &&
               cv_.wait_for(l, std::chrono::microseconds(timeout_in_us)) !=
                   std::cv_status::timeout);
    }
    return notified;
  }

  mutex mu_;
  condition_variable cv_;
  std::atomic<bool> notified_;
};

inline bool WaitForNotificationWithTimeout(Notification* n,
                                           int64 timeout_in_us) {
  return n->WaitForNotificationWithTimeout(timeout_in_us);
}
};

#endif

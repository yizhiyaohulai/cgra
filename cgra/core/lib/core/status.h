#ifndef CORE_LIB_CORE_STATUS_H_
#define CORE_LIB_CORE_STATUS_H_

#include <functional>
#include <iosfwd>
#include <memory>
#include <string>

#include "cgra/core/lib/core/error_codes.h"
#include "cgra/core/platform/default/logging.h"

namespace cgra {

class Status {
 public:
  Status(){};
  Status(cgra::error::Code code, std::string msg);
  Status(const Status& s);
  void operator=(const Status& s);
  static Status OK() { return Status(); };
  bool ok() const { return (state_ == NULL); };

  cgra::error::Code code() const {
    return ok() ? cgra::error::OK : state_->code;
  }

  const std::string& error_message() const {
    return ok() ? empty_string() : state_->msg;
  }

  bool operator==(const Status& x) const;
  bool operator!=(const Status& x) const;
  void Update(const Status& new_status);
  std::string ToString() const;
  void IgnoreError() const;

 private:
  static const std::string& empty_string();
  struct State {
    cgra::error::Code code;
    std::string msg;
  };
  std::unique_ptr<State> state_;
  void SlowCopyFrom(const State* src);
};

inline Status::Status(const Status& s)
    : state_((s.state_ == NULL) ? NULL : new State(*s.state_)) {}

inline void Status::operator=(const Status& s) {
  if (state_ != s.state_) {
    SlowCopyFrom(s.state_.get());
  }
}

inline bool Status::operator==(const Status& x) const {
  return (this->state_ == x.state_) || (ToString() == x.ToString());
}

inline bool Status::operator!=(const Status& x) const { return !(*this == x); }
std::ostream& operator<<(std::ostream& os, const Status& x);
typedef std::function<void(const Status&)> StatusCallback;

#define CGRA_DO_CHECK_OK(val, level)                                \
  while (auto _result = ::cgra::CgraCheckOpHelper(val, #val)) \
  LOG(level) << *(_result)

#define CGRA_CHECK_OK(val)  CGRA_DO_CHECK_OK(val, FATAL)
#define CGRA_QCHECK_OK(val) CGRA_DO_CHECK_OK(val, QFATAL)

}  // namespace cgra

#endif  // CORE_LIB_CORE_STATUS_H_

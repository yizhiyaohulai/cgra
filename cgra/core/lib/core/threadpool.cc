#include "cgra/core/lib/core/threadpool.h"
#include <thread>

#include "cgra/core/lib/core/nonblockingthreadpool.h"

namespace cgra {

template <typename Env>
using ThreadPoolTempl = NonBlockingThreadPoolTempl<Env>;

namespace thread {

struct SystemEnvironment {
  typedef Thread EnvThread;
  struct TaskImpl {
    std::function<void()> f;
  };
  struct Task {
    std::unique_ptr<TaskImpl> f;
  };

  Env* const env_;
  const std::string name_;

  SystemEnvironment(Env* env, const std::string& name) : 
	  env_(env),name_(name) {}

  EnvThread* CreateThread(std::function<void()> f) {
	  return env_->StartThread(name_, [=] {
		  f();
	  });
  }

  Task CreateTask(std::function<void()> f) {
    uint64 id = 0;
    return Task{std::unique_ptr<TaskImpl>(new TaskImpl{std::move(f)})};
  }

  void ExecuteTask(const Task& t) { t.f->f(); }
};

struct ThreadPool::Impl : ThreadPoolTempl<SystemEnvironment> {
  Impl(Env* env, const std::string& name, int num_threads, bool low_latency_hint)
      : ThreadPoolTempl<SystemEnvironment>(num_threads, low_latency_hint,
		  SystemEnvironment(env, name)) {}

  void ParallelFor(int64 total, int64 cost_per_unit,
                   std::function<void(int64, int64)> fn) {}
};

ThreadPool::ThreadPool(Env* env, const std::string& name, int num_threads)
    : ThreadPool(env, name, num_threads, true) {}

ThreadPool::ThreadPool(Env* env, const std::string& name, int num_threads,
                       bool low_latency_hint) {
  impl_.reset(
      new ThreadPool::Impl(env, "cgra_" + name, num_threads, low_latency_hint));
}

ThreadPool::~ThreadPool() {}

void ThreadPool::Schedule(std::function<void()> fn) {
  impl_->Schedule(std::move(fn));
}

void ThreadPool::ParallelFor(int64 total, int64 cost_per_unit,
                             std::function<void(int64, int64)> fn) {
  impl_->ParallelFor(total, cost_per_unit, std::move(fn));
}

void ThreadPool::ParallelForWithWorkerId(
    int64 total, int64 cost_per_unit,
    const std::function<void(int64, int64, int)>& fn) {
  impl_->ParallelFor(total, cost_per_unit,
                     [this, &fn](int64 start, int64 limit) {
                       // ParallelFor may use the current thread to do some
                       // work synchronously. When calling CurrentThreadId()
                       // from outside of the thread pool, we get -1, so we can
                       // shift every id up by 1.
                       int id = CurrentThreadId() + 1;
                       fn(start, limit, id);
                     });
}

int ThreadPool::NumThreads() const { return impl_->NumThreads(); }

int ThreadPool::CurrentThreadId() const { return impl_->CurrentThreadId(); }

}  // namespace threadpool

}  // namespace cgra

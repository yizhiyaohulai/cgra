#ifndef CGRA_CORE_LIB_CORE_ERRORS_H_
#define CGRA_CORE_LIB_CORE_ERRORS_H_

#include <sstream>

#include "cgra/core/lib/core/status.h"
#include "cgra/core/lib/strings/strcat.h"
#include "cgra/core/platform/logging.h"
#include "cgra/core/platform/macros.h"

namespace cgra {
namespace errors {

typedef ::cgra::error::Code Code;

namespace internal {

template <typename T>
typename std::enable_if<!std::is_convertible<T, strings::AlphaNum>::value,
                        string>::type
PrepareForStrCat(const T& t) {
  std::stringstream ss;
  ss << t;
  return ss.str();
}
inline const strings::AlphaNum& PrepareForStrCat(const strings::AlphaNum& a) {
  return a;
}

}  // namespace internal

// Append some context to an error message.  Each time we append
// context put it on a new line, since it is possible for there
// to be several layers of additional context.
template <typename... Args>
void AppendToMessage(::cgra::Status* status, Args... args) {
  *status = ::cgra::Status(
      status->code(),
      ::cgra::strings::StrCat(status->error_message(), "\n\t", args...));
}

// For propagating errors when calling a function.
#define CGRA_RETURN_IF_ERROR(...)                          \
  do {                                                   \
    const ::cgra::Status _status = (__VA_ARGS__);  \
    if (CGRA_PREDICT_FALSE(!_status.ok())) return _status; \
  } while (0)

#define CGRA_RETURN_WITH_CONTEXT_IF_ERROR(expr, ...)                  \
  do {                                                              \
    ::cgra::Status _status = (expr);                          \
    if (CGRA_PREDICT_FALSE(!_status.ok())) {                          \
      ::cgra::errors::AppendToMessage(&_status, __VA_ARGS__); \
      return _status;                                               \
    }                                                               \
  } while (0)

// Convenience functions for generating and using error status.
// Example usage:
//   status.Update(errors::InvalidArgument("The ", foo, " isn't right."));
//   if (errors::IsInvalidArgument(status)) { ... }
//   switch (status.code()) { case error::INVALID_ARGUMENT: ... }

#define DECLARE_ERROR(FUNC, CONST)                                       \
  template <typename... Args>                                            \
  ::cgra::Status FUNC(Args... args) {                              \
    return ::cgra::Status(                                         \
        ::cgra::error::CONST,                                      \
        ::cgra::strings::StrCat(                                   \
            ::cgra::errors::internal::PrepareForStrCat(args)...)); \
  }                                                                      \
  inline bool Is##FUNC(const ::cgra::Status& status) {             \
    return status.code() == ::cgra::error::CONST;                  \
  }

DECLARE_ERROR(Cancelled, CANCELLED)
DECLARE_ERROR(InvalidArgument, INVALID_ARGUMENT)
DECLARE_ERROR(NotFound, NOT_FOUND)
DECLARE_ERROR(AlreadyExists, ALREADY_EXISTS)
DECLARE_ERROR(ResourceExhausted, RESOURCE_EXHAUSTED)
DECLARE_ERROR(Unavailable, UNAVAILABLE)
DECLARE_ERROR(FailedPrecondition, FAILED_PRECONDITION)
DECLARE_ERROR(OutOfRange, OUT_OF_RANGE)
DECLARE_ERROR(Unimplemented, UNIMPLEMENTED)
DECLARE_ERROR(Internal, INTERNAL)
DECLARE_ERROR(Aborted, ABORTED)
DECLARE_ERROR(DeadlineExceeded, DEADLINE_EXCEEDED)
DECLARE_ERROR(DataLoss, DATA_LOSS)
DECLARE_ERROR(Unknown, UNKNOWN)
DECLARE_ERROR(PermissionDenied, PERMISSION_DENIED)
DECLARE_ERROR(Unauthenticated, UNAUTHENTICATED)

#undef DECLARE_ERROR

// The CanonicalCode() for non-errors.
using ::cgra::error::OK;

}  // namespace errors
}  // namespace cgra

#endif  // CGRA_CORE_LIB_CORE_ERRORS_H_

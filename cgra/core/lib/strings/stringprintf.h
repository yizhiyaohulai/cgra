#ifndef CGRA_CORE_LIB_STRINGS_STRINGPRINTF_H_
#define CGRA_CORE_LIB_STRINGS_STRINGPRINTF_H_

#include <stdarg.h>
#include <string>

#include "cgra/core/platform/macros.h"
#include "cgra/core/platform/types.h"

namespace cgra {
namespace strings {

// Return a C++ string
extern std::string Printf(const char* format, ...)
    // Tell the compiler to do printf format string checking.
    CGRA_PRINTF_ATTRIBUTE(1, 2);

// Append result to a supplied string
extern void Appendf(std::string* dst, const char* format, ...)
    // Tell the compiler to do printf format string checking.
    CGRA_PRINTF_ATTRIBUTE(2, 3);

// Lower-level routine that takes a va_list and appends to a specified
// string.  All other routines are just convenience wrappers around it.
extern void Appendv(std::string* dst, const char* format, va_list ap);

}  // namespace strings
}  // namespace cgra

#endif  // CGRA_CORE_LIB_STRINGS_STRINGPRINTF_H_

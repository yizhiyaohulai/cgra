#include "cgra/core/ilp/gurobi.h"
#include "gurobi_c++.h"
#include <cmath>
#include <iostream>
#include <unordered_map>
#include <fstream>
//#include <json/install/include/nlohmann/json.hpp>
#include "cgra/core/lib/tools/json.hpp"
#include <iomanip>

#define CGRA_ROWS 8 //行
#define CGRA_COLS 8 //列
#define GLOBAL_REG_SIZE 16

using namespace std;
namespace cgra {

GurobiMapper::GurobiMapper(std::unique_ptr<Graph> &graph)
    : am_(CGRA_ROWS * CGRA_COLS), node_num(0) {

  MapperInit();
  GernerMapperMatrixMode(graph);
#if 0
  try {
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env);
    // Create variables
    GRBVar *F = model.addVars(node_num * matrix_num, GRB_BINARY);
    GRBVar *R =
        model.addVars(GLOBAL_REG_SIZE * node_num * node_num, GRB_BINARY);

    GRBLinExpr lhs_object;

    for (unsigned n = 0; n < GLOBAL_REG_SIZE; n++) {
      for (unsigned i = 0; i < node_num; i++) {
        for (unsigned j = 0; j < node_num; j++) {
          if (pe_graph_group[i][j])
            lhs_object += R[n * node_num * node_num + i * node_num + j];
        }
      }
    }
    model.setObjective(lhs_object, GRB_MINIMIZE);

    //model.setObjective(lhs_object = 1, GRB_MAXIMIZE);
    // 1
    for (unsigned i = 0; i < node_num; i++) {
      GRBLinExpr lhs_map = 0;
      for (unsigned j = 0; j < matrix_num; j++) {
        lhs_map += F[j + i * matrix_num];
      }
      model.addConstr(lhs_map == 1, "Map" + std::to_string(i));
    }
    // 2
    for (unsigned j = 0; j < matrix_num; j++) {
      GRBLinExpr lhs_pe = 0;
      for (unsigned i = 0; i < node_num; i++) {
        lhs_pe += F[j + i * matrix_num];
      }
      model.addConstr(lhs_pe <= 1, "PE" + std::to_string(j));
    }
    // 3
    for (unsigned i = 0; i < node_num; i++) {
      for (unsigned j = 0; j < node_num; j++) {
        if (pe_graph_group[i][j] == true) {
          GRBLinExpr lhs_interconnection = 0;
          for (unsigned a = 0; a < matrix_num; a++) {
            for (unsigned b = 0; b < matrix_num; b++) {
              if (cgra_[a][b] == true) {
                auto tmp = (F[i * matrix_num + a] * F[j * matrix_num + b])
                               .getLinExpr();
                lhs_interconnection += tmp;
              }
            }
          }

          for (unsigned n = 0; n < GLOBAL_REG_SIZE; n++)
            lhs_interconnection +=
                R[n * node_num * node_num + i * node_num + j];
          model.addConstr(lhs_interconnection == 1,
                          "Interconnection" + std::to_string(i * node_num + j));
        }
      }
    }

    // 4
    for (unsigned i = 0; i < node_num; i++) {
      if (pe_memory_group[i] == true) {
        GRBLinExpr lhs_mem = 0;
        for (unsigned j = 0; j < matrix_num; j++) {
          lhs_mem += am_[j] * F[j + i * matrix_num];
        }
        model.addConstr(lhs_mem == 1, "Mem" + std::to_string(i));
      }
    }
    // 5
    for (unsigned i = 0; i < node_num; i++) {
      for (unsigned j = 0; j < node_num; j++) {
       // if (pe_graph_group[i][j] == true) {
          GRBLinExpr lhs_edge = 0;
          for (unsigned n = 0; n < GLOBAL_REG_SIZE; n++) {
            lhs_edge += R[n * node_num * node_num + i * node_num + j];
          }
          model.addConstr(lhs_edge <= 1,
                          "Edge" + std::to_string(i * node_num + j));
      //  }
      }
    }

    // 6
    for (unsigned n = 0; n < GLOBAL_REG_SIZE; n++) {
      GRBLinExpr lhs_rr = 0;
      for (unsigned i = 0; i < node_num; i++) {
        for (unsigned j = 0; j < node_num; j++) {
        //  if (pe_graph_group[i][j] == true) {
            lhs_rr += R[n * node_num * node_num + i * node_num + j];
        //  }
        }
      }
      model.addConstr(lhs_rr <= 1.0, "RR" + std::to_string(n));
    }
    // char *home_file = getenv("GUROBI_HOME");
    // char *license_file = getenv("GRB_LICENSE_FILE");
    model.optimize();

    double ilp_runtime = model.get(GRB_DoubleAttr_Runtime);
    std::cout << "Gurobi Runtime: " << ilp_runtime << std::endl;

    int status = model.get(GRB_IntAttr_Status);
    GRBVar *vars = model.getVars();

    int num_vars = model.get(GRB_IntAttr_NumVars);
    std::cout << num_vars << std::endl;
    for (unsigned int i = 0; i < node_num * matrix_num; i++) {
      auto val = F[i].get(GRB_DoubleAttr_X);
      if (val == 1.0) {
        std::cout << "[" << i / matrix_num << "," << i % matrix_num  << "]"<< std::endl;
        //std::cout << load_nodes[i / matrix_num] << std::endl;
        mapper_result.push_back(
            std::make_tuple( i / matrix_num, load_nodes[i / matrix_num], i % matrix_num));
      }
    }

    std::cout << "--------------------------------" << std::endl;

    for (unsigned n = 0; n < GLOBAL_REG_SIZE; n++) {
      for (unsigned i = 0; i < node_num; i++) {
        for (unsigned j = 0; j < node_num; j++) {
          if (R[n * node_num * node_num + i * node_num + j].get(
                  GRB_DoubleAttr_X) == 1.0) {
            std::cout << "[" << n << "," << i << "," << j << "]" << std::endl;
            mapper_reg.push_back(std::make_tuple(n,i,j));
          }
        }
      }
    }
  } catch (GRBException e) {
    cout << "Error code = " << e.getErrorCode() << endl;
    cout << e.getMessage() << endl;
  } catch (...) {
    cout << "Exception during optimization" << endl;
  }
  std::cout << "mapping finish" << std::endl;
  std::cout << "checke it " << std::endl;
#endif
}

void GurobiMapper::MapperInit() {
  // We create load and store PE relationship
  unsigned h{0};
  for (unsigned w = 0; w < CGRA_COLS; w++)
    am_[w + h * CGRA_COLS] = true;

  for (h = 1; h < CGRA_ROWS - 1; h++) {
    for (unsigned w = 0; w < CGRA_COLS; w += (CGRA_COLS - 1)) {
      am_[w + h * CGRA_COLS] = true;
    }
  }

  for (unsigned w = 0; w < CGRA_COLS; w++)
    am_[w + h * CGRA_COLS] = true;

  // We create PE Mutual visiting relation
  std::vector<bool> info_;
  info_.resize(CGRA_COLS * CGRA_ROWS);
  for (unsigned i = 0; i < CGRA_COLS * CGRA_ROWS; i++)
    cgra_.push_back(info_);

  for (unsigned h = 0; h < CGRA_ROWS; h++)
    for (unsigned w = 0; w < CGRA_COLS; w++) {
      auto i = h * CGRA_COLS + w;

      if (h < CGRA_ROWS / 2) // up
      {
        cgra_[i][w] = true; //
        cgra_[w][i] = true;
      } else // down
      {
        cgra_[i][(CGRA_ROWS - 1) * CGRA_COLS + w] = true;
        cgra_[(CGRA_ROWS - 1) * CGRA_COLS + w][i] = true;
      }

      if (w < CGRA_ROWS / 2) // left
      {
        cgra_[i][h * CGRA_COLS] = true;
        cgra_[h * CGRA_COLS][i] = true;
      } else // right
      {
        cgra_[i][h * CGRA_COLS + CGRA_COLS - 1] = true;
        cgra_[h * CGRA_COLS + CGRA_COLS - 1][i] = true;
      }

      cgra_[i][(h - 1) % CGRA_ROWS * CGRA_ROWS + w] = true; // pe top
      cgra_[i][(h + 1) % CGRA_ROWS * CGRA_ROWS + w] = true; // pe end
      cgra_[i][h * CGRA_ROWS + (w - 1) % CGRA_COLS] = true; // pe left
      cgra_[i][h * CGRA_ROWS + (w + 1) % CGRA_COLS] = true; // pe right
      cgra_[i][i] = false;                                  // pe self remove
    }
}

void GurobiMapper::GernerMapperMatrixMode(std::unique_ptr<Graph> &graph) {
  auto num_ = graph->num_node_ids();
  int really_node{0};
  std::unordered_map<std::string, int> gnode_names;
  for (int i = 0; i < num_; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto prop_ = graph->FindNodeProperties(node->Name());

      // acquire load or store type
      if (prop_->node_def.code() == OP_LOAD ||
          prop_->node_def.code() == OP_STORE) {
        pe_memory_group.push_back(true);
      } else {
        pe_memory_group.push_back(false);
      }
      gnode_names[node->Name()] = really_node;
      load_nodes.push_back(node->Name());
      really_node++;
    }
  }

  std::vector<bool> tmp(really_node);
  for (signed i = 0; i < really_node; i++)
    pe_graph_group.push_back(tmp);

  for (int i = 0; i < num_; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto head_index = gnode_names[node->Name()];
      auto prop_ = graph->FindNodeProperties(node->Name());
      std::for_each(node->out_edges().begin(), node->out_edges().end(),
                    [&graph, &gnode_names, this, &head_index](Edge *e) {
                      auto dst_node = e->dst();
                      auto string_name = dst_node->Name();
                      auto index = gnode_names[string_name];
                      pe_graph_group[head_index][index] = true;
                    });
    }
  }

  node_num = really_node;
  matrix_num = am_.size();
}

Status GurobiMapper::StoreMapperJsonFile(const std::string store_path,
                                const unsigned index,
                                std::vector<std::pair<int,int>> f_,
                                std::vector<std::pair<int,int>> r_) {
    std::string filename = store_path + "/" + "mapper" + std::to_string(index) + ".json";
    std::ofstream f(filename, std::ios::out);
    using namespace  nlohmann;
    json json_result;
    json map_pe;
    json map_reg;

    std::for_each(f_.begin(),f_.end(),[this,&map_pe](std::pair<int,int> p){
        json element;
        element["id"] = p.first;
        element["name"] = load_nodes[p.first];
        element["node"] = p.second;
        map_pe.push_back(element);
    });
    json_result["map_node"] =  map_pe;

    std::for_each(r_.begin(),r_.end(),[&map_reg](std::pair<int,int> p){
        json element;
        element["id"] = p.first;
        element["node_src"] = p.second;
        element["node_dst"] = 0;
        map_reg.push_back(element);
    });
    json_result["map_reg"] =  map_reg;
    //reserve fout space
    f << std::setw(4) << json_result << std::endl;
    f.close();
    f.flush();
    return Status::OK();
}

} // namespace cgra

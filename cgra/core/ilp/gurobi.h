#ifndef CGRA_CORE_ILP_GUROBI_H
#define CGRA_CORE_ILP_GUROBI_H

#include <map>
#include "cgra/core/graph/dot_parser.h"

namespace cgra {
typedef std::vector<std::tuple<int,std::string, int>> MAPPER;
typedef std::vector<std::tuple<int,int,int>> MAPPER_REG;

class GurobiMapper{

public:
    explicit GurobiMapper(std::unique_ptr<Graph>& graph);
    void MapperInit();
    //virtual void AdoptCgraArch() = 0;
    void GurobiRun();
    std::map<int, int> mapper_;
    std::vector<bool> am_;
    std::vector<std::vector<bool>> cgra_;
    Status StoreMapperJsonFile(const std::string store_path,
                                    const unsigned index,
                                    std::vector<std::pair<int,int>> f,
                                    std::vector<std::pair<int,int>> r);
private:
    std::unique_ptr<Graph> analyza_graph;
    // pe load or store position
    std::vector<bool> pe_memory_group;
    // pe graph connect relationship
    std::vector<std::vector<bool>> pe_graph_group;
    //all of node name
    std::vector<std::string> load_nodes;
    //node num
    unsigned node_num;
    unsigned matrix_num;

    MAPPER mapper_result;
    MAPPER_REG mapper_reg;
    void GernerMapperMatrixMode(std::unique_ptr<Graph>& graph);
};

}

#endif  //CGRA_CORE_ILP_GUROBI_H

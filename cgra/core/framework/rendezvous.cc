#include "cgra/core/framework/rendezvous.h"
#include <queue>
#include <unordered_map>
#include "cgra/core/lib/core/notification.h"
#include "cgra/core/lib/strings/strcat.h"
#include "cgra/core/platform/default/logging.h"
#include "cgra/core/lib/core/errors.h"

namespace cgra {
/*  static */
std::string Rendezvous::CreateKey(const std::string& src_name, uint64 src_incarnation,
                             const std::string& dst_name,const std::string& name) {
  // NOTE: ';' is not used in the src name's job name.
  //
  // We include both sender and receiver in the key to facilitate
  // debugging. For correctness, we only need to encode the receiver.
  //
  // "src_incarnation" is used to distinguish a worker when it
  // restarts.
  char buf[strings::kFastToBufferSize];
  return strings::StrCat(
      src_name, ";", strings::Uint64ToHexString(src_incarnation, buf), ";",
      dst_name, ";", name);
}

// Return the prefix of "*s" up to the next occurrence of "delim", or
// the whole remaining string if "delim" is not found.  "*s" is advanced
// past the string returned plus the delimiter (if found).
static StringPiece ConsumeNextPart(StringPiece* s, char delim) {
  for (size_t offset = 0; offset < s->size(); offset++) {
    if ((*s)[offset] == delim) {
      StringPiece result(s->data(), offset);
      s->remove_prefix(offset + 1);  // +1: remove delim, as well
      return result;
    }
  }
  // No delimiter found: return rest of string
  StringPiece result(s->data(), s->size());
  s->remove_prefix(s->size());
  return result;
}

/* static */
Status Rendezvous::ParseKey(StringPiece key, ParsedKey* out) {
  if (key.data() == out->buf_.data()) {
    // Caller used our buf_ string directly, so we don't need to copy.  (The
    // SendOp and RecvOp implementations do this, for example).
    DCHECK_EQ(key.size(), out->buf_.size());
  } else {
    // Make a copy that our StringPieces can point at a copy that will persist
    // for the lifetime of the ParsedKey object.
    out->buf_.assign(key.data(), key.size());
  }
  StringPiece s(out->buf_);
  StringPiece parts[5];
  for (int i = 0; i < 4; i++) {
    parts[i] = ConsumeNextPart(&s, ';');
  }
  if (s.empty() &&          // Consumed the whole string
      strings::HexStringToUint64(parts[1], &out->src_incarnation) &&
      !parts[3].empty()) {
    out->src_name = StringPiece(parts[0].data(), parts[0].size());
    out->dst_name = StringPiece(parts[2].data(), parts[2].size());
    out->edge_name = StringPiece(parts[3].data(), parts[3].size());
    return Status::OK();
  }
  return errors::InvalidArgument("Invalid  rendezvous key: ", key);
}

Rendezvous::~Rendezvous() {}

Status Rendezvous::Recv(const ParsedKey& key, const Args& recv_args, Stream* val, bool* is_dead,
                        int64 timeout_ms) {
    Status ret;
    Notification n;
    RecvAsync(key, recv_args,
              [&ret, &n, val, is_dead, &recv_args](const Status& s, const Args& send_args,
                                            const Args& recv_args1, const Stream& v,
                                            const bool dead) {
      ret = s;
      *val = v;
      *is_dead = dead;
      auto args = const_cast<Args*>(&recv_args);
      *args = send_args;
      n.Notify();
    });
    if (timeout_ms > 0) {
      int64 timeout_us = timeout_ms * 1000;
      bool notified = WaitForNotificationWithTimeout(&n, timeout_us);
      if (!notified) {
        return Status(error::DEADLINE_EXCEEDED,
                      "Timed out waiting for notification");
      }
    } else {
      n.WaitForNotification();
    }
    return ret;
}

Status Rendezvous::Recv(const ParsedKey& key, const Args& args, Stream* val,
                        bool* is_dead) {
  const int64 no_timeout = 0;
  return Recv(key, args, val, is_dead, no_timeout);
}



class LocalRendezvousImpl : public Rendezvous {
 public:
  explicit LocalRendezvousImpl() {}

  Status Send(const ParsedKey& key,  const Args& send_args, const Stream& val,
              const bool is_dead) override {
    uint64 key_hash = KeyHash(key.FullKey());
    VLOG(2) << "Send " << this << " " << key_hash << " " << key.FullKey();

    mu_.lock();
    if (!status_.ok()) {
      // Rendezvous has been aborted.
      Status s = status_;
      mu_.unlock();
      return s;
    }

    ItemQueue* queue = &table_[key_hash];
    if (queue->empty() || queue->front()->IsSendValue()) {
      // There is no waiter for this message. Append the message
      // into the queue. The waiter will pick it up when arrives.
      // Only send-related fields need to be filled.
     Item* item = new Item;
     item->value = val;
     item->is_dead = is_dead;
     item->send_args = send_args;
     queue->push_back(item);
     mu_.unlock();
     return Status::OK();
    }

    // There is an earliest waiter to consume this message.
    Item* item = queue->front();
    item->recv_args = send_args;
    queue->pop_front();
    mu_.unlock();

    // Notify the waiter by invoking its done closure, outside the
    // lock.
    DCHECK(!item->IsSendValue());
    item->waiter(Status::OK(), send_args, item->recv_args, val, is_dead);
    delete item;
    return Status::OK();
  }

  void RecvAsync(const ParsedKey& key, const Args& recv_args,DoneCallback done) override {
    uint64 key_hash = KeyHash(key.FullKey());
    VLOG(2) << "Recv " << this << " " << key_hash << " " << key.FullKey();

    mu_.lock();
    if (!status_.ok()) {
      // Rendezvous has been aborted.
      Status s = status_;
      mu_.unlock();
      done(s, Args(), recv_args, Stream(), false);
      return;
    }

    ItemQueue* queue = &table_[key_hash];
    if (queue->empty() || !queue->front()->IsSendValue()) {
      // There is no message to pick up.
      // Only recv-related fields need to be filled.
      Item* item = new Item;
      item->waiter = std::move(done);
      item->recv_args = recv_args;
      queue->push_back(item);
      mu_.unlock();
      return;
    }

    // A message has already arrived and is queued in the table under
    // this key.  Consumes the message and invokes the done closure.
    Item* item = queue->front();
    queue->pop_front();
    mu_.unlock();

    // Invokes the done() by invoking its done closure, outside scope
    // of the table lock.
    DCHECK(item->IsSendValue());
    done(Status::OK(), item->send_args, recv_args, item->value, item->is_dead);
    delete item;
  }

  void StartAbort(const Status& status) override {
    CHECK(!status.ok());
    Table table;
    {
      mutex_lock l(mu_);
      status_.Update(status);
      table_.swap(table);
    }
    for (auto& p : table) {
      for (Item* item : p.second) {
        if (!item->IsSendValue()) {
          item->waiter(status, Args(), Args(), Stream(), false);
        }
        delete item;
      }
    }
  }

 private:
  struct Item {
    DoneCallback waiter = nullptr;
    Stream value;
    bool is_dead = false;
    Args send_args;
    Args recv_args;
    ~Item() {}

    // Returns true iff this item represents a value being sent.
    bool IsSendValue() const { return this->waiter == nullptr; }
  };

  // We key the hash table by KeyHash of the Rendezvous::CreateKey string
  static uint64 KeyHash(const StringPiece& k) {
    return std::hash<string>()(k.ToString());
  }

  typedef std::deque<Item*> ItemQueue;
  typedef std::unordered_map<int64, ItemQueue> Table;
  mutex mu_;
  Table table_;
  Status status_;
  ~LocalRendezvousImpl() override {
    if (!table_.empty()) {
      StartAbort(errors::Cancelled("LocalRendezvousImpl deleted"));
    }
  }
   CGRA_DISALLOW_COPY_AND_ASSIGN(LocalRendezvousImpl);
};

Rendezvous* NewLocalRendezvous() { return new LocalRendezvousImpl(); }
}

#include "cgra/core/framework/session.h"
#include "cgra/core/framework/session_factory.h"

namespace cgra {

Session* NewSession(SessionOptions& option) {
	
	SessionFactory* factory;
	Status status = SessionFactory::GetFactory(option, &factory);
	if (status == Status::OK()) {
		Session* session = factory->NewSession(option);
		return session;
	}
	return nullptr;
}

}



#include "cgra/core/framework/session_factory.h"
#include <string>
#include "cgra/core/lib/core/errors.h"
#include "cgra/core/platform/default/mutex.h"

namespace cgra {
namespace {

mutex* get_session_factory_lock() {
  static mutex session_factory_lock(LINKER_INITIALIZED);
  return &session_factory_lock;
}

typedef std::unordered_map<std::string, SessionFactory*> SessionFactories;
SessionFactories* session_factories() {
  static SessionFactories* session_factories = new SessionFactories;
  return session_factories;
}
}

void SessionFactory::Register(const std::string& runtime_type,
                              SessionFactory* factory) {
  std::string type_ = static_cast<std::string>(runtime_type);
  mutex_lock l(*get_session_factory_lock());
  if (!session_factories()->insert({runtime_type, factory}).second) {
    std::cout << "Two session factories are being registered " << type_
              << std::endl;
  }
}

Status SessionFactory::GetFactory(SessionOptions& option,
                                  SessionFactory** factory) {
  mutex_lock l(*get_session_factory_lock());
  std::vector<std::pair<const std::string&, SessionFactory*>> content_factory;
  for (auto& session_factory : *session_factories()) {
    if ((session_factory.second)->AcceptOption(option)) {
      content_factory.push_back(session_factory);
    } else {
      return errors::InvalidArgument(
          "SessionFactory unsupported SessionOptions:", option.type,
          "Like this");
    }
  }

  if (content_factory.size() == 1) {
    *factory = content_factory[0].second;
	return Status::OK();
  } else if (content_factory.size() > 1) {
    std::vector<string> factory_types;
    factory_types.reserve(content_factory.size());
    for (const auto& candidate_factory : content_factory) {
      factory_types.push_back(candidate_factory.first);
    }
    return errors::Internal(
        "Multiple session factories registered for the given session ");
  } else {
    return errors::NotFound(
        "No session factory registered for the given session options: {",
        option.type, "} ");
  }
}
}

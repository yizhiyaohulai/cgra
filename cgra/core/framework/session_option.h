#ifndef CGRA_CORE_FRAMEWORK_SESSION_OPTION_H
#define CGRA_CORE_FRAMEWORK_SESSION_OPTION_H
#include <string>
#include <cgra/core/protobuf/config.pb.h>

namespace cgra {

class SessionOptions {
public:
	std::string type;
	ConfigProto config;
};
}	//namespace cgra

#endif

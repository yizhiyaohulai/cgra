#ifndef CGRA_CORE_FRAMEWORK_DEVICE_FACTORY_H
#define CGRA_CORE_FRAMEWORK_DEVICE_FACTORY_H
#include "cgra/core/framework/device.h"
#include <string>
namespace cgra {

class DeviceFactory {
public:
  explicit DeviceFactory(){};
  ~DeviceFactory() = default;
  static void Register(const std::string &device_type,
                       DeviceFactory *device_factory);
  static Device *NewDevice(const std::string &device_type);
  static void CreateDevice(const std::string &device_type);
};

namespace dfactory {
template <typename Factory> class Registrar {
  explicit Registrar(std::string &device_type) {
    DeviceFactory::Register(device_type, new Factory());
  }
};

} // namespace dfactory

#define REGISTER_LOCAL_DEVICE_FACTORY(device_type, device_factory, ...)        \
  INTERNAL_REGISTER_LOCAL_DEVICE_FACTORY(device_type, device_factory,          \
                                         __COUNTER__, ##__VA_ARGS__)

#define INTERNAL_REGISTER_LOCAL_DEVICE_FACTORY(device_type, device_factory,    \
                                               ctr, ...)                       \
  static ::cgra::dfactory::Registrar<device_factory>                     \
      INTERNAL_REGISTER_LOCAL_DEVICE_FACTORY_NAME(ctr)(device_type,            \
                                                       ##__VA_ARGS__)

// __COUNTER__ must go through another macro to be properly expanded
#define INTERNAL_REGISTER_LOCAL_DEVICE_FACTORY_NAME(ctr) ___##ctr##__object_

} // namespace cgra

#endif

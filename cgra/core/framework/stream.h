#ifndef CGRA_CORE_FRAMEWORK_STREAM_H_
#define CGRA_CORE_FRAMEWORK_STREAM_H_

#include <string>
#include <iostream>
#include <type_traits>
#include <typeinfo>
#include <vector>
namespace cgra {
template <typename T>
class StreamContext {
 public:
  explicit StreamContext(){};
  ~StreamContext() = default;
  T register_t;
};

class Stream{
public:
    explicit Stream(){};
    ~Stream() = default;
};

}  // namespace cgra

#endif

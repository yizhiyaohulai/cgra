#ifndef CGRA_CORE_FRAMEWORK_SESSION_H
#define CGRA_CORE_FRAMEWORK_SESSION_H

#include "cgra/core/framework/session_option.h"
#include "cgra/core/graph/dot_parser.h"

namespace cgra {

class Session {
 public:
 // virtual void PRun() = 0;
  virtual void Run() = 0;
};

Session* NewSession(SessionOptions& option);

}  // namespace cgra

#endif

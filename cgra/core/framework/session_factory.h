#ifndef CGRA_CORE_FRAMEWORK_SESSION_FACTORY_H_
#define CGRA_CORE_FRAMEWORK_SESSION_FACTORY_H_
#include <iostream>
#include <mutex>
#include <unordered_map>
#include "cgra/core/framework/session.h"
#include "cgra/core/framework/session_option.h"
#include "cgra/core/lib/core/errors.h"
#include "cgra/core/lib/core/status.h"
namespace cgra {

class SessionFactory {
 public:
  virtual ~SessionFactory() = default;
  virtual bool AcceptOption(SessionOptions& option) = 0;
  virtual Session* NewSession(SessionOptions& option) = 0;
  static Status GetFactory(SessionOptions& option, SessionFactory** factory);
  static void Register(const std::string& runtime_type, SessionFactory* factory);
};
}

#endif

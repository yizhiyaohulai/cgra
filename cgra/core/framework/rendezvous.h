#ifndef CGRA_CORE_FRAMEWORK_RENDEZVOUS_H_
#define CGRA_CORE_FRAMEWORK_RENDEZVOUS_H_

#include "cgra/core/framework/stream.h"
#include "cgra/core/lib/core/refcount.h"
#include "cgra/core/lib/core/status.h"
#include "cgra/core/lib/core/stringpiece.h"
#include "cgra/core/platform/default/mutex.h"

namespace cgra {

class Rendezvous : public core::RefCounted {
public:
  struct Args {
    std::string cmd;
  };

  static std::string CreateKey(const std::string &src_device,
                               uint64 src_incarnation,
                               const std::string &dst_device,
                               const std::string &name);
  struct ParsedKey {
    StringPiece src_name;
    uint64 src_incarnation = 0;
    StringPiece dst_name;
    StringPiece edge_name;
    ParsedKey() {}
    ParsedKey(const ParsedKey &b) { *this = b; }

    ParsedKey &operator=(const ParsedKey &b);
    StringPiece FullKey() const { return buf_; }

  private:
    friend class Rendezvous;
    friend class SendOp;
    friend class RecvOp;
    string buf_;
  };

  static Status ParseKey(StringPiece key, ParsedKey *out);

  virtual Status Send(const ParsedKey &key, const Args &args, const Stream &val,
                      const bool is_dead) = 0;

  typedef std::function<void(const Status &, const Args &, const Args &,
                             const Stream &, const bool)>
      DoneCallback;

  virtual void RecvAsync(const ParsedKey &key, const Args &args,
                         DoneCallback done) = 0;
  Status Recv(const ParsedKey &key, const Args &args, Stream *val,
              bool *is_dead, int64 timeout_ms);
  Status Recv(const ParsedKey &key, const Args &args, Stream *val,
              bool *is_dead);
  // Aborts all pending and future Send/Recv with the given "status".
  //
  // StartAbort() does not wait for ongoing calls to finish.
  // REQUIRES: !status.ok()
  virtual void StartAbort(const Status &status) = 0;
protected:
  ~Rendezvous() override;
};

// Returns a Rendezvous instance that is limited to use only by
// producers and consumers in the local process.  The caller assumes
// ownership of one Ref() on the returned object.
Rendezvous *NewLocalRendezvous();

} // namespace cgra
#endif // CGRA_CORE_FRAMEWORK_RENDEZVOUS_H_

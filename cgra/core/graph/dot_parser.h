#ifndef CGRA_CORE_GRAPH_DOTPARSER_H_
#define CGRA_CORE_GRAPH_DOTPARSER_H_

#include "cgra/core/graph/graph.h"
#include <memory>

namespace cgra {

class DotParser {
public:
  /// \Parse Dfg Graph File
  ///
  /// Notice: Add src node to dst node.
  std::unique_ptr<Graph> ParseGraph(std::string dot_filename);

  Status RemoveUnnecessaryLeafNodes(std::unique_ptr<Graph> &graph);

  Status ReplaceIcmpToSub(std::unique_ptr<Graph> &graph);

  Status RemovePhiNodes(std::unique_ptr<Graph> &graph);

  void ExtractUsefulInform(std::string dot_filename,
                           std::unique_ptr<Graph> &graph);

  /// \ Accelerate Invailed Operation.
  ///
  /// Remove Non Operation from graph.
  bool AccelerateInvailedOp(std::unique_ptr<Graph> &graph);

  /// \Merge Similarity Const Value
  bool MergeSimilarConstStructs(std::unique_ptr<Graph> &graph);

  /// \Export Optimize DFG.dot File
  bool PrintDfgFileSystem(std::unique_ptr<Graph> &graph, std::string filename);

  /// \Spread Immediate Value
  Status UnfoldImmediateValue(std::unique_ptr<Graph> &graph);

  /// \Extract Immediate Value
  bool ExtractConstImmediateValue(std::unique_ptr<Graph> &graph);

  /// \ Merge Redundancy Operation.
  ///
  /// multi add operation merge .
  bool MergeRedundancyOp(std::unique_ptr<Graph> &graph);

  /// \Change Dfg Operation code To Cgra Operation code
  Status ConvertToCgraCode(std::unique_ptr<Graph> &graph);

  /// \abs operation merge
  bool MergeAbsOp(std::unique_ptr<Graph> &graph);

  /// \analysis Fabs funtion in source code
  Status AnalysisFabsFunction(std::unique_ptr<Graph> &graph);

  Status StoreJsonFile(std::unique_ptr<Graph> &graph,
                       const std::string store_path, const unsigned index);
  Status StoreMapperJsonFile(std::vector<std::pair<std::string, int>>& s,
                             const std::string store_path,
                             const unsigned index);

private:
  /// \Concordance Operation name from graph
  ///
  /// Sort Node Operation and Rename Operation name
  bool ConcordanceDisorderOp(std::unique_ptr<Graph> &graph);

  /// \Demarcate sharedmemory allowed have a placeholder
  ///
  /// Cgra middle variate architecture have placeholder
  bool DemarcateShareMemoryPlaceholder(std::unique_ptr<Graph> &graph);

  /// \Verify Node parameter
  Status PrintNodeParameter(std::unique_ptr<Graph> &graph);

  Status RepaireStoreEdgeIndex(std::unique_ptr<Graph> &graph);

private:
  int loop_begin;
  int loop_end;
  int loop_offset;
};

} // namespace cgra

#endif

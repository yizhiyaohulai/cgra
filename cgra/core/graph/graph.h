#ifndef CGRA_CORE_GRAPH_GRAPH_H_
#define CGRA_CORE_GRAPH_GRAPH_H_
#include <map>
#include <memory>
#include <unordered_map>
#include <vector>
#include "cgra/core/framework/op_def.pb.h"
#include "cgra/core/framework/node_def.pb.h"
#include "cgra/core/lib/core/errors.h"

namespace cgra {
class Node;
class Edge;
class Graph;
class NodeProperties {
 public:
  NodeProperties(const OpDef* op_def, const NodeDef& node_def)
      : op_def(op_def), node_def(node_def){};
  const OpDef* op_def;  // not owned
  NodeDef node_def;
};

class Edge {
 public:
  Node* src() const { return src_; };
  Node* dst() const { return dst_; };
  int id() const { return id_; };
  int src_output() const { return src_output_; };
  int dst_input() const { return dst_input_; };

 private:
  Edge(){};
  friend class Node;
  friend class Graph;
  Node* src_;
  Node* dst_;
  int id_;
  int src_output_;
  int dst_input_;
};

class Node {
 public:
  int id() const { return id_; };
  int cost_id() const { return cost_id_; };
  const std::string& Name();
  const std::string& Op();
  const std::vector<Edge*>& in_edges() const { return in_edges_; }
  const std::vector<Edge*>& out_edges() const { return out_edges_; }
 private:
  Node();
  friend class Graph;

  NodeProperties* properties() const { return props_.get(); }

  void Clear();
  int id_;       // -1 until Initialize() is called
  int cost_id_;  // -1 if there is no corresponding cost accounting node
  std::vector<Edge*> in_edges_;
  std::vector<Edge*> out_edges_;

  std::shared_ptr<NodeProperties> props_;
  Graph* graph_;
  inline int num_outputs() const;
  void Initialize(int id, int cost_id, std::shared_ptr<NodeProperties> props);
};

class Graph {
 public:
  explicit Graph();
  virtual ~Graph();
  static const int kControlSlot;
 public:
  std::string NewName(std::string prefix);
  Node* AddNode(const OpDef* opdef, NodeDef& nodedef);

  // Copies *node, which may belong to another graph, to a new node,
  // which is returned.  Does not copy any edges.  *this owns the
  // returned instance.
  Node* CopyNode(const Node* node);
  void RemoveNode(Node* node);
  const Edge* AddEdge(Node* source, int x, Node* dest, int y);
  void RemoveEdge(const Edge*);
  void UpdateEdge(Node* new_src, int new_src_index, Node* dst, int dst_index);
  Node* AllocateNode(std::shared_ptr<NodeProperties> props,
                     const Node* cost_node);
  void ReleaseNode(Node* node);
  Status IsValidNode(const Node* node) const;
  std::shared_ptr<NodeProperties> FindNodeProperties(const std::string name);
  Node* FindNode(const std::string name);
  void FindNodeOp(const std::string name, std::vector<Node*>& nodes);
  const Edge* FindEdge(const Node* dst, int index);

  // The number of live edges in the graph.
  //
  // Because edges can be removed from the graph, num_edges() is often
  // smaller than num_edge_ids(). If one needs to create an array of
  // edges indexed by edge ids, num_edge_ids() should be used as the
  // array's size.
  int num_edges() const { return num_edges_; }

  // Returns one more than the maximum id assigned to any node.
  int num_node_ids() const { return nodes_.size(); }

  // Returns the node associated with an id, or nullptr if no node
  // with that id (the node with that id was removed and the id has
  // not yet been re-used). *this owns the returned instance.
  // REQUIRES: 0 <= id < num_node_ids().
  Node* FindNodeId(int id) const { return nodes_[id]; }

  // Returns the Edge associated with an id, or nullptr if no edge
  // with that id (the node with that id was removed and the id has
  // not yet been re-used). *this owns the returned instance.
  // REQUIRES: 0 <= id < num_node_ids().
  const Edge* FindEdgeId(int id) const { return edges_[id]; }

  // Returns one more than the maximum id assigned to any edge.
  int num_edge_ids() const { return edges_.size(); }

  //Returns the opcode max number
  int TraversalMaxNodeName(const std::string name);

 private:
  std::vector<Edge*> edges_;
  std::vector<Node*> nodes_;
  int num_nodes_ = 0;
  int num_edges_ = 0;

  // Allocated but free nodes and edges.
  std::vector<Node*> free_nodes_;
  std::vector<Edge*> free_edges_;
};

}  // namespace cgra
#endif

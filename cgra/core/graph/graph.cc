#include "cgra/core/graph/graph.h"
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <map>
#include <regex>
#include <unordered_map>
#include "cgra/core/lib/core/errors.h"
#include "cgra/core/platform/default/logging.h"

namespace cgra {

const int Graph::kControlSlot = -1;

Node::Node() : id_(-1), cost_id_(-1), props_(nullptr) {}

void Node::Initialize(int id, int cost_id,
                      std::shared_ptr<NodeProperties> props) {
  id_ = id;
  cost_id_ = cost_id;
  props_ = std::move(props);
}

void Node::Clear() {
  in_edges_.clear();
  out_edges_.clear();
  id_ = -1;
  cost_id_ = -1;
  props_.reset();
}

const std::string& Node::Name() { return props_->node_def.name(); }
const std::string& Node::Op() { return props_->node_def.op(); }

inline int Node::num_outputs() const { return out_edges_.size(); }

// Graph::Graph(const OpRegistryInterface* ops)
//    : ops_(ops, FunctionDefLibrary()),
//      arena_(8 << 10 /* 8kB */) {
//  Status status;
//
//}

Graph::Graph() {}

Graph::~Graph() {
  // Manually call the destructors for all the Nodes we constructed using
  // placement new.
  for (Node* node : nodes_) {
    if (node != nullptr) {
      node->~Node();
    }
  }
  for (Node* node : free_nodes_) {
    node->~Node();
  }
}

Node* Graph::AddNode(const OpDef* opdef, NodeDef& nodedef) {
  Node* node =
      AllocateNode(std::make_shared<NodeProperties>(opdef, nodedef), nullptr);
  return node;
}

Node* Graph::CopyNode(const Node* node) {
  Node* copy = AllocateNode(node->props_, node);
  return copy;
}

std::shared_ptr<NodeProperties> Graph::FindNodeProperties(
    const std::string name) {
  for (auto it : nodes_) {
    if (it != nullptr) {
      if (it->Name() == name) {
        return it->props_;
      }
    }
  }
  errors::NotFound("Don't Find NodeProperties Node in nodes_");
  return nullptr;
}

Node* Graph::FindNode(const std::string name) {
  for (auto it : nodes_) {
    if (it != nullptr) {
      if (it->Name() == name) {
        return it;
      }
    }
  }
  return nullptr;
}

void Graph::FindNodeOp(const std::string name, std::vector<Node*>& nodes) {
  for (auto it : nodes_) {
    if (it != nullptr) {
      if (it->Op() == name) {
        nodes.push_back(it);
      }
    }
  }
}

void Graph::RemoveNode(Node* node) {
  // Remove any edges involving this node.
  while (!node->in_edges_.empty()) {
    RemoveEdge(node->in_edges_[0]);
  }
  while (!node->out_edges_.empty()) {
    RemoveEdge(node->out_edges_[0]);
  }
  ReleaseNode(node);
}

const Edge* Graph::AddEdge(Node* source, int x, Node* dest, int y) {
  Edge* e = nullptr;
  if (free_edges_.empty()) {
    e = new Edge;  // placement new
  } else {
    e = free_edges_.back();
    free_edges_.pop_back();
  }
  e->id_ = edges_.size();
  e->src_ = source;
  e->dst_ = dest;
  e->src_output_ = x;
  e->dst_input_ = y;
  source->out_edges_.push_back(e);
  dest->in_edges_.push_back(e);
  edges_.push_back(e);
  ++num_edges_;
  return e;
}

void Graph::RemoveEdge(const Edge* e) {
  for (auto it = e->src_->out_edges_.begin();
       it != e->src_->out_edges_.end();) {
    if ((*it) == e) {
      it = e->src_->out_edges_.erase(it);
    } else {
      ++it;
    }
  }

  for (auto it = e->dst_->in_edges_.begin(); it != e->dst_->in_edges_.end();) {
    if ((*it) == e) {
      it = e->dst_->in_edges_.erase(it);
    } else {
      ++it;
    }
  }

  edges_[e->id_] = nullptr;

  Edge* del = const_cast<Edge*>(e);
  del->src_ = nullptr;
  del->dst_ = nullptr;
  del->id_ = -1;
  del->src_output_ = kControlSlot - 1;
  del->dst_input_ = kControlSlot - 1;
  free_edges_.push_back(del);
  --num_edges_;
}

void Graph::UpdateEdge(Node* new_src, int new_src_index, Node* dst,
                       int dst_index) {
  const Edge* e = FindEdge(dst, dst_index);
  RemoveEdge(e);
  AddEdge(new_src, new_src_index, dst, dst_index);
}

const Edge* Graph::FindEdge(const Node* dst, int index) {
  for (const Edge* e : edges_) {
    // edges_ will contain null edges if RemoveEdge() was called.
    if (e == nullptr) continue;
    if (e->dst() == dst && e->dst_input() == index) {
      return e;
    }
  }
  return nullptr;
}

Node* Graph::AllocateNode(std::shared_ptr<NodeProperties> props,
                          const Node* cost_node) {
  Node* node = nullptr;
  if (free_nodes_.empty()) {
    node = new Node();
  } else {
    node = free_nodes_.back();
    free_nodes_.pop_back();
  }
  node->graph_ = this;
  const int id = nodes_.size();
  int cost_id = cost_node ? cost_node->cost_id() : id;
  node->Initialize(id, cost_id, std::move(props));
  nodes_.push_back(node);
  ++num_nodes_;
  return node;
}

void Graph::ReleaseNode(Node* node) {
  nodes_[node->id()] = nullptr;
  free_nodes_.push_back(node);
  --num_nodes_;
  node->Clear();
}

std::string Graph::NewName(std::string prefix) { return prefix; }

Status Graph::IsValidNode(const Node* node) const {
  if (node == nullptr) {
    return errors::InvalidArgument("Parameter: Node is null");
  }
  const int id = node->id();
  if (id < 0) {
    return errors::InvalidArgument("node id ", id, " is less than zero");
  }

  if (static_cast<size_t>(id) >= nodes_.size()) {
    return errors::InvalidArgument(
        "node id ", id, " is >= than number of nodes in graph ", nodes_.size());
  }
  if (nodes_[node->id()] != node) {
    return errors::InvalidArgument("Node with id ", id,
                                   " is different from the passed in node. "
                                   "Does it belong to a different graph?");
  }
}

int Graph::TraversalMaxNodeName(const std::string name) {
  auto node_num = num_node_ids();
  std::set<unsigned> node_indexs;
  for (unsigned i = 0; i < node_num; i++) {
    auto node = nodes_[i];
    if (node != nullptr) {
      std::string node_name = node->props_->node_def.name();
      if (node_name.find(name) != string::npos) {
        std::regex reg{R"([\d=]*)"};
        std::regex_token_iterator<std::string::const_iterator> it(
            node_name.cbegin(), node_name.cend(), reg);
        for (auto iter = it; iter != std::sregex_token_iterator(); iter++) {
          if (iter->length()) {
            node_indexs.insert(std::atoi(iter->str().c_str()));
          }
        }
      }
    }
  }
  if (node_indexs.size())
	  return *(node_indexs.rbegin());
  else
	  return -1;
}

}  // namespace cgra

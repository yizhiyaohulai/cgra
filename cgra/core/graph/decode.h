#ifndef CGRA_CORE_GRAPH_DECODE_H_
#define CGRA_CORE_GRAPH_DECODE_H_

namespace cgra {

enum FUNCOPTION {
  K_TOP_FUN_OPTION,
  K_ALU_FUN_OPTION,
  K_MEM_FUN_OPTION,
  K_JUMP_FUN_OPTION
};

struct {
  unsigned char opcode : 4;
  unsigned short iteration : 10;
  unsigned char out3 : 1;
  unsigned char out2 : 7;
  unsigned char out1 : 7;
  unsigned char imm : 1;
  unsigned char in4 : 6;
  unsigned char in3 : 8;
  unsigned char in2 : 8;
  unsigned char in1 : 8;
  FUNCOPTION func : 2;
  unsigned char configextend : 1;
} ALU_PE_ITEM;

struct {
  unsigned char opcode : 2;
  unsigned char reserve0 : 3;
  unsigned short iteration : 10;
  unsigned char reserve1 : 1;
  unsigned char reserve2 : 7;
  unsigned char out1 : 7;
  unsigned char reserve3 : 3;
  unsigned char offset : 4;
  unsigned char inmem : 8;
  unsigned char directaddrmem : 8;
  unsigned char addrmem : 8;
  FUNCOPTION func : 2;
  unsigned char configextend : 1;
} MEM_PE_ITEM;

struct {
  unsigned char opcode : 5;
  unsigned short iteration : 10;
  unsigned char reserve1 : 1;
  unsigned char reserve2 : 7;
  unsigned char outjump : 7;
  unsigned char jump : 3;
  unsigned char offset : 4;
  unsigned char addrjump : 8;
  unsigned char int2 : 8;
  unsigned char int1 : 8;
  FUNCOPTION func : 2;
  unsigned char configextend : 1;
} JUMP_PE_ITEM;

struct {
  unsigned char count : 6;
  unsigned char reserve0 : 6;
  unsigned char iteration_line1 : 3;
  unsigned char iteration_line2 : 2;
  unsigned char initial_idle : 6;
  unsigned char initial_pe : 7;
  unsigned char initial_pea : 7;
  unsigned char indexpe : 8;
  unsigned char bitwidth : 3;
  unsigned char packageindex : 5;
  unsigned char reserve1 : 3;
  unsigned char taskpackagenum : 5;
  FUNCOPTION func : 2;
  unsigned char configextend : 1;
} TOP_PE_ITEM;
};

#endif
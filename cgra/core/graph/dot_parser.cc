#include "cgra/core/graph/dot_parser.h"
#include "cgra/core/graph/graph.h"
#include "cgra/core/lib/core/errors.h"
#include <assert.h>
#include <fstream>
#include <iomanip>
#include <iostream>
//#include <json/install/include/nlohmann/json.hpp>
#include <cgra/core/lib/tools/json.hpp>
#include <memory>
#include <queue>
#include <regex>
#include <string>
#include <unordered_map>

namespace cgra {

void ReplaceSymbolMethod(std::string &str, const std::string source,
                         const std::string reserve) {
  while (str.find(source) != std::string::npos) {
    str = str.replace(str.find(source), 1, reserve);
  }
}

auto capture_num(const std::string &name) {
  std::regex reg{R"([\d=]*)"};
  std::regex_token_iterator<std::string::const_iterator> it(name.cbegin(),
                                                            name.cend(), reg);
  for (auto iter = it; iter != std::sregex_token_iterator(); iter++) {
    if (iter->length()) {
      return std::atoi(iter->str().c_str());
    }
  }
  return 0;
}

void SplitString(const std::string& s, std::vector<string>& v, const std::string& c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
     pos1 = 0;
     while(string::npos != pos2)
     {
         v.push_back(s.substr(pos1, pos2-pos1));

         pos1 = pos2 + c.size();
         pos2 = s.find(c, pos1);
     }
     if(pos1 != s.length())
         v.push_back(s.substr(pos1));
}

std::unordered_map<std::string, std::string>
serach_regex_tuple_string_(std::string &str) {
  std::smatch matchResult;
  static std::regex pattern("[\\[][a-z]+[=][a-z0-9]+[\\]]");
  std::unordered_map<std::string,std::string> map_result_;
  while (std::regex_search(str, matchResult, pattern)) {
    for (auto x = matchResult.begin(); x != matchResult.end(); x++){
        std::string x_str = x->str();
        x_str = x_str.substr(0, x_str.length() - 1);    //delete laset charater
        x_str = x_str.substr(1, x_str.length());        //delete first charater
        //std::cout << x_str << std::endl;
        std::vector<std::string> commom_;
        SplitString(x_str,commom_,"=");
        map_result_[commom_[0]] = commom_[1];
    }
    str = matchResult.suffix().str();
  }
  return map_result_;
}

void DotParser::ExtractUsefulInform(std::string dot_filename,
                                    std::unique_ptr<Graph> &graph) {}

/// branch num is node inputedge index
std::unique_ptr<Graph> DotParser::ParseGraph(std::string dot_filename) {
  assert(!dot_filename.empty());

  std::ifstream infile;
  infile.open(dot_filename.data());
  assert(infile.is_open());

  std::string str;
  std::unique_ptr<Graph> graphs = std::unique_ptr<Graph>(new Graph);
  static std::regex pattern_edge("(.*)[\\-][\\>](.*?)[\\[](.*)");
  static std::regex pattern("(.*)[\\[](.*)[\\]];");
  std::smatch matchResult;
  while (getline(infile, str)) {
      if(std::regex_match(str, matchResult, pattern_edge)){
         std::string node_name = matchResult[2];
         std::string sub_node_name = matchResult[1];
         auto map_ = serach_regex_tuple_string_(str);
         std::shared_ptr<NodeProperties> props_ =
             graphs->FindNodeProperties(node_name);
         std::shared_ptr<NodeProperties> const_props_ =
             graphs->FindNodeProperties(sub_node_name);
         unsigned branch = std::stoi(map_["operand"]);
         if(!map_["const"].empty()){
            signed data =  std::stoi(map_["const"]);
            const_props_->node_def.set_const_(true);
            const_props_->node_def.set_i(data);
         }
         else{
            const_props_->node_def.set_const_(false);
         }

         props_->node_def.add_input(node_name.c_str());
         graphs->AddEdge(graphs->FindNode(sub_node_name), 0,
                         graphs->FindNode(node_name), branch);
      }
      else{
        if(std::regex_match(str,matchResult,pattern)){
            NodeDef node_def;
            std::string::size_type pos = str.find("[");
            auto first = str.substr(0, pos);
            node_def.set_name(first);
            auto map_ = serach_regex_tuple_string_(str);
            node_def.set_op(map_["opcode"]);
            std::string pointername = map_["pointername"];
            //node_def.set_p_name(pointername);
            graphs->AddNode(new OpDef, node_def);
        }
      }
  }
  return graphs;
}

Status DotParser::ConvertToCgraCode(std::unique_ptr<Graph> &graph) {
  auto node_num = graph->num_node_ids();
  for (unsigned i = 0; i < node_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto prop_ = graph->FindNodeProperties(node->Name());
      /*
      replace node name to cgra recognize node name
      */
      if (prop_->node_def.op() == "phi") {
        prop_->node_def.set_op("sel");
        auto index = capture_num(node->Name());
        std::string name = "sel" + std::to_string(index);
        prop_->node_def.set_name(name.c_str());
        prop_->node_def.set_code(cgra::OP_SEL);
      } else if (prop_->node_def.op() == "shl") {
        prop_->node_def.set_op("sll");
        auto index = capture_num(node->Name());
        std::string name = "sll" + std::to_string(index);
        prop_->node_def.set_name(name.c_str());
        prop_->node_def.set_code(cgra::OP_SLL);
      } else if (prop_->node_def.op() == "shra") {
        prop_->node_def.set_op("srl");
        auto index = capture_num(node->Name());
        std::string name = "srl" + std::to_string(index);
        prop_->node_def.set_name(name.c_str());
        prop_->node_def.set_code(cgra::OP_SRL);
      } else if (prop_->node_def.op() == "load") {
        prop_->node_def.set_code(cgra::OP_LOAD);
      } else if (prop_->node_def.op() == "output") {
        prop_->node_def.set_op("store");
        auto index = capture_num(node->Name());
        std::string name = "store" + std::to_string(index);
        prop_->node_def.set_name(name.c_str());
        prop_->node_def.set_code(cgra::OP_STORE);
      } else if (prop_->node_def.op() == "store") {
        prop_->node_def.set_code(cgra::OP_NOP);
      } else if (prop_->node_def.op() == "and") {
        prop_->node_def.set_code(cgra::OP_ADD);
      } else if (prop_->node_def.op() == "nop") {
        prop_->node_def.set_code(cgra::OP_NOP);
      } else if (prop_->node_def.op() == "add") {
        prop_->node_def.set_code(cgra::OP_ADD);
      } else if (prop_->node_def.op() == "mul") {
        prop_->node_def.set_code(cgra::OP_MUL);
      } else if (prop_->node_def.op() == "or") {
        prop_->node_def.set_code(cgra::OP_OR);
      } else if (prop_->node_def.op() == "const" ||
                 prop_->node_def.op() == "icmp" ||
                 prop_->node_def.op() == "input") {
      } else {
        return errors::NotFound("UnRecogne Opcode:", prop_->node_def.op(),
                                ",Please Check it now");
      }
    }
  }
  return Status::OK();
}

Status DotParser::RemoveUnnecessaryLeafNodes(std::unique_ptr<Graph> &graph) {
  size_t nodes_num = graph->num_node_ids();
  for (unsigned i = 0; i < nodes_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto proper_ = graph->FindNodeProperties(node->Name());
      auto type = proper_->node_def.op();
      if (type != "store" && type != "load" && type != "icmp") {
        std::vector<Node *> delete_node;
        auto out_size = node->out_edges().size();
        auto input_size = node->in_edges().size();
        if (out_size == 0 && input_size == 0) {
          delete_node.push_back(node);
        }
        auto sub_node = node->out_edges()[0]->dst();
        if (input_size == 0 && out_size == 1 &&
            sub_node->in_edges().size() == 1 &&
            sub_node->out_edges().size() == 0) {
          delete_node.push_back(node);
          delete_node.push_back(sub_node);
        }
        std::for_each(delete_node.begin(), delete_node.end(),
                      [&graph](Node *item) { graph->RemoveNode(item); });
      }
    }
  }
  return Status::OK();
}

Status DotParser::ReplaceIcmpToSub(std::unique_ptr<Graph> &graph) {
  std::vector<Node *> nodes;
  graph->FindNodeOp("icmp", nodes);
  for (auto it : nodes) {
    auto in_num = it->in_edges().size();
    auto out_num = it->out_edges().size();
    if (in_num == 2 && out_num == 1) {
      auto prop_ = graph->FindNodeProperties(it->Name());
      prop_->node_def.set_op("sub");
      auto index = capture_num(it->Name());
      std::string name = "sub" + std::to_string(index);
      prop_->node_def.set_name(name.c_str());
    }
  }
  return Status::OK();
}

/*

*/
Status DotParser::RemovePhiNodes(std::unique_ptr<Graph> &graph) {
  auto nodes_num = graph->num_node_ids();
  for (unsigned i = 0; i < nodes_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto prop_ = graph->FindNodeProperties(node->Name());
      if (prop_->node_def.op() == "phi" || prop_->node_def.op() == "sel") {
        std::vector<Node *> src_nodes;
        std::vector<Node *> dst_nodes;

        Node *pre_node{nullptr};
        Node *dispose_node{nullptr};
        Node *sub_node{nullptr};

        // find all phi src node
        for (auto e : node->in_edges()) {
          auto n = e->src();
          for (auto pree : n->in_edges()) {
            if (pree->src() == node) {
              pre_node = n;
            }
          }
          if (n != pre_node) {
            src_nodes.push_back(n);
          }
        }

        // find all phi dst node
        for (auto e : node->out_edges()) {
          auto n = e->dst();
          for (auto sube : n->out_edges()) {
            if (sube->dst() == node) {
              sub_node = n;
              dispose_node = n;
            }
          }
          if (n != sub_node)
            dst_nodes.push_back(n);
        }

        if (dispose_node != nullptr) {
          std::vector<Node *> remove_nodes;

          auto dispose_prop_ = graph->FindNodeProperties(dispose_node->Name());
          // find icmp
          for (auto leaf : dispose_node->out_edges()) {
            auto leaf_node = leaf->dst();
            auto leaf_prop_ = graph->FindNodeProperties(leaf_node->Name());
            if (leaf_prop_->node_def.op() == "icmp") {
              auto sub_leaf_size = leaf_node->in_edges().size();
              for (unsigned k = 0; k < sub_leaf_size; k++) {
                auto sub_leaf_node = leaf_node->in_edges()[k]->src();
                if (sub_leaf_node != dispose_node) {
                  auto sub_leaf_prop_ =
                      graph->FindNodeProperties(sub_leaf_node->Name());
                  if (sub_leaf_prop_->node_def.op() == "input") {
                    // add store
                    // delete
                  } else if (sub_leaf_prop_->node_def.op() == "const") {
                    // add const
                    int data = sub_leaf_prop_->node_def.i();
                    this->loop_end = data;
                    //                    if (data < 255) {
                    //                      ImmediateDef *imdef =
                    //                      dispose_prop_->node_def.add_im();
                    //                      imdef->set_immediate(data);
                    //                      imdef->set_op("loop_num");
                    //                    } else {
                    //                      //////
                    //                      // add store
                    //                    }
                    graph->RemoveNode(sub_leaf_node);
                  }
                }
              }
              remove_nodes.push_back(leaf_node);
            }
          }

          // find const input
          for (auto leaf : dispose_node->in_edges()) {
            auto leaf_node = leaf->src();
            auto leaf_prop_ = graph->FindNodeProperties(leaf_node->Name());
            if (leaf_prop_->node_def.op() == "const") {
              int data = leaf_prop_->node_def.i();
              this->loop_offset = data;
              if (data < 255) {
                ImmediateDef *imdef = dispose_prop_->node_def.add_im();
                imdef->set_op("loop_offset");
                imdef->set_immediate(data);
              } else {
                // add store
                NodeDef node_def;
                node_def.set_i(data);
                node_def.set_op("load");
                auto index = graph->TraversalMaxNodeName("load") + 1;
                std::string replace_name = "load" + std::to_string(index);
                node_def.set_name(replace_name);
                node_def.set_code(cgra::OpCode::OP_LOAD);
                node_def.set_append_string("loop_offset");
                auto expand_node = graph->AddNode(new OpDef, node_def);
                auto input_index = dispose_node->in_edges().size();
                graph->AddEdge(expand_node, 0, dispose_node, input_index);
              }
              remove_nodes.push_back(leaf_node);
            } else if (leaf_prop_->node_def.op() == "input") {
            }
          }

          // delete phi const input
          for (auto n = src_nodes.begin(); n != src_nodes.end();) {
            auto p = graph->FindNodeProperties((*n)->Name());
            if (p->node_def.op() == "const") {
              int data = p->node_def.i();
              this->loop_begin = data;
              if (data < 255) {
                //                ImmediateDef *imdef =
                //                dispose_prop_->node_def.add_im();
                //                imdef->set_op("loop_begin");
                //                imdef->set_immediate(data);
              } else {
                //                // add store opcode to src nodes
                //                  NodeDef node_def;
                //                  node_def.set_i(data);
                //                  node_def.set_op("load");
                //                  auto index =
                //                  graph->TraversalMaxNodeName("load") + 1;
                //                  std::string replace_name = "load" +
                //                  std::to_string(index);
                //                  node_def.set_name(replace_name);
                //                  node_def.set_code(cgra::OpCode::OP_LOAD);
                //                  node_def.set_append_string("loop_begin");
                //                  auto expand_node = graph->AddNode(new OpDef,
                //                  node_def); auto input_index =
                //                  dispose_node->in_edges().size();
                //                  graph->AddEdge(expand_node, 0, dispose_node,
                //                  input_index);
              }
              graph->RemoveNode(*n);
              n = src_nodes.erase(n);
            } else {
              n++;
            }
          }

          // add delete phi node
          remove_nodes.push_back(node);
          // remove_nodes.push_back(dispose_node); //----------------------

          std::for_each(remove_nodes.begin(), remove_nodes.end(),
                        [&graph](Node *item) { graph->RemoveNode(item); });

          // add new edge
          auto src_node_num = src_nodes.size();
          auto dst_node_num = dst_nodes.size();
          for (unsigned k = 0; k < src_node_num; k++) {
            graph->AddEdge(src_nodes[k], 0, dispose_node, k);
          }

          for (unsigned k = 0; k < dst_node_num; k++) {
            graph->AddEdge(dispose_node, k, dst_nodes[k], 0);
          }
          // lay down ring
          // graph->AddEdge(dispose_node,dispose_node->out_edges().size(),dispose_node,dispose_node->in_edges().size());
          dispose_prop_->node_def.set_iterator(true);
        }
      }
    }
  }
  return Status::OK();
}

bool DotParser::AccelerateInvailedOp(std::unique_ptr<Graph> &graph) {
  std::vector<Node *> nodes;
  graph->FindNodeOp("nop", nodes);

  for (auto it : nodes) {
    auto in_edges = it->in_edges();
    auto out_edges = it->out_edges();
    size_t in_num = in_edges.size();
    size_t out_num = out_edges.size();
    std::vector<Node *> input_nodes;
    std::vector<Node *> output_nodes;
    for (auto e : in_edges) {
      input_nodes.push_back(e->src());
    }
    for (auto e : out_edges) {
      output_nodes.push_back(e->dst());
    }
    graph->RemoveNode(it);
    for (unsigned i = 0; i < in_num; i++) {
      for (unsigned j = 0; j < out_num; j++) {
        graph->AddEdge(input_nodes[i], i, output_nodes[j], j);
      }
    }
  }
  return true;
}

// Merge Similarity Const Value, solve the same load operation
bool DotParser::MergeSimilarConstStructs(std::unique_ptr<Graph> &graph) {
  bool status{false};
  std::vector<Node *> nodes;
  graph->FindNodeOp("const", nodes);
  std::unordered_map<int, std::vector<Node *>> map_;
  for (auto it : nodes) {
    std::shared_ptr<NodeProperties> props_ =
        graph->FindNodeProperties(it->Name());
    int val = props_->node_def.i();
    auto info = map_[val];
    info.push_back(it);
    map_[val] = info;
  }

  unsigned branch = 0;

  for (auto it : map_) {
    auto info = it.second;
    auto size = info.size();
    if (size > 1) {
      auto src_node = info[0];
      // statistics info[0] how many branch
      branch += info[0]->out_edges().size();
      // delete same const node
      for (unsigned i = 1; i < size; i++) {
        auto out_edges = info[i]->out_edges();
        size_t out_num = out_edges.size();
        std::vector<Node *> output_nodes;
        for (auto e : out_edges) {
          output_nodes.push_back(e->dst());
          graph->AddEdge(src_node, 0, e->dst(), branch);
          branch++;
        }
        graph->RemoveNode(info[i]);
      }
      status = true;
    }
  }
  return status;
}

/// \First Merge Same Const Const code
/// Second Extract ImmediateValue
bool DotParser::ExtractConstImmediateValue(std::unique_ptr<Graph> &graph) {
  size_t nodes_num = graph->num_node_ids();
  for (size_t i = 0; i < nodes_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto proper_ = graph->FindNodeProperties(node->Name());
      if (proper_->node_def.const_()) {
        int data = proper_->node_def.i();
        if (data < 255 || proper_->node_def.im_reg() == true) {
          // store to immediate reg
          // query sub node
          // proper_->node_def.set_im_reg(true);
          // if (proper_->node_def.im_reg()) {
          //
          for (unsigned j = 0; j < node->out_edges().size(); j++) {
            auto sub_node = node->out_edges()[j]->dst();
            auto sub_propert_ = graph->FindNodeProperties(sub_node->Name());
            sub_propert_->node_def.set_im_reg(true);
            if (proper_->node_def.im_reg()) {
              int immediate_size = proper_->node_def.im_size();
              for (unsigned k = 0; k < immediate_size; k++) {
                auto im = sub_propert_->node_def.add_im();
                im->set_op(proper_->node_def.im(k).op());
                im->set_immediate(proper_->node_def.im(k).immediate());
              }
            } else {
              int immediate_size = sub_propert_->node_def.im_size();
              auto im = sub_propert_->node_def.add_im();
              im->set_op(sub_propert_->node_def.op());
              im->set_immediate(data);
            }
          }
          // delete const node
          graph->RemoveNode(node);
        } else {
          // replace this const operation to store operation
          proper_->node_def.set_op("load");
          auto index = graph->TraversalMaxNodeName("load") + 1;
          std::string replace_name = "load" + std::to_string(index);
          proper_->node_def.set_name(replace_name);
          proper_->node_def.set_im_reg(false);
          proper_->node_def.clear_im();
          proper_->node_def.set_code(OP_LOAD);
          proper_->node_def.set_type(DT_INT32);
        }
      }
    }
  }
  return true;
}

bool DotParser::MergeRedundancyOp(std::unique_ptr<Graph> &graph) {
  size_t nodes_num = graph->num_node_ids();
  bool status{false};
  for (size_t i = 0; i < nodes_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto proper_ = graph->FindNodeProperties(node->Name());
      if ((proper_->node_def.op() == "mul") ||
          (proper_->node_def.op() == "sll")) {
        size_t out_edges = node->out_edges().size();
        if (out_edges == 1) {
          auto sub_node = node->out_edges()[0]->dst();
          auto sub_propert_ = graph->FindNodeProperties(sub_node->Name());
          if (sub_propert_->node_def.op() == "add") {
            /* save (mul or sll) inputedge src node  save add inputedge src node
               save add outputedge dst node
               delete (mul or sll) and add operation
               add mac operation
               add edges */
            std::vector<Node *> src_nodes;
            std::vector<Node *> dst_nodes;
            for (auto it : node->in_edges())
              src_nodes.push_back(it->src());

            for (auto it : sub_node->in_edges()) {
              if (it->src()->Name() != node->Name()) {
                src_nodes.push_back(it->src());
              }
            }

            for (auto it : sub_node->out_edges())
              dst_nodes.push_back(it->dst());

            NodeDef node_def;
            bool im_status = proper_->node_def.im_reg();
            if (proper_->node_def.im_reg() | sub_propert_->node_def.im_reg()) {
              node_def.set_im_reg(true);
              size_t node_im_num = proper_->node_def.im_size();
              size_t sub_node_im_num = sub_propert_->node_def.im_size();
              for (unsigned t = 0; t < node_im_num; t++) {
                auto im = node_def.add_im();
                im->set_op(proper_->node_def.im(t).op());
                im->set_immediate(proper_->node_def.im(t).immediate());
              }

              for (unsigned u = 0; u < sub_node_im_num; u++) {
                auto im = node_def.add_im();
                im->set_op(sub_propert_->node_def.im(u).op());
                im->set_immediate(sub_propert_->node_def.im(u).immediate());
              }
            }
            auto index = graph->TraversalMaxNodeName("mac") + 1;
            node_def.set_op("mac");
            std::string create_node_name = "mac" + std::to_string(index);
            node_def.set_name(create_node_name.c_str());
            auto regenerate_node = graph->AddNode(new OpDef, node_def);
            graph->RemoveNode(node);
            graph->RemoveNode(sub_node);

            auto src_node_num = src_nodes.size();
            auto dst_node_num = dst_nodes.size();
            for (unsigned k = 0; k < src_node_num; k++) {
              graph->AddEdge(src_nodes[k], 0, regenerate_node, k);
            }

            for (unsigned k = 0; k < dst_node_num; k++) {
              graph->AddEdge(regenerate_node, k, dst_nodes[k], 0);
            }

            status = true;
          }
        }
      }
    }
  }
  return status;
}

bool DotParser::PrintDfgFileSystem(std::unique_ptr<Graph> &graph,
                                   std::string filename) {
  std::fstream file_output;
  file_output.open(filename, std::ios::out);

  file_output << "digraph G { \n";
  size_t nodes_size = graph->num_node_ids();
  for (unsigned i = 0; i < nodes_size; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto proper_ = graph->FindNodeProperties(node->Name());
      std::ostringstream str;
      str << proper_->node_def.name() << "[opcode=" << proper_->node_def.op()
          << "]";

      if (proper_->node_def.im().size()) {
        str << "[shape=hexagon]";
        bool iterator_status = proper_->node_def.iterator();
        if (iterator_status == true) {
          str << "[iterator=1]";
        }
        str << "[";
        std::for_each(proper_->node_def.im().begin(),
                      proper_->node_def.im().end(), [&str](ImmediateDef imdef) {
                        str << "imm=" << std::to_string(imdef.immediate());
                      });
        str << "];\n";
      } else {
        str << ";\n";
      }
      file_output << str.str();
    }
  }

  size_t edges_size = graph->num_edge_ids();
  for (unsigned i = 0; i < edges_size; i++) {
    const Edge *edge = graph->FindEdgeId(i);
    if (edge != nullptr) {
      Node *src = edge->src();
      Node *dst = edge->dst();
      if (src->id() < 0 || dst->id() < 0)
        continue;
      auto src_proper_ = graph->FindNodeProperties(src->Name());
      auto dst_proper_ = graph->FindNodeProperties(dst->Name());
      file_output << src_proper_->node_def.name() << "->"
                  << dst_proper_->node_def.name()
                  << "[operand =" << edge->dst_input() << "]";
      if (src_proper_->node_def.const_() == true) {
        file_output << "[const=" << src_proper_->node_def.i() << "];\n";
      } else {
        file_output << "\n";
      }
    }
  }
  file_output << "} \n";
  file_output.close();
  return true;
}

Status DotParser::UnfoldImmediateValue(std::unique_ptr<Graph> &graph) {
  auto node_num = graph->num_node_ids();
  for (unsigned i = 0; i < node_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto prop_ = graph->FindNodeProperties(node->Name());
      auto size = prop_->node_def.im_size();
      if (size > 1) {
        // type const  find is exist this node
        // no create node and add edge

        for (unsigned int i = 1; i < size; i++) {
          auto max_abs_num = graph->TraversalMaxNodeName("load") + 1;
          std::string name = "load" + std::to_string(max_abs_num);
          auto append_string = prop_->node_def.im(i).op();
          auto data = prop_->node_def.im(i).immediate();
          NodeDef node_def;
          node_def.set_i(data);
          node_def.set_op("load");
          node_def.set_name(name);
          node_def.set_code(cgra::OpCode::OP_LOAD);
          node_def.set_append_string(append_string);
          auto regenerate_node = graph->AddNode(new OpDef, node_def);
          graph->AddNode(new OpDef, node_def);
          auto input_index = node->in_edges().size();
          graph->AddEdge(regenerate_node, 0, node, input_index);
        }
      }
    }
  }
  return Status::OK();
}

Status DotParser::PrintNodeParameter(std::unique_ptr<Graph> &graph) {
  auto node_num = graph->num_node_ids();
  for (unsigned i = 0; i < node_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      std::ostringstream str;
      auto prop_ = graph->FindNodeProperties(node->Name());
      str << "Node name:" << prop_->node_def.name()
          << " Node op:" << prop_->node_def.op();
      auto im_num = prop_->node_def.im_size();
      str << " Node im_num:" << im_num << " ";
      for (unsigned j = 0; j < im_num; j++) {
        str << " sub op:" << prop_->node_def.im(j).op()
            << " sub data:" << prop_->node_def.im(j).immediate();
      }
      std::cout << str.str() << std::endl;
    }
  }
  return Status::OK();
}

/*
  seek 	the abs operation max index
  seek prev operation icmp
  seek prev operation sub
  seel prev operation sub
  delete four operation
  add new operation
*/
Status DotParser::AnalysisFabsFunction(std::unique_ptr<Graph> &graph) {
  auto node_num = graph->num_node_ids();
  auto max_abs_num = graph->TraversalMaxNodeName("abs") + 1;

  for (unsigned i = 0; i < node_num; i++) {
    Node *node = graph->FindNodeId(i);
    if (node != nullptr) {
      auto prop_ = graph->FindNodeProperties(node->Name());
      if (prop_->node_def.op() == "sel") {
        // find sel opreation out_edge
        std::vector<Node *> dst_node;
        std::vector<Node *> src_node;
        std::vector<Node *> delete_node;
        bool is_abs = true;
        bool is_reg = false;
        int im_data = 0;
        for (auto e : node->out_edges()) {
          dst_node.push_back(e->dst());
        }
        for (auto e : node->in_edges()) {
          Node *tmp = e->src();
          if (tmp->Op() != "icmp" || tmp->Op() != "sub") {
            is_abs = true;
          } else if (tmp->Op() == "icmp") {
            std::string op_name = tmp->Name();
            auto propert_ = graph->FindNodeProperties(op_name);
            if ((propert_->node_def.im_reg() == true) ||
                propert_->node_def.im(0).immediate() == 0) {
              is_abs = true;
              delete_node.push_back(tmp);
            } else
              is_abs = false;
          } else if (tmp->Op() == "sub") {
            /// judge sub
            /// need repair bug
            std::string op_name = tmp->Name();
            auto propert_ = graph->FindNodeProperties(op_name);
            if (propert_->node_def.im_reg() == true &&
                propert_->node_def.im(0).immediate() == -1) {
              delete_node.push_back(tmp);
              is_abs = true;
            } else if (propert_->node_def.im_reg() == true &&
                       propert_->node_def.im(0).immediate() != -1) {
              is_reg = true;
              im_data = propert_->node_def.im(0).immediate();
              for (auto e : tmp->in_edges()) {
                src_node.push_back(e->src());
              }
            } else {
              delete_node.push_back(tmp);
              for (auto e : tmp->in_edges()) {
                src_node.push_back(e->src());
              }
            }
          }
        }

        if (is_abs == true) {
          NodeDef node_def;
          std::string regenerate_name = "abs" + std::to_string(max_abs_num);
          node_def.set_name(regenerate_name);
          node_def.set_op("abs");
          node_def.set_code(OP_ABS);
          if (is_reg) {
            node_def.set_im_reg(true);
            auto im = node_def.add_im();
            im->set_op("abs");
            im->set_immediate(im_data);
          }

          for (auto d : delete_node) {
            graph->RemoveNode(d);
          }

          auto regenerate_node = graph->AddNode(new OpDef, node_def);
          auto dst_node_num = dst_node.size();
          auto src_node_num = src_node.size();
          for (unsigned nn = 0; nn < dst_node_num; nn++) {
            graph->AddEdge(regenerate_node, 0, dst_node[nn], nn);
          }
          for (unsigned nn = 0; nn < src_node_num; nn++) {
            graph->AddEdge(src_node[nn], nn, regenerate_node, 0);
          }
        }
      }
    }
  }
  return Status::OK();
}

Status DotParser::StoreJsonFile(std::unique_ptr<Graph> &graph,
                                const std::string store_path,
                                const unsigned index) {

  std::string filename =
      store_path + "/" + "loop" + std::to_string(index) + ".json";
  std::ofstream f(filename, std::ios::out);
  using namespace nlohmann;
  json accelerate_json;
  accelerate_json["loop_begin"] = this->loop_begin;
  accelerate_json["loop_end"] = this->loop_end;
  accelerate_json["loop_offset"] = this->loop_offset;

  // reserve fout space
  f << std::setw(4) << accelerate_json << std::endl;
  f.close();
  f.flush();
  return Status::OK();
}

} // namespace cgra

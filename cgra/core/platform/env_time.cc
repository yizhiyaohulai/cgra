#include "cgra/core/platform/env_time.h"
#include <chrono>
namespace cgra{

class GenerateEnvTime : public EnvTime{
public:
    virtual uint64 NowMicros(){
        std::chrono::system_clock::time_point nowTp = std::chrono::system_clock::now();
        std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds> tp = \
                std::chrono::time_point_cast<std::chrono::microseconds>(nowTp);
        //time_t tt = std::chrono::system_clock::to_time_t(tp);
        return tp.time_since_epoch().count();
    }
};

EnvTime* EnvTime::Default() {
   EnvTime* env_time = new GenerateEnvTime();
   return env_time;
};

}; //namespace cgra

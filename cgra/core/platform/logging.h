#ifndef CGRA_PLATFORM_LOGGING_H_
#define CGRA_PLATFORM_LOGGING_H_

#include "cgra/core/platform/platform.h"  // To pick up PLATFORM_define

#if defined(PLATFORM_GOOGLE) || defined(PLATFORM_GOOGLE_ANDROID) || \
    defined(GOOGLE_LOGGING)
#include "tensorflow/core/platform/google/build_config/logging.h"
#else
#include "cgra/core/platform/default/logging.h"
#endif

namespace cgra {

namespace internal {
// Emit "message" as a log message to the log for the specified
// "severity" as if it came from a LOG call at "fname:line"
void LogString(const char* fname, int line, int severity,
               const string& message);
}  // namespace internal

}  // namespace cgra

#endif  // TENSORFLOW_PLATFORM_LOGGING_H_

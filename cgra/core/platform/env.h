#ifndef CGRA_CORE_PLATFORM_ENV_H
#define CGRA_CORE_PLATFORM_ENV_H

#include "cgra/core/platform/macros.h"
#include <functional>

namespace cgra {

/// Represents a thread used to run a Tensorflow function.
class Thread {
 public:
  Thread() {}

  /// Blocks until the thread of control stops running.
  ~Thread() {};

 private:
  CGRA_DISALLOW_COPY_AND_ASSIGN(Thread);
};

class Env {
public:
	virtual ~Env() = default;
	//static Env* Default();
	Thread* StartThread(const std::string& name, std::function<void()> fn);
};

}  // namespace cgra

#endif  // CGRA_CORE_PLATFORM_ENV_H

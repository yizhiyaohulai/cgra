#ifndef CGRA_CORE_PLATFORM_HOSTINFO_H
#define CGRA_CORE_PLATFORM_HOSTINFO_H
#include <string>

namespace cgra {

namespace port {
std::string Hostname();
} // namespace port

} // namespace cgra

#endif // CGRA_CORE_PLATFORM_HOSTINFO_H

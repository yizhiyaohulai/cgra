#ifndef CGRA_CORE_PLATFORM_DEFAULT_INTEGRAL_TYPES_H
#define CGRA_CORE_PLATFORM_DEFAULT_INTEGRAL_TYPES_H

namespace cgra {

typedef signed char int8;
typedef short int16;
typedef int int32;
typedef long int64;

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long uint64;
};
#endif  // endif GREP_CORE_PLATFORM_DEFAULT_INTEGRAL_TYPES_H

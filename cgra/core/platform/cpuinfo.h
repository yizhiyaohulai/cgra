#ifndef CGRA_CORE_PLATFORM_CPUINFO_H
#define CGRA_CORE_PLATFORM_CPUINFO_H

namespace cgra {

namespace port {
int NumSchedulableCPUs();
}

} // namespace cgra

#endif

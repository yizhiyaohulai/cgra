#ifndef CGRA_CORE_PLATFORM_WINDOWS_PORT_H
#define CGRA_CORE_PLATFORM_WINDOWS_PORT_H

#include <string>

namespace cgra {

namespace port {
	std::string Hostname();
	int NumSchedulableCPUs();
}

}  // namspace cgra

#endif // CGRA_CORE_PLATFORM_WINDOWS_PORT_H

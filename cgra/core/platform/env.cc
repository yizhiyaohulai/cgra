#include "cgra/core/platform/env.h"
#include <thread>

namespace cgra {

class StdThread : public Thread {
 public:
  // name are both ignored.
  StdThread(const std::string& name, std::function<void()> fn) : thread_(fn) {}
  ~StdThread() { thread_.join(); }

 private:
  std::thread thread_;
};

Thread* Env::StartThread(const std::string& name, std::function<void()> fn) {
  return new StdThread(name, fn);
}

} //namespace cgra

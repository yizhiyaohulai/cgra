#include "cgra/llvm/codegen/llvm_common.h"
#include "llvm-c/Core.h"

#include "cgra/core/platform/logging.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/TextDiagnosticBuffer.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/FrontendTool/Utils.h"
#include "clang/Lex/PreprocessorOptions.h"
#include <clang/CodeGen/BackendUtil.h>
#include <clang/CodeGen/CodeGenAction.h>
#include <llvm/MC/SubtargetFeature.h>
#include <llvm/Target/TargetOptions.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include "cgra/llvm/optimize/optimize.h"


using namespace llvm;
using namespace clang;
using namespace clang::frontend;

namespace cgra {

//char *program_src = "int main() \n"
//                    "{\n"
//                    "  int i;\n"
//                    "  i = 10;\n"
//                    "  return i;\n"
//                    "}\n";

//char *program_src1 = "//#include <stdio.h>\n"
//                     "  volatile int * a;//[N] = {1,2,3,4,5,6,7,8,9,10};\n"
//                     "  volatile int * n; \n"
//                     "  // Simple accumulation \n"
//                     "  int main() {\n"
//                     "      int N = *n;\n"
//                     "      int sum = 0;\n"
//                     "      int i;\n"
//                     "      for (i = 0; i < N; i++) {\n"
//                     "          //DFGLoop: loop\n"
//                     "          sum += a[i];\n"
//                     "      }\n"
//                     "  return sum;\n"
//                     "}\n";

llvm::MemoryBufferRef read_source_file(const std::string &input) {
  using FileOrError = llvm::ErrorOr<std::unique_ptr<llvm::MemoryBuffer>>;
  FileOrError inputFileOrErr =
      llvm::MemoryBuffer::getFileOrSTDIN(input, -1, false);
  if (!inputFileOrErr) {
    LOG(ERROR) << "Input file is not exist\n";
  }
  return inputFileOrErr.get()->getMemBufferRef();
}

int generate_ir_file(const std::string &content,
                     llvm::MemoryBufferRef &memorybuffer) {

  CompilerInstance Compiler;
  auto Invocation = std::make_shared<CompilerInvocation>();

  std::string log;
  llvm::raw_string_ostream s_log(log);

  // compiler options:
  std::vector<std::string> opts_array;
  opts_array.push_back("input.cc");
  opts_array.push_back("-v");
  opts_array.push_back("-xc");
  opts_array.push_back("-O3");
  opts_array.push_back("-emit-llvm");
  //  opts_array.push_back("-fno-vectorize");
  //  opts_array.push_back("-fno-slp-vectorize");
  //  opts_array.push_back("-fno-unroll-loops");

  std::vector<const char *> opts_carray;
  for (unsigned i = 0; i < opts_array.size(); i++) {
    opts_carray.push_back(opts_array.at(i).c_str());
  }

  llvm::IntrusiveRefCntPtr<clang::DiagnosticIDs> DiagID;
  llvm::IntrusiveRefCntPtr<clang::DiagnosticOptions> DiagOpts;
  clang::TextDiagnosticBuffer *DiagsBuffer;

  DiagID = new clang::DiagnosticIDs();
  DiagOpts = new clang::DiagnosticOptions();
  DiagsBuffer = new clang::TextDiagnosticBuffer();

  clang::DiagnosticsEngine Diags(DiagID, &*DiagOpts, DiagsBuffer);

  bool Success;
  Success = clang::CompilerInvocation::CreateFromArgs(
      *Invocation, opts_carray.data(), opts_carray.data() + opts_carray.size(),
      Diags);
  if (!Success) {
    LOG(ERROR) << "invalide option" << log << "\n";
    return 1;
  }
//  Compiler.createFileManager();
//  Compiler.createFileManager()->getFile("/home/hujb/source/cgra-thu/benchmarks/microbench/sum/sum.c");
//  Compiler.createSourceManager(Compiler.getFileManager());
  Compiler.getHeaderSearchOpts().UseBuiltinIncludes = false;
  Compiler.getHeaderSearchOpts().UseStandardSystemIncludes = true;
//  Compiler.getHeaderSearchOpts().AddPath("/usr/include/linux", clang::frontend::Angled, false, false);
  Compiler.getHeaderSearchOpts().AddPath("/usr/include", clang::frontend::Angled, false, false);
//  Compiler.getHeaderSearchOpts().AddPath("/usr/include/c++/5/tr1", clang::frontend::Angled, false, false);
  //Compiler.getHeaderSearchOpts().AddPath("/usr/include/i386-linux-gnu", clang::frontend::Angled, false, false);
//  Compiler.getHeaderSearchOpts().AddPath("/usr/include/x86_64-linux-gnu", clang::frontend::Angled, false, false);
//  Compiler.getHeaderSearchOpts().AddPath("/usr/include/c++/5", clang::frontend::Angled, false, false);
  Compiler.getHeaderSearchOpts().AddPath("/usr/lib/gcc/x86_64-linux-gnu/5/include/", clang::frontend::Angled, false, false);

  Compiler.getLangOpts().NoBuiltin = true;
  //Compiler.getTargetOpts().Triple = "i386-unknown-linux-gnu";
  Compiler.getTargetOpts().Triple = llvm::sys::getDefaultTargetTriple();
  // Compiler.getFrontendOpts().ProgramAction = EmitBC;
  Compiler.getFrontendOpts().ProgramAction = EmitLLVM;
  Compiler.getFrontendOpts().Inputs.push_back(
      FrontendInputFile("input.cc", InputKind::CXX));
  // Compiler.getPreprocessorOpts().addRemappedFile("input.cc",
  // llvm::MemoryBuffer::getMemBuffer(program_src1).release());
  // Compiler.getPreprocessorOpts().addRemappedFile("input.cc",llvm::MemoryBuffer::getMemBuffer(memorybuffer,false).release());

  using FileOrError = llvm::ErrorOr<std::unique_ptr<llvm::MemoryBuffer>>;
  FileOrError inputFileOrErr =
      llvm::MemoryBuffer::getFileOrSTDIN("/home/hujb/source/cgra-thu/benchmarks/microbench/sum/sum.c", -1, false);
  if (!inputFileOrErr) {
      LOG(ERROR) << "Input file is not exist\n";
  }


  auto data = inputFileOrErr.get()->getMemBufferRef().getBuffer().data();
  LOG(WARNING) << data << "\n";

  Compiler.getPreprocessorOpts().addRemappedFile(
      "input.cc", llvm::MemoryBuffer::getMemBuffer(data).release());

  // Compiler.createDiagnostics();
  Compiler.createDiagnostics(new clang::TextDiagnosticPrinter(
      s_log, &Invocation->getDiagnosticOpts()));

  SmallVector<char, 256> IRBuffer;
  std::unique_ptr<raw_pwrite_stream> IRStream(
      new raw_svector_ostream(IRBuffer));

  Compiler.setOutputStream(std::move(IRStream));
  //   Compiler.setInvocation(std::move(Invocation));
  // Success = ExecuteCompilerInvocation(&Compiler);
  std::unique_ptr<FrontendAction> act = CreateFrontendAction(Compiler);
  if (!Compiler.ExecuteAction(*act)) {
    LOG(ERROR) << "failed to compile:" << log << "\n";
    return 2;
  }
  std::cout << IRBuffer.data() << std::endl;
  if (!StringRef(IRBuffer.data()).startswith("LLVM")) {
    LOG(ERROR) << "not a LL file: \n";
  }

  std::string str = IRBuffer.data();
  LOG(INFO) << str.size() << "\n";

  option_ir_file(str);
  aa();
  return 0;
}

} // namespace cgra

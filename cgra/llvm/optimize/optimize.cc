#include "cgra/llvm/optimize/optimize.h"

#include <llvm/ExecutionEngine/MCJIT.h>

#include <llvm/Analysis/TargetTransformInfo.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/Support/SourceMgr.h>

#include <llvm/IR/Argument.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DIBuilder.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Intrinsics.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/MDBuilder.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Value.h>
#include <llvm/IR/Verifier.h>

#include <llvm/IR/LegacyPassManager.h>
#include <llvm/Transforms/IPO.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/Transforms/Utils/Cloning.h>
#include <llvm/Transforms/Utils/ModuleUtils.h>

#include <llvm/CodeGen/TargetLoweringObjectFileImpl.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Support/Casting.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Target/TargetOptions.h>

#include <llvm/Linker/Linker.h>


#include "llvm-c/Core.h"

#include "cgra/core/platform/logging.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/TextDiagnosticBuffer.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/FrontendTool/Utils.h"
#include "clang/Lex/PreprocessorOptions.h"
#include <clang/CodeGen/BackendUtil.h>
#include <clang/CodeGen/CodeGenAction.h>
#include <llvm/MC/SubtargetFeature.h>
#include <llvm/Target/TargetOptions.h>
#include <llvm/Support/PluginLoader.h>

#include <fstream>
#include <iostream>
#include <sstream>


using namespace llvm;
using namespace clang;
using namespace clang::frontend;

namespace  {
class DetectLoop: public FunctionPass {
public:
    DetectLoop() : FunctionPass(ID) {}

    static char ID;

    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
        //AU.addRequired<LoopInfoWrapperPass>();
    }

    virtual bool runOnFunction(Function &F) {
        errs() << "DetectLoop: ModulePass entry Function!" << "\n";
        return false;
    }
};
char DetectLoop::ID = 0;

class CgraTestPass: public ModulePass {
public:
    CgraTestPass() : ModulePass(ID){};
    static char ID;
    virtual bool runOnModule(llvm::Module &M) {
        errs() << "CgraTestPass: ModulePass entry Function!" << "\n";
        return true;
    }
};
char CgraTestPass::ID = 0;
}
namespace cgra {
void addOptPasses(
    llvm::legacy::PassManagerBase &passes,
    llvm::legacy::FunctionPassManager &fnPasses,
    llvm::TargetMachine *machine
    ) {
    llvm::PassManagerBuilder builder;
    builder.OptLevel = 3;
    builder.SizeLevel = 0;
    builder.Inliner = llvm::createFunctionInliningPass(3, 0, false);
    builder.LoopVectorize = true;
    builder.SLPVectorize = true;
    machine->adjustPassManager(builder);

    builder.populateFunctionPassManager(fnPasses);
    builder.populateModulePassManager(passes);
}

void addLinkPasses(llvm::legacy::PassManagerBase &passes) {
    llvm::PassManagerBuilder builder;
    builder.VerifyInput = true;
    builder.Inliner = llvm::createFunctionInliningPass(3, 0, false);
    builder.populateLTOPassManager(passes);
}

void optimizeModule(llvm::TargetMachine *machine, llvm::Module *module) {
    module->setTargetTriple(machine->getTargetTriple().str());
    module->setDataLayout(machine->createDataLayout());

    llvm::legacy::PassManager passes;
    passes.add(new llvm::TargetLibraryInfoWrapperPass(machine->getTargetTriple()));
    passes.add(llvm::createTargetTransformInfoWrapperPass(machine->getTargetIRAnalysis()));

    llvm::legacy::FunctionPassManager fnPasses(module);
    fnPasses.add(llvm::createTargetTransformInfoWrapperPass(machine->getTargetIRAnalysis()));

    addOptPasses(passes, fnPasses, machine);
    addLinkPasses(passes);

    fnPasses.doInitialization();
    for (llvm::Function &func : *module) {
        fnPasses.run(func);
    }
    fnPasses.doFinalization();

    passes.add(llvm::createVerifierPass());
    passes.add(new DetectLoop());
    passes.run(*module);
}

std::string getFeaturesStr(){
    llvm::SubtargetFeatures Features;
    llvm::StringMap<bool> feature_map;
    if (llvm::sys::getHostCPUFeatures(feature_map)) {
        for (auto &feature : feature_map) {
            Features.AddFeature(feature.first(), feature.second);
        }
    }
    return Features.getString();
}

// Returns the TargetMachine instance or zero if no triple is provided.
static TargetMachine* GetTargetMachine(Triple TheTriple, StringRef CPUStr,
                                       StringRef FeaturesStr) {
    std::string Error;
    const Target *TheTarget = llvm::TargetRegistry::lookupTarget(TheTriple.getTriple(),
                                                                 Error);
    // Some modules don't specify a triple, and this is okay.
    if (!TheTarget) {
        return nullptr;
    }
    llvm::TargetOptions target_options;
    auto reloc_model = llvm::Optional<llvm::Reloc::Model>();
    return TheTarget->createTargetMachine(TheTriple.getTriple(), CPUStr,
                                          FeaturesStr, target_options, reloc_model,
                                          llvm::CodeModel::Medium, CodeGenOpt::Aggressive);
}

int option_ir_file(const std::string str) {
    LLVMContext Context;
    InitializeAllTargets();
    InitializeAllTargetMCs();
    InitializeAllAsmPrinters();
    InitializeAllAsmParsers();

    // Initialize passes
    PassRegistry &Registry = *PassRegistry::getPassRegistry();
    initializeCore(Registry);
    initializeCoroutines(Registry);
    initializeScalarOpts(Registry);
    initializeObjCARCOpts(Registry);
    initializeVectorization(Registry);
    initializeIPO(Registry);
    initializeAnalysis(Registry);
    initializeTransformUtils(Registry);
    initializeInstCombine(Registry);
    initializeAggressiveInstCombine(Registry);
    initializeInstrumentation(Registry);
    initializeTarget(Registry);


    // For codegen passes, only passes that do IR to IR transformation are
    // supported.
    initializeExpandMemCmpPassPass(Registry);
    initializeScalarizeMaskedMemIntrinPass(Registry);
    initializeCodeGenPreparePass(Registry);
    initializeAtomicExpandPass(Registry);
    initializeRewriteSymbolsLegacyPassPass(Registry);
    initializeWinEHPreparePass(Registry);
    initializeDwarfEHPreparePass(Registry);
    initializeSafeStackLegacyPassPass(Registry);
    initializeSjLjEHPreparePass(Registry);
    initializePreISelIntrinsicLoweringLegacyPassPass(Registry);
    initializeGlobalMergePass(Registry);
    initializeIndirectBrExpandPassPass(Registry);
    initializeInterleavedAccessPass(Registry);
    initializeEntryExitInstrumenterPass(Registry);
    initializePostInlineEntryExitInstrumenterPass(Registry);
    initializeUnreachableBlockElimLegacyPassPass(Registry);
    initializeExpandReductionsPass(Registry);
    initializeWasmEHPreparePass(Registry);
    initializeWriteBitcodePassPass(Registry);

    //struct PluginLoader loader;
    //loader = "/home/hujb/source/cgra/cgra/contrib/cmake/build/libcgra_llvm_passes.so";

    SMDiagnostic Err;

    llvm::MemoryBufferRef ref = llvm::MemoryBuffer::getMemBuffer(str)->getMemBufferRef();

    //std::unique_ptr<llvm::Module> M = llvm::parseIR(ref,Err,Context);
    std::unique_ptr<llvm::Module> M = llvm::parseIRFile("/home/hujb/source/cgra-thu/benchmarks/microbench/add/add.ll",Err,Context);
    if (!M) {
        LOG(ERROR) << "ParseIR File failure "  << Err.getMessage().str() << "\n";
        return 1;
    }

    Triple ModuleTriple(M->getTargetTriple());
    std::string CPUStr, FeaturesStr;
    TargetMachine *Machine = nullptr;

    if (ModuleTriple.getArch()) {
        CPUStr = llvm::sys::getHostCPUName();
        FeaturesStr = getFeaturesStr();
        Machine = GetTargetMachine(ModuleTriple, CPUStr, FeaturesStr);
    }

    if (Machine == nullptr) {
        LOG(ERROR) << "Failed to create target machine \n";
    }

    LOG(INFO) << "llvm optimize modules \n";
    optimizeModule(Machine,M.get());
    return 0;
}

int aa(){
    std::cout << "hello aa" << std::endl;
    return  1;
}
}


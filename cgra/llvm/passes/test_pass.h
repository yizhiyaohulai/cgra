#ifndef CGRA_LLVM_PASSES_TEST_PASS_H
#define CGRA_LLVM_PASSES_TEST_PASS_H

#include "llvm/DebugInfo/DWARF/DWARFDebugLine.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/DebugLoc.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

using namespace llvm;

namespace {

class CgraTestPass : public ModulePass {
public:
  static char ID;
  int user_data;
  explicit CgraTestPass();
  virtual bool runOnModule(llvm::Module &M);
};

} // namespace
#endif

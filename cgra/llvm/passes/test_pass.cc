#include "cgra/llvm/passes/test_pass.h"

namespace {

CgraTestPass::CgraTestPass() : ModulePass(ID), user_data(-1) {}

bool CgraTestPass::runOnModule(llvm::Module &M) {
  user_data = 100;
  errs() << "CgraTestPass: ModulePass entry Function!" << "\n";
  return true;
}

} // namespace

char CgraTestPass::ID = 0;
static RegisterPass<CgraTestPass> F0("cgratest", "cgra test Pass", false,
                                     true);

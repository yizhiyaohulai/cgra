#include "llvm/DebugInfo/DWARF/DWARFDebugLine.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/DebugLoc.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

using namespace llvm;

namespace {
class ConstantReplacementPass : public ModulePass {
 public:
  static char ID;

  ConstantReplacementPass() : ModulePass(ID) { srand(time(NULL)); }

  virtual bool runOnModule(llvm::Module &M) {
    //// list to collect instruction
    ///*
    // * You cannot change an iterator while iterating over it
    // To remove instructions or modify, first collect the instructions to
    // remove/modify
    // *
    // * **/
    // This are the list of load to delete
    SmallVector<Instruction *, 128> *WorkListLoad =
        new SmallVector<Instruction *, 128>();
    // This is the list of instruction to modify the source operand
    SmallVector<Instruction *, 128> *WorkListUserOfLoad =
        new SmallVector<Instruction *, 128>();

    for (auto gv_iter = M.global_begin(); gv_iter != M.global_end();
         gv_iter++) {
      /* GLOBAL DATA INFO*/
      GlobalVariable *gv = &*gv_iter;
      Constant *const_gv = gv->getInitializer();
      ConstantFP *Fvalue;
      float gv_fpval;
      if (!const_gv->isNullValue()) {
        if (ConstantFP *constfp_gv =
                llvm::dyn_cast<llvm::ConstantFP>(const_gv)) {
          gv_fpval = (constfp_gv->getValueAPF()).convertToFloat();
          Fvalue = constfp_gv;
          errs() << gv_fpval << "\n";  // Value retrieved here
                                       // Collect Instruction to modify
        }

        //if (ConstantInt *constint_gv =
        //        llvm::dyn_cast<llvm::ConstantInt>(const_gv)) {
        //  if (constint_gv->getBitWidth() <= 32) {
        //    int64_t int_value = constint_gv->getSExtValue();
        //    errs() << int_value << "\n";
        //  }
        //}

        for (auto user_of_gv : gv->users()) {
          // Collect in a worklist
          if (llvm::Instruction *instr_ld_gv =
                  llvm::dyn_cast<Instruction>(user_of_gv)) {
            if (LoadInst *loadInst = dyn_cast<LoadInst>(instr_ld_gv)) {
              WorkListLoad->push_back(loadInst);
              for (auto user_of_load : loadInst->users()) {
                user_of_load->dump();
                Instruction *instruction1 = dyn_cast<Instruction>(user_of_load);
                errs() << "getOpcode:" << instruction1->getOpcode() << "  Value:" << gv_fpval;
               // errs() << "getOpcodeName:" << instruction1->getOpcodeName();
                instruction1->dump();
                // instruction1->setOperand(0, Fvalue);
                // instruction1->dump();
                // if(Instruction *instruction1 =
                // dyn_cast<Instruction>(user_of_load))
                WorkListUserOfLoad->push_back(instruction1);
                // instruction1->setOperand(0, Fvalue);
                // instruction1->dump();
              }
            }
          }
        }

        //// Modify Here
        // while (!WorkListUserOfLoad->empty()) {
        //  Instruction *instruction = WorkListUserOfLoad->pop_back_val();
        //  instruction->setOperand(0, Fvalue);
        //  instruction->dump();
        //}

        //// Removing all loads that are used by the global variable
        // while (!WorkListLoad->empty()) {
        //  Instruction *instruction = WorkListLoad->pop_back_val();
        //  instruction->eraseFromParent();
        //}
      }
    }
    // errs() << "Info>>>: ModulePass entry Function!";
    return true;
  }
};
}  // namespace

char ConstantReplacementPass::ID = 0;
static RegisterPass<ConstantReplacementPass> F0("constantREP",
                                                "Constant Replacement Pass ",
                                                false, true);

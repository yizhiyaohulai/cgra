#include "llvm/Analysis/ConstantFolding.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/MemoryBuiltins.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/ValueTracking.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/PredIteratorCache.h"
#include "llvm/IR/Value.h"
#include "llvm/Support/raw_ostream.h"
#include <fstream>
//#include <json/install/include/nlohmann/json.hpp>
#include "cgra/core/lib/tools/json.hpp"
#include <iomanip>
//#include "cgra/llvm/passes/test_pass.h"

using namespace llvm;

namespace {
//cl::opt<std::string> inputTagPairs(
//    "in-tag-pairs", cl::Positional,
//    cl::desc("<input file that contains tag number and string pairs>"));

struct PointerTracking : public FunctionPass {
  static char ID;
  Function *FF;
  ScalarEvolution *SE;
  LoopInfo *LI;
  DominatorTree *DT;
  const DataLayout *DL;
  TargetLibraryInfo *TLI;
  Function *callocFunc;
  Function *reallocFunc;
  Function *mallocFunc;
  //CgraTestPass* testPass;
  // typedef ICmpInst::Predicate Predicate;
  // PredIteratorCache predCache;
  // SmallPtrSet<const SCEV*, 1> analyzing;
  PointerTracking() : FunctionPass(ID) {}

  virtual void getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<LoopInfoWrapperPass>();
    AU.addRequired<DominatorTreeWrapperPass>();
    AU.addRequired<ScalarEvolutionWrapperPass>();
    AU.addRequired<TargetLibraryInfoWrapperPass>();
    // AU.addPreserved<AliasAnalysis>();
    // AU.setPreservesCFG();
    // AU.addRequired<PostDominatorTree>();
  }

  virtual bool runOnFunction(Function &F) {
    //    predCache.clear();
    //    assert(analyzing.empty());
//    std::string dir = inputTagPairs;
//    errs() << "inputtagpairs:" << inputTagPairs << "\n";
//    std::string append_str = "_graph_loop1.dot";
//    std::string file_dir =
//        dir.replace(dir.find_last_of(".bc"), append_str.size() + 1, append_str);

    std::vector<std::pair<std::string, size_t>> mem_info_;
    FF = &F;
    SE = &getAnalysis<ScalarEvolutionWrapperPass>().getSE();
    LI = &getAnalysis<LoopInfoWrapperPass>().getLoopInfo();
    DT = &getAnalysis<DominatorTreeWrapperPass>().getDomTree();
    TLI = &getAnalysis<TargetLibraryInfoWrapperPass>().getTLI();

    //auto test_info  = &getAnalysis<CgraTestPass>;

    DL = &FF->getParent()->getDataLayout();
    PointerTracking &PT = *const_cast<PointerTracking *>(this);
    for (inst_iterator I = inst_begin(*FF), E = inst_end(*FF); I != E; ++I) {
      if (!I->getType()->isPointerTy())
        continue;
      Value *Base;
      const SCEV *Limit, *Offset;
      getPointerOffset(&*I, Base, Limit, Offset);
      if (!Base)
        continue;
      if (Base == &*I) {
        const SCEV *S = getAllocationElementCount(Base);

        errs() << Base->getName().str() << "\n";
        //*Limit->FastID.Data();
        APInt val = cast<SCEVConstant>(Limit)->getValue()->getValue();
        auto re = val.getZExtValue();
        mem_info_.push_back(std::make_pair(Base->getName().str(),re));
        errs() << *Base << " ==> " << *S << " elements, ";
        errs() << *Limit << " bytes allocated\n";
        continue;
      }
      errs() << &*I << " -- base: " << *Base;
      errs() << " offset: " << *Offset;
    }

    std::string filename = "memory.json";
    std::ofstream f(filename, std::ios::out);
    using namespace  nlohmann;
    json json_result;
    json memory_node;
    std::for_each(mem_info_.begin(),mem_info_.end(),[&memory_node](std::pair<std::string, size_t> p){
        json element;
        element["object"] = p.first;
        element["size"] =  p.second;
        memory_node.push_back(element);
    });
    json_result["memory"] =  memory_node;
    f << std::setw(4) << json_result << std::endl;
    f.close();
    f.flush();
  }

  virtual bool doInitialization(Module &M) {
    errs() << "doInitialization"
           << "\n";
    const Type *PTy = Type::getInt8PtrTy(M.getContext());
    // Find calloc(i64, i64) or calloc(i32, i32).
    callocFunc = M.getFunction("calloc");
    if (callocFunc) {
      const FunctionType *Ty = callocFunc->getFunctionType();
      errs() << "calloc find"
             << "\n";
    }

    reallocFunc = M.getFunction("realloc");
    if (reallocFunc) {
      errs() << "realloc find"
             << "\n";
    }

    mallocFunc = M.getFunction("malloc");
    if (mallocFunc) {
      const FunctionType *Ty = mallocFunc->getFunctionType();

      std::vector<const Type *> args, args2;
      args.push_back(PTy);
      args.push_back(Type::getInt64Ty(M.getContext()));
      args2.push_back(PTy);
      args2.push_back(Type::getInt32Ty(M.getContext()));
      //        const FunctionType *Malloc1Type =  FunctionType::get(PTy,
      //        false); const FunctionType *Malloc2Type = FunctionType::get(PTy,
      //        args2, false); if (Ty != Malloc1Type && Ty != Malloc2Type)
      //          mallocFunc = 0; // Give up
      errs() << "mallocFunc find"
             << "\n";
      //        errs() << *Ty << "\n";
    }

    return false;
  }

  const SCEV *getAllocationElementCount(Value *V) const {
    // We only deal with pointers.
    const PointerType *PTy = cast<PointerType>(V->getType());
    return computeAllocationCountForType(V, PTy->getElementType());
  }

  // Calculates the number of elements allocated for pointer P,
  // the type of the element is stored in Ty.
  const SCEV *computeAllocationCount(Value *P, const Type *&Ty) const {
    Value *V = P->stripPointerCasts();
    if (AllocaInst *AI = dyn_cast<AllocaInst>(V)) {
      Value *arraySize = AI->getArraySize();
      Ty = AI->getAllocatedType();
      // arraySize elements of type Ty.
      return SE->getSCEV(arraySize);
    }

    if (CallInst *CI = extractMallocCall(V, TLI)) {
      Value *arraySize = getMallocArraySize(CI, *DL, TLI);
      const Type *AllocTy = getMallocAllocatedType(CI, TLI);
      if (!AllocTy || !arraySize)
        return SE->getCouldNotCompute();
      Ty = AllocTy;
      // arraySize elements of type Ty.
      return SE->getSCEV(arraySize);
    }

    if (GlobalVariable *GV = dyn_cast<GlobalVariable>(V)) {
      if (GV->hasDefinitiveInitializer()) {
        Constant *C = GV->getInitializer();
        if (const ArrayType *ATy = dyn_cast<ArrayType>(C->getType())) {
          Ty = ATy->getElementType();
          return SE->getConstant(Type::getInt32Ty(P->getContext()),
                                 ATy->getNumElements());
        }
      }
      Ty = GV->getType();
      return SE->getConstant(Type::getInt32Ty(P->getContext()), 1);
      // TODO: implement more tracking for globals
    }

    if (CallInst *CI = dyn_cast<CallInst>(V)) {
      CallSite CS(CI);
      Function *F =
          dyn_cast<Function>(CS.getCalledValue()->stripPointerCasts());
      const Loop *L = LI->getLoopFor(CI->getParent());
      if (F == callocFunc) {
        Ty = Type::getInt8Ty(P->getContext());
        // calloc allocates arg0*arg1 bytes.
        return SE->getSCEVAtScope(
            SE->getMulExpr(SE->getSCEV(CS.getArgument(0)),
                           SE->getSCEV(CS.getArgument(1))),
            L);
      } else if (F == reallocFunc) {
        Ty = Type::getInt8Ty(P->getContext());
        // realloc allocates arg1 bytes.
        return SE->getSCEVAtScope(CS.getArgument(1), L);
      }
    }

    return SE->getCouldNotCompute();
  }

  // Calculates the number of elements of type Ty allocated for P.
  const SCEV *computeAllocationCountForType(Value *P, const Type *Ty) const {
    const Type *elementTy;
    const SCEV *Count = computeAllocationCount(P, elementTy);
    if (isa<SCEVCouldNotCompute>(Count))
      return Count;
    if (elementTy == Ty)
      return Count;

    if (!DL) // need TargetData from this point forward
      return SE->getCouldNotCompute();

    uint64_t elementSize = DL->getTypeAllocSize(const_cast<Type *>(elementTy));
    uint64_t wantSize = DL->getTypeAllocSize(const_cast<Type *>(Ty));
    if (elementSize == wantSize)
      return Count;
    if (elementSize % wantSize) // fractional counts not possible
      return SE->getCouldNotCompute();
    return SE->getMulExpr(
        Count, SE->getConstant(Count->getType(), elementSize / wantSize));
  }

  const SCEV *getAllocationSizeInBytes(Value *V) const {
    return computeAllocationCountForType(V, Type::getInt8Ty(V->getContext()));
  }

  void getPointerOffset(Value *Pointer, Value *&Base, const SCEV *&Limit,
                        const SCEV *&Offset) const {
    Pointer = Pointer->stripPointerCasts();
    Base = GetUnderlyingObject(Pointer, *DL);
    Limit = getAllocationSizeInBytes(Base);
    if (isa<SCEVCouldNotCompute>(Limit)) {
      Base = 0;
      Offset = Limit;
      return;
    }

    Offset = SE->getMinusSCEV(SE->getSCEV(Pointer), SE->getSCEV(Base));
    if (isa<SCEVCouldNotCompute>(Offset)) {
      Base = 0;
      Limit = Offset;
    }
  }
};
} // namespace

char PointerTracking::ID = 0;
static RegisterPass<PointerTracking> X("pointertracking",
                                       "Track pointer bounds", false, false);

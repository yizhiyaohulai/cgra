#include "cgra/cc/run/gpu_device_factory.h"

namespace cgra{

GPUDeviceFactory::GPUDeviceFactory()
{

}

void GPUDeviceFactory::NewDevice(const std::string & device_type,std::vector<Device *>& devices)
{
    auto gpu_device = new GPUDevice();
    devices.emplace_back(gpu_device);
}

} //namesapce cgra



﻿#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <iostream>
#include <cgra/cc/run/cgra.h>
#include "cgra/core/platform/cpuinfo.h"
#include "cgra/core/platform/hostinfo.h"
#include "cgra/core/platform/logging.h"

using namespace cgra;

/* program entry */
int main(int argc, char *argv[])
{
    LOG(INFO) << "System is Running";
    using namespace  port;
    auto cpu_num = NumSchedulableCPUs();
    auto host_name = Hostname();
    CreateSession(argc,argv);
	exit(EXIT_SUCCESS);
}

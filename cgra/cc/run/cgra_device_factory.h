#ifndef CGRA_CC_RUN_CGRA_DEVICE_FACTORY_H
#define CGRA_CC_RUN_CGRA_DEVICE_FACTORY_H

#include "cgra/core/framework/device.h"
#include "cgra/core/framework/device_factory.h"
#include <vector>

namespace cgra {

class CGRADeviceFactory : public DeviceFactory {
public:
  CGRADeviceFactory();
  static void Register(const std::string &device_type,
                       DeviceFactory *device_factory);
  static void NewDevice(const std::string & device_type,std::vector<Device*>& devices);
};

}; // namespace cgra

#endif // CGRA_DEVICE_FACTORY_H

// static void Register(const std::string &device_type,
//                     DeviceFactory *device_factory);
// static Device *NewDevice(const std::string &device_type);
// static void CreateDevice(const std::string &device_type);

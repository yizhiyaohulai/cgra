#include "cgra/cc/run/cgra_device_factory.h"


namespace cgra{

CGRADeviceFactory::CGRADeviceFactory()
{

}

void  CGRADeviceFactory::NewDevice(const std::string & device_type,std::vector<Device *>& devices)
{
    auto cgra_device = new CGRADevice();
    devices.emplace_back(cgra_device);
}

} //namespace cgra





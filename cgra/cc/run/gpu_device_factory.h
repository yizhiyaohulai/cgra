#ifndef CGRA_CC_RUN_SRC_DEVICE_FACTORY_H
#define CGRA_CC_RUN_SRC_DEVICE_FACTORY_H

#include "cgra/core/framework/device.h"
#include "cgra/core/framework/device_factory.h"
#include <vector>

namespace cgra {
class GPUDeviceFactory : public DeviceFactory {
public:
  explicit GPUDeviceFactory();
  void NewDevice(const std::string &device_type, std::vector<Device*> &devices);
};
} // namespace cgra
#endif // CGRA_CC_RUN_SRC_DEVICE_FACTORY_H

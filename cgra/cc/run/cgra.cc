#include "cgra/cc/run/cgra.h"
#include "cgra/core/framework/session.h"
#include "cgra/core/framework/session_option.h"
#include "cgra/llvm/codegen/llvm_common.h"
#include <cgra/core/framework/stream.h>
#include <cgra/core/lib/core/runqueue.h>
#include <cgra/core/lib/core/threadpool.h>
#include <cgra/core/platform/windows/port.h>
#include <fstream>
#include <memory>
#include <process.hpp>

using namespace TinyProcessLib;

namespace cgra {

int FilePretreatmentTag(std::string path, std::string file) {
  RunQueue<std::string, 1024> modify_strs;
  int tag_num{0};

  std::vector<std::string> absolute_files;
  absolute_files.push_back(path + "/" + file + ".c");
  absolute_files.push_back(path + "/" + file + ".cpp");
  absolute_files.push_back(path + "/" + file + ".cc");

  std::string line;
  for (auto it : absolute_files) {
    std::ifstream input(it, std::ios_base::in);
    if (!input.is_open())
      std::cout << errors::NotFound("Could not find path ", it,
                                    "try other file")
                << std::endl;
    else {
      while (std::getline(input, line)) {
        size_t comment;
        if ((comment = line.find("//DFGLoop:")) != std::string::npos) {
          tag_num++;
          std::cout << "find str" << std::endl;
          std::string replace_str =
              "DFGLOOP_TAG(" + std::to_string(tag_num) + ");";
          auto c_str =
              line.replace(comment, replace_str.size() + 1, replace_str);
          std::cout << c_str << std::endl;
          modify_strs.PushBack(c_str);
        } else
          modify_strs.PushBack(line);
      }
      break;
    }
  }

  if (tag_num >= 1) {
    modify_strs.PushFront(
        "__attribute__((noinline)) void DFGLOOP_TAG(int index);\n");
  }

  std::cout << "tagnum : " << tag_num << std::endl;

  FILE *tagc_fp;
  std::string tagc_file = path + "/" + file + ".tagged.c";
  tagc_fp = fopen(tagc_file.c_str(), "w+");
  auto tagc_size = modify_strs.Size();
  for (size_t i = 0; i < tagc_size; i++) {
    // std::cout << modify_strs.PopFront().c_str() << std::endl;
    fprintf(tagc_fp, "%s\n", modify_strs.PopFront().c_str());
  }
  fclose(tagc_fp);

  FILE *tagnum_fp;
  std::string tagnum_file = path + "/" + file + ".tag";
  tagnum_fp = fopen(tagnum_file.c_str(), "w+");
  for (int i = 0; i < tag_num; i++)
    fprintf(tagnum_fp, "%d loop%d\n", i + 1, i + 1);
  fclose(tagnum_fp);
  return tag_num;
}

void test() {}

void CreateSession(int argc, char *argv[]) {

  StreamContext<int> a;
  a.register_t = std::move(5);
  StreamContext<std::function<void()>> b;
  b.register_t = std::move(std::bind(test));

  SessionOptions options;
  if (argc < 2) {
    std::cout << "Usage: <dot_file> <code_file> <xml_file>" << std::endl;
    exit(0);
  }

  std::string file_dir = argv[1];
  auto index = file_dir.find_last_of("/");
  auto file_name = file_dir.substr(index + 1);
  // options.config.set_dfg_file(file_dir + "/" + file_name + ".dot");
  // options.config.set_ir_dfg_file(file_dir + "/" + file_name + ".graph.dot");

  //'../../../build/script/LoopParser.py' 'mac.c' 'mac.tagged.c' 'mac.tag'
  // clang -emit-llvm -c 'mac.tagged.c' -o 'mac.bc' -O3 -fno-vectorize
  // -fno-slp-vectorize -fno-unroll-loops opt 'mac.bc' -o '/dev/null' -load
  // '../../../build/lib/libDFG.so' --dfg-out -in-tag-pairs 'mac.tag' -loop-tags
  // 'loop'

  auto tag_num = FilePretreatmentTag(file_dir, file_name);

  std::string clang_cmd = "clang -emit-llvm -c";
  clang_cmd += " " + file_dir + "/" + file_name + ".tagged.c" + " -o";
  clang_cmd += " " + file_dir + "/" + file_name + ".bc";
  clang_cmd += " -O3 -fno-vectorize -fno-slp-vectorize -fno-unroll-loops";

  std::string content;
  auto memory_buffer_ref = read_source_file("/home/hujb/source/cgra-thu/benchmarks/microbench/sum/sum.c");
  generate_ir_file(content,memory_buffer_ref);

  std::string opt_cmd = "opt ";
  opt_cmd += file_dir + "/" + file_name + ".bc" + " ";
  opt_cmd += " -o /dev/null -load  ";
  opt_cmd += "./libcgra_llvm_passes.so -pointertracking";
  opt_cmd += " --dfg-out -in-tag-pairs " + file_dir + "/" + file_name + ".tag" +
             " -loop-tags ";

  std::string append_str;
  append_str += "'";
  for (signed i = 0; i < tag_num; i++) {
    append_str += "loop" + std::to_string(i + 1) + " ";
  }
  append_str += "'";
  opt_cmd += append_str;
  std::cout << "clang_cmd:" << clang_cmd << std::endl;
  std::cout << "opt_cmd:" << opt_cmd << std::endl;

  Process process1(clang_cmd);
  process1.get_exit_status();

  Process process2(opt_cmd);
  auto exit_status = process2.get_exit_status();

  // generate ir dfg file and desire dfg file name
  // this operation name use the pargse dfg file
  std::cout << "tag_num" << tag_num << std::endl;
  for (signed i = 0; i < tag_num; i++) {
    std::string ir_dfg_file = file_dir + "/" + file_name + "_graph_loop" +
                              std::to_string(i + 1) + ".dot";
    std::string dfg_file =
        file_dir + "/" + file_name + std::to_string(i + 1) + ".dot";
    options.config.add_ir_dfg_file(ir_dfg_file);
    options.config.add_dfg_file(dfg_file);
  }

  options.config.set_code_file(file_name);
  options.config.set_code_dir(file_dir);
  options.config.set_code_dir(file_dir);
  options.config.set_accelerate_num(tag_num);

  std::unique_ptr<Session> session(NewSession(options));
  session->Run();
}

} // namespace cgra

# Copyright 2019 The CGRA Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
########################################################
# cgra_core_graph library
########################################################
file(GLOB_RECURSE cgra_core_ilp_srcs
    "${cgra_source_dir}/cgra/core/ilp/*.cc"
    "${cgra_source_dir}/cgra/core/ilp/*.h"
)

add_library(cgra_core_ilp OBJECT ${cgra_core_ilp_srcs})
add_dependencies(cgra_core_ilp cgra_core_framework)


file(GLOB_RECURSE cgra_core_distributed_runtime_srcs
   "${cgra_source_dir}/cgra/core/distributed_runtime/*.h"
   "${cgra_source_dir}/cgra/core/distributed_runtime/*.cc"
)

#file(GLOB_RECURSE cgra_core_distributed_runtime_exclude_srcs
#
#)

#list(REMOVE_ITEM cgra_core_distributed_runtime_srcs ${cgra_core_distributed_runtime_exclude_srcs})

#add_library(cgra_core_distributed_runtime OBJECT ${cgra_core_distributed_runtime_srcs})

#add_dependencies(cgra_core_distributed_runtime
#    grpc
#)

########################################################
# grpc_tensorflow_server executable
########################################################
#set(grpc_cgra_server_srcs
#    "${tensorflow_source_dir}/tensorflow/core/distributed_runtime/rpc/grpc_tensorflow_server.cc"
#)

#add_executable(grpc_cgra_server
#   ${grpc_cgra_server_srcs}
#)

#target_link_libraries(grpc_cgra_server PUBLIC
#
#)

# Copyright 2017 The CGRA Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
########################################################
# cgra_core_direct_session library
########################################################
file(GLOB cgra_core_direct_session_srcs
   "${cgra_source_dir}/cgra/core/common_runtime/direct_session.cc"
   "${cgra_source_dir}/cgra/core/common_runtime/direct_session.h"
)

add_library(cgra_core_direct_session OBJECT ${cgra_core_direct_session_srcs})

add_dependencies(cgra_core_direct_session cgra_core_framework cgra_core_ilp tinyprocess)

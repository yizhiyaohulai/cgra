# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
set(cgra_run_srcs
    "${cgra_source_dir}/cgra/cc/run/main.cc"
    "${cgra_source_dir}/cgra/cc/run/cgra.cc"
    "${cgra_source_dir}/cgra/cc/run/cgra.h"
    "${cgra_source_dir}/cgra/cc/run/gpu_device_factory.cc"
    "${cgra_source_dir}/cgra/cc/run/gpu_device_factory.h"
    "${cgra_source_dir}/cgra/cc/run/cgra_device_factory.cc"
    "${cgra_source_dir}/cgra/cc/run/cgra_device_factory.h"
    "${cgra_source_dir}/cgra/llvm/codegen/llvm_common.cc"
    "${cgra_source_dir}/cgra/llvm/codegen/llvm_common.h"
)

add_executable(cgra_run_srcs
    ${cgra_run_srcs}
    $<TARGET_OBJECTS:cgra_core_lib>
    $<TARGET_OBJECTS:cgra_core_framework>
    $<TARGET_OBJECTS:cgra_core_graph>
    $<TARGET_OBJECTS:cgra_core_direct_session>
    $<TARGET_OBJECTS:cgra_core_ilp>
)

if (WIN32)
  
else (WIN32)
    set(cgra_EXTERNAL_LIBRARIES ${cgra_EXTERNAL_LIBRARIES} pthread)
endif (WIN32)


message("=========LLVM_INCLUDE_DIRS======")
message(${LLVM_INCLUDE_DIRS})
target_include_directories(cgra_run_srcs PRIVATE ${tinyprocess_INCLUDE_DIR} ${LLVM_INCLUDE_DIRS})

set(static_clang_libs ${LLVM_LIBRARY_DIRS}/libclangCodeGen.a
                      ${LLVM_LIBRARY_DIRS}/libclangFrontend.a
                      ${LLVM_LIBRARY_DIRS}/libclangBasic.a
                      ${LLVM_LIBRARY_DIRS}/libclangDriver.a
                      ${LLVM_LIBRARY_DIRS}/libclangSerialization.a
                      ${LLVM_LIBRARY_DIRS}/libclangParse.a
                      ${LLVM_LIBRARY_DIRS}/libclangSema.a
                      ${LLVM_LIBRARY_DIRS}/libclangAnalysis.a
                      ${LLVM_LIBRARY_DIRS}/libclangAST.a
                      ${LLVM_LIBRARY_DIRS}/libclangTooling.a
                      ${LLVM_LIBRARY_DIRS}/libclangToolingRefactor.a
                      ${LLVM_LIBRARY_DIRS}/libclangRewriteFrontend.a
                      ${LLVM_LIBRARY_DIRS}/libclangLex.a
                      ${LLVM_LIBRARY_DIRS}/libclangEdit.a
                      ${LLVM_LIBRARY_DIRS}/libclangFrontendTool.a
                      ${LLVM_LIBRARY_DIRS}/libclangARCMigrate.a
                      ${LLVM_LIBRARY_DIRS}/libclangToolingCore.a
                      ${LLVM_LIBRARY_DIRS}/libclangStaticAnalyzerFrontend.a
                      ${LLVM_LIBRARY_DIRS}/libclangRewrite.a
                      ${LLVM_LIBRARY_DIRS}/libclangStaticAnalyzerCore.a
                      ${LLVM_LIBRARY_DIRS}/libclangASTMatchers.a
                      ${LLVM_LIBRARY_DIRS}/libclangStaticAnalyzerCheckers.a
                      ${LLVM_LIBRARY_DIRS}/libclangCrossTU.a
                      ${LLVM_LIBRARY_DIRS}/libclangIndex.a
                      ${LLVM_LIBRARY_DIRS}/libLLVMPasses.a
                      ${LLVM_LIBRARY_DIRS}/libLLVMLTO.a
                      ${LLVM_LIBRARY_DIRS}/libLLVMCoroutines.a
                      ${LLVM_LIBRARY_DIRS}/libLLVMCoverage.a
                      )
target_link_libraries(cgra_run_srcs PUBLIC
    ${cgra_EXTERNAL_LIBRARIES}
    ${tinyprocess_STATIC_LIBRARIES}
    ${llvm_libs}
    ${static_clang_libs}
#    libcgra_llvm_optimize.so
    cgra_llvm_optimize
)

install(TARGETS cgra_run_srcs
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)

#message("=========llvm_libs======")
#message(${llvm_libs})
#message(libcgra_llvm_optimize.so)



#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
########################################################
# cgra_llvm_passes library
########################################################
file(GLOB_RECURSE cgra_llvm_passes_srcs
    "${cgra_source_dir}/cgra/llvm/passes/*.cc"
    "${cgra_source_dir}/cgra/llvm/passes/*.h"
)

add_library(cgra_llvm_passes SHARED ${cgra_llvm_passes_srcs})

find_package(LLVM REQUIRED CONFIG)

#ADD_DEPENDENCIES(cgra_llvm_passes cgra_core_graph)

set(LLVM_LINK_COMPONENTS
    all
    )

execute_process(COMMAND llvm-config --cxxflags
 OUTPUT_VARIABLE LLVM_CXXFLAGS)
 string(STRIP ${LLVM_CXXFLAGS} LLVM_CXXFLAGS)

llvm_map_components_to_libnames(llvm_libs ${LLVM_LINK_COMPONENTS})
target_compile_options(cgra_llvm_passes PRIVATE -fno-rtti -fvisibility-inlines-hidden -Werror=date-time -std=c++11 -Wwrite-strings -Wcast-qual -Wno-missing-field-initializers -fPIC  -Wall -Wextra -Wno-unused-parameter  -pedantic -Wno-long-long -Wno-maybe-uninitialized -Wdelete-non-virtual-dtor -Wno-comment -g  -fno-exceptions)

#target_link_libraries(cgra_llvm_passes ${llvm_libs})
target_include_directories(cgra_llvm_passes PRIVATE ${LLVM_INCLUDE_DIRS})
target_compile_definitions(cgra_llvm_passes PRIVATE ${LLVM_DEFINITIONS})

#message("=========LLVM_INCLUDE_DIRS_PASSES======")
#message(${LLVM_INCLUDE_DIRS})
#message(${LLVM_LIBRARY_DIRS})


























#set(GernerateFile ${CMAKE_CURRENT_BINARY_DIR}/libCgraLLVMPasses.so)

#execute_process(COMMAND llvm-config --ldflags
# OUTPUT_VARIABLE LLVM_LDFLAGS)
#string(STRIP ${LLVM_LDFLAGS} LLVM_LDFLAGS)


#string(CONCAT x ${LLVM_CXXFLAGS} " " ${LLVM_LDFLAGS} " " ${LLVM_LIBS} " -shared -Wl,-flat_namespace -Wl,-undefined,suppress")
##message(${x})

#set(CMAKE_SHARED_LINKER_FLAGS ${x})
#set(CMAKE_SHARED_LINKER_FLAGS_DEBUG ${x})

#ADD_CUSTOM_TARGET(GenerateLLVMPasses
#                  COMMAND sh ${cgra_source_dir}/cgra/llvm/passes/make.sh ${GernerateFile} ${cgra_llvm_passes_srcs}
#                  DEPENDS llvm)



function(RELATIVE_PROTOBUF_GENERATE_CPP SRCS HDRS ROOT_DIR)
  if(NOT ARGN)
    message(SEND_ERROR "Error: RELATIVE_PROTOBUF_GENERATE_CPP() called without any proto files")
    return()
  endif()

  set(${SRCS})
  set(${HDRS})
  foreach(FIL ${ARGN})
    set(ABS_FIL ${ROOT_DIR}/${FIL})
    get_filename_component(FIL_WE ${FIL} NAME_WE)
    get_filename_component(FIL_DIR ${ABS_FIL} PATH)
    file(RELATIVE_PATH REL_DIR ${ROOT_DIR} ${FIL_DIR})

    list(APPEND ${SRCS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.cc")
    list(APPEND ${HDRS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.h")

    add_custom_command(
      OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.cc"
             "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.h"
      COMMAND  ${PROTOBUF_PROTOC_EXECUTABLE}
      ARGS --cpp_out  ${CMAKE_CURRENT_BINARY_DIR} -I ${ROOT_DIR} ${ABS_FIL} -I ${PROTOBUF_INCLUDE_DIRS}
      DEPENDS ${ABS_FIL} protobuf
      COMMENT "Running C++ protocol buffer compiler on ${FIL}"
      VERBATIM )
  endforeach()

  set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
  set(${SRCS} ${${SRCS}} PARENT_SCOPE)
  set(${HDRS} ${${HDRS}} PARENT_SCOPE)
endfunction()

if(NOT WIN32)
  function(RELATIVE_PROTOBUF_GENERATE_GRPC_CPP SRCS HDRS ROOT_DIR)
    if(NOT ARGN)
      message(SEND_ERROR "Error: RELATIVE_PROTOBUF_GENERATE_GRPC_CPP() called without any proto files")
      return()
    endif()

    set(${SRCS})
    set(${HDRS})
    foreach(FIL ${ARGN})
      set(ABS_FIL ${ROOT_DIR}/${FIL})
      get_filename_component(FIL_WE ${FIL} NAME_WE)
      get_filename_component(FIL_DIR ${ABS_FIL} PATH)
      file(RELATIVE_PATH REL_DIR ${ROOT_DIR} ${FIL_DIR})

      list(APPEND ${SRCS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.grpc.pb.cc")
      list(APPEND ${HDRS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.grpc.pb.h")
      list(APPEND ${SRCS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.cc")
      list(APPEND ${HDRS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.h")

      add_custom_command(
        OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.grpc.pb.cc"
               "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.grpc.pb.h"
               "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.cc"
               "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb.h"
        COMMAND ${PROTOBUF_PROTOC_EXECUTABLE}
        ARGS --grpc_out ${CMAKE_CURRENT_BINARY_DIR} --cpp_out ${CMAKE_CURRENT_BINARY_DIR} --plugin protoc-gen-grpc=${GRPC_BUILD}/grpc_cpp_plugin -I ${ROOT_DIR} ${ABS_FIL} -I ${PROTOBUF_INCLUDE_DIRS}
        DEPENDS ${ABS_FIL} protobuf grpc
        COMMENT "Running C++ protocol buffer grpc compiler on ${FIL}"
        VERBATIM )
    endforeach()

    set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
    set(${SRCS} ${${SRCS}} PARENT_SCOPE)
    set(${HDRS} ${${HDRS}} PARENT_SCOPE)
  endfunction()
endif()

function(RELATIVE_PROTOBUF_TEXT_GENERATE_CPP SRCS HDRS ROOT_DIR)
  if(NOT ARGN)
      message(SEND_ERROR "Error: RELATIVE_PROTOBUF_TEXT_GENERATE_CPP() called without any proto files")
    return()
  endif()

  set(${SRCS})
  set(${HDRS})
  foreach(FIL ${ARGN})
    set(ABS_FIL ${ROOT_DIR}/${FIL})
    get_filename_component(FIL_WE ${FIL} NAME_WE)
    get_filename_component(FIL_DIR ${ABS_FIL} PATH)
    file(RELATIVE_PATH REL_DIR ${ROOT_DIR} ${FIL_DIR})

    list(APPEND ${SRCS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb_text.cc")
    list(APPEND ${HDRS} "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb_text.h")

    add_custom_command(
      OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb_text.cc"
             "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}/${FIL_WE}.pb_text.h"
      COMMAND ${PROTO_TEXT_EXE}
      ARGS "${CMAKE_CURRENT_BINARY_DIR}/${REL_DIR}" ${REL_DIR} ${ABS_FIL} "${ROOT_DIR}/tensorflow/tools/proto_text/placeholder.txt"
      DEPENDS ${ABS_FIL} ${PROTO_TEXT_EXE}
      COMMENT "Running C++ protocol buffer text compiler (${PROTO_TEXT_EXE}) on ${FIL}"
      VERBATIM )
  endforeach()

  set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
  set(${SRCS} ${${SRCS}} PARENT_SCOPE)
  set(${HDRS} ${${HDRS}} PARENT_SCOPE)
endfunction()

file(GLOB_RECURSE cgra_core_lib_srcs
    "${cgra_source_dir}/cgra/core/lib/*.cc"
    "${cgra_source_dir}/cgra/core/lib/*.h"
    "${cgra_source_dir}/cgra/core/lib/core/*.cc"
    "${cgra_source_dir}/cgra/core/lib/core/*.h"
    "${cgra_source_dir}/cgra/core/lib/gtl/*.cc"
    "${cgra_source_dir}/cgra/core/lib/gtl/*.h"
    "${cgra_source_dir}/cgra/core/lib/strings/*.cc"
    "${cgra_source_dir}/cgra/core/lib/strings/*.h"
)

file(GLOB cgra_core_platform_srcs
    "${cgra_source_dir}/cgra/core/platform/*.h"
    "${cgra_source_dir}/cgra/core/platform/*.cc"
    "${cgra_source_dir}/cgra/core/platform/default/*.h"
    "${cgra_source_dir}/cgra/core/platform/default/*.cc"
    "${cgra_source_dir}/cgra/core/platform/posix/*.cc"
    )

list(APPEND cgra_core_lib_srcs ${cgra_core_platform_srcs})

if(UNIX)
  file(GLOB cgra_core_platform_posix_srcs

  )
  list(APPEND cgra_core_lib_srcs ${cgra_core_platform_posix_srcs})
endif(UNIX)


set(PROTO_TEXT_EXE "proto_text")
set(cgra_proto_text_srcs
    "cgra/core/framework/node_def.proto"
    "cgra/core/framework/op_def.proto"
    "cgra/core/framework/types.proto"
    "cgra/core/framework/stream.proto"
    "cgra/core/protobuf/config.proto"
)
RELATIVE_PROTOBUF_GENERATE_CPP(PROTO_TEXT_SRCS PROTO_TEXT_HDRS
    ${cgra_source_dir} ${cgra_proto_text_srcs}
)

file(GLOB_RECURSE cgra_core_framework_srcs
    "${cgra_source_dir}/cgra/core/framework/*.h"
    "${cgra_source_dir}/cgra/core/framework/*.cc"
    "${cgra_source_dir}/cgra/core/protobuf/*.cc"
    "${cgra_source_dir}/cgra/core/protobuf/*.h"
)

if(WIN32)
  file(GLOB cgra_core_platform_windows_srcs
      "${cgra_source_dir}/cgra/core/platform/windows/*.h"
      "${cgra_source_dir}/cgra/core/platform/windows/*.cc"
  )
  list(APPEND cgra_core_lib_srcs ${cgra_core_platform_windows_srcs})


  file(GLOB cgra_core_platform_remove_srcs
      "${cgra_source_dir}/cgra/core/platform/default/env_time.cc"
  )
  list(REMOVE_ITEM cgra_core_lib_srcs ${cgra_core_platform_remove_srcs})
endif(WIN32)

add_library(cgra_core_lib OBJECT ${cgra_core_lib_srcs})
add_dependencies(cgra_core_lib nsync)

add_library(cgra_core_framework OBJECT
    ${cgra_core_framework_srcs}
    ${PROTO_TEXT_SRCS})
add_dependencies(cgra_core_framework
    cgra_core_lib
)


if(WIN32)
  # Cmake > 3.6 will quote this as -D"__VERSION__=\"MSVC\"" which nvcc fails on.
  # Instead of defining this global, limit it to cgra_core_framework where its used.
  target_compile_definitions(cgra_core_framework PRIVATE __VERSION__="MSVC")
endif()


# Copyright 2017 The Cgra Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
include (ExternalProject)

set(gurobi_INCLUDE_DIR $ENV{GUROBI_HOME}/include)
set(gurobi_URL https://github.com/glfw/glfw.git)
set(gurobi_TAG 8055dad7e4d93069a72fdb8abecc131b3c3e5040)

if(WIN32)
  set(gurobi_STATIC_LIBRARIES $ENV{GUROBI_HOME}/lib/libgurobi81.dll)
else()
  set(gurobi_STATIC_LIBRARIES $ENV{GUROBI_HOME}/lib/libgurobi81.so)
  #set(gurobi_LIBRARIES $ENV{GUROBI_HOME}/lib/libgurobi_c++.a)
endif()



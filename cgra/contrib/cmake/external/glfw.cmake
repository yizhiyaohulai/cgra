# Copyright 2017 The Cgra Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
include (ExternalProject)

set(glfw_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/glfw/install/include)
set(glfw_URL https://github.com/glfw/glfw.git)
set(glfw_TAG 8055dad7e4d93069a72fdb8abecc131b3c3e5040)
set(glfw_BUILD ${CMAKE_CURRENT_BINARY_DIR}/glfw/src/glfw/build)
set(glfw_INSTALL ${CMAKE_CURRENT_BINARY_DIR}/glfw/install)

if(WIN32)
  if(${CMAKE_GENERATOR} MATCHES "Visual Studio.*")
    set(glfw_STATIC_LIBRARIES
        ${glfw_INSTALL}/lib/glfw3.lib)
  endif()

  # This section is to make sure CONFIGURE_COMMAND use the same generator settings
  set(GLFW_GENERATOR_PLATFORM)
  if (CMAKE_GENERATOR_PLATFORM)
      set(GLFW_GENERATOR_PLATFORM -A ${CMAKE_GENERATOR_PLATFORM})
  endif()
  set(GLFW_GENERATOR_TOOLSET)
    if (CMAKE_GENERATOR_TOOLSET)
    set(GLFW_GENERATOR_TOOLSET -T ${CMAKE_GENERATOR_TOOLSET})
  endif()
  set(GLFW_ADDITIONAL_CMAKE_OPTIONS -G ${CMAKE_GENERATOR} ${GLFW_GENERATOR_PLATFORM} ${GLFW_GENERATOR_TOOLSET})
  # End of section
else()
  set(glfw_STATIC_LIBRARIES ${glfw_INSTALL}/lib/libglfw.so)
endif()

ExternalProject_Add(glfw
    PREFIX glfw
    GIT_REPOSITORY ${glfw_URL}
    GIT_TAG ${glfw_TAG}
    INSTALL_DIR ${glfw_INSTALL}
    DOWNLOAD_DIR "${DOWNLOAD_LOCATION}"
    CONFIGURE_COMMAND ${CMAKE_COMMAND}  ../glfw 
    -DCMAKE_CONFIGURATION_TYPES:STRING=${CMAKE_BUILD_TYPE}
    -DGLFW_INSTALL:BOOL=ON
    -DCMAKE_INSTALL_PREFIX:STRING=${glfw_INSTALL}
    -DBUILD_SHARED_LIBS=ON
    ${GLFW_ADDITIONAL_CMAKE_OPTIONS}
)

# depends on the existence of glad/glad.h
ExternalProject_Add_Step(glfw copy_rand
    COMMAND ${CMAKE_COMMAND} -E copy_directory 
    ${CMAKE_SOURCE_DIR}/patches/glfw/glad ${glfw_INCLUDE_DIR}/glad
    COMMAND ${CMAKE_COMMAND} -E copy_directory 
    ${CMAKE_SOURCE_DIR}/patches/glfw/KHR ${glfw_INCLUDE_DIR}/KHR
    COMMAND ${CMAKE_COMMAND} -E copy
    ${CMAKE_SOURCE_DIR}/patches/glfw/linmath.h ${glfw_INCLUDE_DIR}
    #DEPENDEES patch
    DEPENDERS build
)


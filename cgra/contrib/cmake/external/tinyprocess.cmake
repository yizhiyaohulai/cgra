# Copyright 2017 The Cgra Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
include (ExternalProject)

set(tinyprocess_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/tinyprocess/install/include)
set(tinyprocess_URL https://github.com/eidheim/tiny-process-library.git)
set(tinyprocess_TAG 5bc47531a97f80ac07092ac3ed56a26139c748b3)
set(tinyprocess_BUILD ${CMAKE_CURRENT_BINARY_DIR}/tinyprocess/src/tinyprocess-build)
set(tinyprocess_INSTALL ${CMAKE_CURRENT_BINARY_DIR}/tinyprocess/install)

if(WIN32)

else()
    set(tinyprocess_STATIC_LIBRARIES ${tinyprocess_INSTALL}/lib/libtiny-process-library.so)
endif()

ExternalProject_Add(tinyprocess
    PREFIX tinyprocess
    GIT_REPOSITORY ${tinyprocess_URL}
    GIT_TAG ${tinyprocess_TAG}
    INSTALL_DIR ${tinyprocess_INSTALL}
    DOWNLOAD_DIR "${DOWNLOAD_LOCATION}"
    CONFIGURE_COMMAND ${CMAKE_COMMAND}  ../tinyprocess
    -DCMAKE_CONFIGURATION_TYPES:STRING=${CMAKE_BUILD_TYPE}
    -DCMAKE_INSTALL_PREFIX:STRING=${tinyprocess_INSTALL}
    -DBUILD_SHARED_LIBS=ON
)

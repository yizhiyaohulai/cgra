# Copyright 2017 The Cgra Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
include (ExternalProject)
set(llvm_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/llvm/install/include)
set(llvm_LIBS_DIR ${CMAKE_CURRENT_BINARY_DIR}/llvm/install/lib)
set(llvm_COMMAND_DIR ${CMAKE_CURRENT_BINARY_DIR}/llvm/install/bin)
set(llvm_URL https://gitlab.com/yizhiyaohulai/llvm.git)
set(llvm_TAG 73a12c1f835f49968dd03ef6fccfbb7dd56e363b)
set(llvm_BUILD ${CMAKE_CURRENT_BINARY_DIR}/llvm/build/src/llvm-build)
set(llvm_INSTALL ${CMAKE_CURRENT_BINARY_DIR}/llvm/install)

if(WIN32)

else()

endif()

ExternalProject_Add(llvm
    PREFIX llvm
    GIT_REPOSITORY ${llvm_URL}
    GIT_TAG ${llvm_TAG}
    INSTALL_DIR ${llvm_INSTALL}
    DOWNLOAD_DIR "${DOWNLOAD_LOCATION}"
    CONFIGURE_COMMAND ${CMAKE_COMMAND}  ../llvm
    -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
    -DBUILD_SHARED_LIBS=ON
    -DCMAKE_INSTALL_PREFIX:STRING=${llvm_INSTALL}
    -DBUILD_SHARED_LIBS=ON
)




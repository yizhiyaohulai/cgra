"""
Copyright  :  -- This confidential and proprietary software may be used
              -- only as authorized by a licensing agreement from
              -- Research Center for Mobile Computing (RCMC), Tsinghua University.
              -- (C) COPYRIGHT 2019 RCMC, Tsinghua University
              -- ALL RIGHTS RESERVED
              -- The entire notice above must be reproduced on all authorized copies.
File Name  :  txt2json.py
Module Name:  assembly2machine
Author     :  Lie Luo
Version    :  1.0

Description:  input.txt -> config.json
"""

# -*- coding: utf-8 -*-

import json
from json_class import TEX_config

# get the tex string
file = open('./input.txt', 'r', encoding='UTF-8')
text = file.read()
file.close()

# string -> list
text_line = text.split('\n')
# list -> dict
for line in text_line:
    # print(line)
    if (len(line) != 0):
        if (line[0] != '#'):
            # print('line into conf')
            conf = TEX_config(line.strip())
    

# Write into json
    
# print(json.dumps(conf.output_js(), indent=4))
with open("./conf/config.json","w") as file:
    file.write(json.dumps(conf.output_js(), indent=4))
"""
Copyright  :  -- This confidential and proprietary software may be used
              -- only as authorized by a licensing agreement from
              -- Research Center for Mobile Computing (RCMC), Tsinghua University.
              -- (C) COPYRIGHT 2019 RCMC, Tsinghua University
              -- ALL RIGHTS RESERVED
              -- The entire notice above must be reproduced on all authorized copies.
File Name  :  json2config.py
Module Name:  assembly2machine
Author     :  Lie Luo
Version    :  1.0

Description:  config.json -> config_code.txt
"""
# -*- coding: utf-8 -*-


from PE_class import PEA
import json

# get the json string
file = open('./conf/config.json', 'r')
text = file.read()
file.close()

# string -> dict
js = json.loads(text)

# get PEA info
PEA_TOP = js['PEA_TOP']

file = open('./conf/config_code.bin', 'w')
for one_PEA in PEA_TOP:
    PEA_info = PEA(one_PEA)
    # print
    # PEA_info.PEA_config_2_machineCode_print()
    
    # write into file
    PEA_info.PEA_config_2_machineCode(file)

file.close()
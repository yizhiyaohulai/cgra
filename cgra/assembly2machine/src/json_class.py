"""
Copyright  :  -- This confidential and proprietary software may be used
              -- only as authorized by a licensing agreement from
              -- Research Center for Mobile Computing (RCMC), Tsinghua University.
              -- (C) COPYRIGHT 2019 RCMC, Tsinghua University
              -- ALL RIGHTS RESERVED
              -- The entire notice above must be reproduced on all authorized copies.
File Name  :  json_class.py
Module Name:  assembly2machine
Author     :  Lie Luo
Version    :  1.0

Description:  the class used in 'txt2json.py' & 'txt2json.py'
"""

import re

########################################
#             Keyword Class            #
########################################
class Keyword:
    def __init__(self, input_str):
        info = input_str.split('_')
        self.style = info[0]
        self.parameter = info[1:]

########################################
#             TEX_TOP Class            #
########################################
class TEX_TOP:
    def __init__(self, input_tuple):
        info = re.split(r'\s*,\s*', input_tuple[1:-1])
        
        for name, value in zip(TEX_TOP.name_list, info):
            setattr(self, name, int(value))
    
    name_list = ['Index_PE',
                 'Count',
                 'Iteration_line',
                 'Initial_idle',
                 'Iteration_PE',
                 'Iteration_PEA',
                 'Task_packageNum',
                 'Package_index',
                 'Bit_width',
                 'R1',
                 'R2']

########################################
#            TEX_ALU Class             #
########################################
class TEX_ALU:
    def __init__(self, input_tuple, op_name):
        info = re.split(r'\s*,\s*', input_tuple[1:-1])
        config = {

            'OP_code': op_name[1:],

            'PE_type': 'ALU',

            'Imm': {
                'type': False
            }
        }
        #---------- set default value ----------#
        for name, value_str in zip(TEX_ALU.name_list, info):
            # default value is ''
            if value_str != '':
                value = Keyword(value_str)

                if name in ['In_1', 'In_2', 'In_3']:

                    if name in 'In_2' and value.style == 'IMM':
                        inner_dict = {
                            'type': 'Imm',
                        }
                        config['Imm'] = {
                            'type': True,
                            'num': int(value.parameter[0])
                        }
                    
                    else:
                        if value.style in ['GR', 'LR']:
                            inner_dict = {
                                'type': value.style,
                                'num': int(value.parameter[0])
                            }
                        elif value.style == 'Self':
                            flag = 'My_out' + value.parameter[0] + '_' + value.parameter[1]
                            inner_dict = { 'type': flag }
                        
                        elif value.style == 'Route':
                            loc = {
                                'LUC': ['r1', 'r2', 'r3', 'r7', 'd1', 'd2', 'd3', 'd7'],
                                'RUC': ['l1', 'l2', 'l3', 'l7', 'd1', 'd2', 'd3', 'd7'],
                                'LDC': ['r1', 'r2', 'r3', 'r7', 'u1', 'u2', 'u3', 'u7'],
                                'RDC': ['l1', 'l2', 'l3', 'l7', 'u1', 'u2', 'u3', 'u7'],
                                
                                'U': ['l', 'r', 'le', 're', 'd1', 'd2', 'd3', 'd7'],
                                'D': ['l', 'r', 'le', 're', 'u1', 'u2', 'u3', 'u7'],
                                'L': ['u', 'd', 'ue', 'de', 'r1', 'r2', 'r3', 'r7'],
                                'R': ['u', 'd', 'ue', 'de', 'l1', 'l2', 'l3', 'l7'],
                                
                                'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                                'RU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                                'LD': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                                'RD': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            }
                            flag = 'Other_out' + value.parameter[0] + '_' + value.parameter[1]
                            inner_dict = {
                                'type': flag,
                                'num': loc[value.parameter[2]].index(value.parameter[3])
                            }

                    config[name] = inner_dict
                    
                elif name == 'In_4':
                    if value.style == 'Self':
                        flag = 'My_' + value.parameter[0]
                        inner_dict = { 'type': flag }
                    
                    elif value.style == 'Route':
                        loc = {
                            'LUC': ['r1', 'r2', 'r3', 'r7', 'd1', 'd2', 'd3', 'd7'],
                            'RUC': ['l1', 'l2', 'l3', 'l7', 'd1', 'd2', 'd3', 'd7'],
                            'LDC': ['r1', 'r2', 'r3', 'r7', 'u1', 'u2', 'u3', 'u7'],
                            'RDC': ['l1', 'l2', 'l3', 'l7', 'u1', 'u2', 'u3', 'u7'],
                            
                            'U': ['l', 'r', 'le', 're', 'd1', 'd2', 'd3', 'd7'],
                            'D': ['l', 'r', 'le', 're', 'u1', 'u2', 'u3', 'u7'],
                            'L': ['u', 'd', 'ue', 'de', 'r1', 'r2', 'r3', 'r7'],
                            'R': ['u', 'd', 'ue', 'de', 'l1', 'l2', 'l3', 'l7'],
                            
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                        }
                        flag = 'Other_' + value.parameter[0]
                        inner_dict = {
                            'type': flag,
                            'num': loc[value.parameter[1]].index(value.parameter[2])
                        }
                    config[name] = inner_dict
                    
                elif name in ['Out_1', 'Out_2']:
                    if value.style in ['GR', 'LR']:
                        inner_dict = {
                            'type': value.style,
                            'num': int(value.parameter[0])
                        }
                    elif value.style == 'NR':
                        inner_dict = {
                            'type': 'NR'
                        }
                    config[name] = inner_dict
                    
                elif name == 'Out_3':
                    config[name] = False if value.style == '0' else True
                    
                elif name == 'Iteration':
                    inner_dict = {
                        'Iter_type': value.style,
                        'Iter_num': int(value.parameter[0])
                    }
                    if value.style == 'IMM':
                        inner_dict['Iter_Interval'] = int(value.parameter[1])
                    config[name] = inner_dict
                else:
                    print('ALU name Error !')
            
            else:
                #---------- set default value as ''(does not have the key) ----------#
                pass


        setattr(self, 'PE_config', config)
    
    name_list = ['In_1',
                 'In_2',
                 'In_3',
                 'In_4',
                 'Out_1',
                 'Out_2',
                 'Out_3',
                 'Iteration']
    
########################################
#             TEX_LSU Class            #
########################################
class TEX_LSU:
    def __init__(self, input_tuple, op_name):
        info = re.split(r'\s*,\s*', input_tuple[1:-1])
        config = {
            'OP_code': 'G' + op_name[1],
            'PE_type': 'LSU',
        }
        for name, value_str in zip(TEX_LSU.name_list, info):
            if value_str != '':
                value = Keyword(value_str)
                if name == 'Addr_mem':
                    if value.style == 'IMM':
                        inner_dict = {
                            'type': 'IMM_' + value.parameter[0],
                        }
                        config['Direct_addr_mem'] = int(value.parameter[1])
                    else:
                        if value.style in ['GR', 'LR']:
                            inner_dict = {
                                'type': value.style,
                                'num': int(value.parameter[0])
                            }
                        elif value.style == 'Self':
                            flag = 'My_PE_' + value.parameter[0]
                            inner_dict = { 'type': flag }
                        
                        elif value.style == 'Route':
                            loc = {
                                'LUC': ['r1', 'r2', 'r3', 'r7', 'd1', 'd2', 'd3', 'd7'],
                                'RUC': ['l1', 'l2', 'l3', 'l7', 'd1', 'd2', 'd3', 'd7'],
                                'LDC': ['r1', 'r2', 'r3', 'r7', 'u1', 'u2', 'u3', 'u7'],
                                'RDC': ['l1', 'l2', 'l3', 'l7', 'u1', 'u2', 'u3', 'u7'],
                                
                                'U': ['l', 'r', 'le', 're', 'd1', 'd2', 'd3', 'd7'],
                                'D': ['l', 'r', 'le', 're', 'u1', 'u2', 'u3', 'u7'],
                                'L': ['u', 'd', 'ue', 'de', 'r1', 'r2', 'r3', 'r7'],
                                'R': ['u', 'd', 'ue', 'de', 'l1', 'l2', 'l3', 'l7'],
                                
                                'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                                'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                                'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                                'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            }
                            flag = 'Other_PE_' + value.parameter[0]
                            inner_dict = {
                                'type': flag,
                                'num': loc[value.parameter[2]].index(value.parameter[3])
                            }
                    config[name] = inner_dict
                
                elif name == 'In_mem':
                    if value.style in ['GR', 'LR']:
                        inner_dict = {
                            'type': value.style,
                            'num': int(value.parameter[0])
                        }
                    elif value.style == 'Self':
                        flag = 'My_out' + value.parameter[0] + '_' + value.parameter[1]
                        inner_dict = { 'type': flag }
                    
                    elif value.style == 'Route':
                        loc = {
                            'LUC': ['r1', 'r2', 'r3', 'r7', 'd1', 'd2', 'd3', 'd7'],
                            'RUC': ['l1', 'l2', 'l3', 'l7', 'd1', 'd2', 'd3', 'd7'],
                            'LDC': ['r1', 'r2', 'r3', 'r7', 'u1', 'u2', 'u3', 'u7'],
                            'RDC': ['l1', 'l2', 'l3', 'l7', 'u1', 'u2', 'u3', 'u7'],
                            
                            'U': ['l', 'r', 'le', 're', 'd1', 'd2', 'd3', 'd7'],
                            'D': ['l', 'r', 'le', 're', 'u1', 'u2', 'u3', 'u7'],
                            'L': ['u', 'd', 'ue', 'de', 'r1', 'r2', 'r3', 'r7'],
                            'R': ['u', 'd', 'ue', 'de', 'l1', 'l2', 'l3', 'l7'],
                            
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                            'LU': ['u', 'd', 'l', 'r', 'ue', 'de', 'le', 're'],
                        }
                        flag = 'Other_out' + value.parameter[0] + '_' + value.parameter[1]
                        inner_dict = {
                            'type': flag,
                            'num': loc[value.parameter[2]].index(value.parameter[3])
                        }

                    config[name] = inner_dict
                    pass

                elif name == 'Offset':
                    config['Offset'] = int(value.style)

                elif name == 'Out_1':
                    if value.style in ['GR', 'LR']:
                        inner_dict = {
                            'type': value.style,
                            'num': int(value.parameter[0])
                        }
                    elif value.style == 'NR':
                        inner_dict = {
                            'type': 'NR'
                        }
                    config[name] = inner_dict

                elif name == 'Iteration':
                    # print('style: '+value.style)
                    # print('parameter: '+value.parameter[0])
                    inner_dict = {
                        'Iter_type': value.style,
                        'Iter_num': int(value.parameter[0])
                    }
                    if value.style == 'IMM':
                        inner_dict['Iter_Interval'] = int(value.parameter[1])
                    config[name] = inner_dict

                else: # R1 / R2 / R3 / R4
                    pass

        setattr(self, 'PE_config', config)
    
    name_list = ['Addr_mem',
                 'In_mem',
                 'Offset',
                 'Out_1',
                 'Iteration',
                 'R1',
                 'R2',
                 'R3',
                 'R4']

########################################
#           TEX_config Class           #
########################################
class TEX_config:
    # init static var
    Index_PE = 0
    Count = 0
    Iteration_line = 0
    Initial_idle = 0
    Iteration_PE = 0
    Iteration_PEA = 0
    Task_packageNum = 0
    Package_index = 0
    Bit_width = 32
    R1 = 0
    R2 = 0
    is_first_line = True

    # pointer
    PEA_TOP_i = {}
    PEA_TOP = []
    PE_i = {}

    def __init__(self, hand_line):
        # print('new line!!!')
        # print(TEX_config.PE_i)
        split_point = hand_line.find('(')
        self.op_name = hand_line[:split_point]
        self.op_info = hand_line[split_point:]

        # first line
        if self.op_name == '\\Top' and TEX_config.is_first_line:
            # print('get first TOP')
            TEX_config.is_first_line = False
            self.op = TEX_TOP(self.op_info)

            TEX_config.Index_PE = self.op.Index_PE
            TEX_config.Count = self.op.Count
            TEX_config.Iteration_line = self.op.Iteration_line
            TEX_config.Initial_idle = self.op.Initial_idle
            TEX_config.Iteration_PE = self.op.Iteration_PE
            TEX_config.Iteration_PEA = self.op.Iteration_PEA
            # print('FFFFirst!!  Task_packageNum is changed to: %d' % self.op.Task_packageNum)
            TEX_config.Task_packageNum = self.op.Task_packageNum
            # print('FFFFirst!!  Package_index is changed to: %d' % self.op.Package_index)
            TEX_config.Package_index = self.op.Package_index

            self.init_PEA_TOP_i()
            # print('first PEA_TOP')
            # print(TEX_config.PE_i)
            
        

        elif self.op_name == '\\Top':
            # TOP means this is a new PEA or PE
            # TOP info is just used to change the info
            # print('get NOT first TOP')
            self.op = TEX_TOP(self.op_info)

            TEX_config.Index_PE = self.op.Index_PE
            TEX_config.Count = self.op.Count
            TEX_config.Iteration_line = self.op.Iteration_line
            TEX_config.Initial_idle = self.op.Initial_idle
            TEX_config.Iteration_PE = self.op.Iteration_PE
            TEX_config.Iteration_PEA = self.op.Iteration_PEA
            # print('Task_packageNum is changed to: %d' % self.op.Task_packageNum)
            TEX_config.Task_packageNum = self.op.Task_packageNum

            if TEX_config.Package_index != self.op.Package_index:
                # new PEA
                TEX_config.PEA_TOP.append(TEX_config.PEA_TOP_i)
                # print('Package_index is changed to: %d' % self.op.Package_index)
                TEX_config.Package_index = self.op.Package_index
                self.init_PEA_TOP_i()
            else:
                # new PE
                self.new_PE_i()

        else:
            # ALU or LSU means this is a config info
            if self.op_name in TEX_config.ALU:
                # print('ALU')
                self.op = TEX_ALU(self.op_info, self.op_name)
            elif self.op_name in TEX_config.LSU:
                # print('LSU')
                # print(TEX_config.PE_i.keys())
                self.op = TEX_LSU(self.op_info, self.op_name)
            else:
                print('Input Error: Find unknown keywrods')
            
            # print(TEX_config.PE_i.keys())
            TEX_config.PE_i['PE_config'].append(self.op.PE_config)

    def init_PEA_TOP_i(self):
        # generate a new PEA_TOP_i
        # print('generate a new PEA_TOP')
        TEX_config.PEA_TOP_i = {
            "Iteration_PEA": TEX_config.Iteration_PEA,
            "Package_index": TEX_config.Package_index,
            "Task_packageNum": TEX_config.Task_packageNum,
            "PE": [{
                "Index_PE": TEX_config.Index_PE,
                "Iteration_PE": TEX_config.Iteration_PE,
                "Iteration_line": TEX_config.Iteration_line,
                "Initial_idle": TEX_config.Initial_idle,
                "PE_config": []
            }]
        }
        # add this PEA_TOP_i into PEA_TOP list
        TEX_config.PEA_TOP.append(TEX_config.PEA_TOP_i)
        # change the PE_i pointer
        TEX_config.PE_i = TEX_config.PEA_TOP[-1]['PE'][-1]
        # print(TEX_config.PE_i.keys())

    def new_PE_i(self):
        # generate a new PE
        # print('generate a new PE')
        new_PE = {
            "Index_PE": TEX_config.Index_PE,
            "Iteration_PE": TEX_config.Iteration_PE,
            "Iteration_line": TEX_config.Iteration_line,
            "Initial_idle": TEX_config.Initial_idle,
            "PE_config": []
        }
        # add this new_PE into PE list
        TEX_config.PEA_TOP_i["PE"].append(new_PE)
        # change the PE_i pointer
        TEX_config.PE_i = TEX_config.PEA_TOP_i['PE'][-1]
        # print(TEX_config.PE_i.keys())

    def output_js(self):
        return { 'PEA_TOP': TEX_config.PEA_TOP }

    ALU = ['\\Nop',
           '\\Rout',
           '\\Add',
           '\\Sub',
           '\\Uadd',
           '\\Usub',
           '\\And',
           '\\Or',
           '\\Xor',
           '\\Abs',
           '\\Sel',
           '\\Sll',
           '\\Srl',
           '\\Arl',
           '\\All',
           '\\Clz',
           '\\Mul',
           '\\Mac',
           '\\Umul',
           '\\Umac']

    LSU = ['\\Load',
           '\\Store']
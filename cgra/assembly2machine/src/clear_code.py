"""
Copyright  :  -- This confidential and proprietary software may be used
              -- only as authorized by a licensing agreement from
              -- Research Center for Mobile Computing (RCMC), Tsinghua University.
              -- (C) COPYRIGHT 2019 RCMC, Tsinghua University
              -- ALL RIGHTS RESERVED
              -- The entire notice above must be reproduced on all authorized copies.
File Name  :  makefile.py
Module Name:  assembly2machine
Author     :  Lie Luo
Version    :  1.0

Description:  config_code.txt -> code.txt
"""
# -*- coding: utf-8 -*-


# clear config
with open("./conf/config_code.bin", "r") as r_file, \
     open("./conf/code.bin", "w") as w_file:
    for line in r_file:
        if (len(line) != 1) and (line[0:2] != '//'):
            w_file.write(line.replace('_', ''))
"""
Copyright  :  -- This confidential and proprietary software may be used
              -- only as authorized by a licensing agreement from
              -- Research Center for Mobile Computing (RCMC), Tsinghua University.
              -- (C) COPYRIGHT 2019 RCMC, Tsinghua University
              -- ALL RIGHTS RESERVED
              -- The entire notice above must be reproduced on all authorized copies.
File Name  :  PE_class.py
Module Name:  assembly2machine
Author     :  Lie Luo
Version    :  1.0

Description:  the class used in json2config.py
"""

##################################
#          class define          #
##################################

# PE Config class
########################################
#           PE Config Class            #
########################################
class Config:
    def __init__(self, PE_config):
        for key, value in PE_config.items():
            setattr(self, key, value)

    ################  ALU code  ################
    def get_ALU_code(self):
        config_list = [('Config_extend', 1),   # 1:  [63]
                    ('Func', 2),               # 2:  [62:61]
                    ('In_1', 8),               # 8:  [60:53]
                    ('In_2', 8),               # 8:  [52:45]
                    ('In_3', 8),               # 8:  [44:37]
                    ('In_4', 6),               # 6:  [36:31]
                    ('Imm', 1),                # 1:  [30]
                    ('Out_1', 7),              # 7:  [29:23]
                    ('Out_2', 7),              # 7:  [22:16]
                    ('Out_3', 1),              # 1:  [15]
                    ('Iteration', 10),         # 10: [14:5]
                    ('OP_code', 5)]            # 5:  [4:0]
        # 'Config_extend'
        code = '0'
        # other keywords
        for config_name, width in config_list[1:]:

            code += '_'

            if config_name == 'Func':
                code += '01'

            elif (config_name == 'In_1') or (config_name == 'In_2') or (config_name == 'In_3'):
                is_hasattr = False
                is_imm = False
                if config_name == 'In_2':
                    if self.Imm['type']:
                        is_imm = True
                        num_b = bin(self.Imm['num'])
                        code += num_b[2:].zfill(width)
                    elif hasattr(self, 'In_2'):
                        conf = self.In_2
                        is_hasattr = True

                if (config_name == 'In_1') and hasattr(self, 'In_1'):
                    conf = self.In_1
                    is_hasattr = True
                elif (config_name == 'In_3') and hasattr(self, 'In_3'):
                    conf = self.In_3
                    is_hasattr = True

                if not is_imm:
                    if is_hasattr:
                        if conf['type'] == None:
                            code += ''.zfill(width)
                        elif conf['type'] == 'LR':
                            code += '000'
                            num_b = bin(conf['num'])
                            code += num_b[2:].zfill(width-3)
                        elif conf['type'] == 'GR':
                            code += '001'
                            num_b = bin(conf['num'])
                            code += num_b[2:].zfill(width-3)
                        elif conf['type'] == 'Other_out1_0':
                            code += '0100'
                            num_b = bin(conf['num'])
                            code += num_b[2:].zfill(width-4)
                        elif conf['type'] == 'Other_out1_1':
                            code += '0101'
                            num_b = bin(conf['num'])
                            code += num_b[2:].zfill(width-4)
                        elif conf['type'] == 'My_out1_0':
                            code += '01100'
                            code += ''.zfill(width-5)
                        elif conf['type'] == 'My_out1_1':
                            code += '01110'
                            code += ''.zfill(width-5)
                        elif conf['type'] == 'My_out2_0':
                            code += '01101'
                            code += ''.zfill(width-5)
                        elif conf['type'] == 'My_out2_1':
                            code += '01111'
                            code += ''.zfill(width-5)
                        elif conf['type'] == 'Other_out2_0':
                            code += '1000'
                            num_b = bin(conf['num'])
                            code += num_b[2:].zfill(width-4)
                        elif conf['type'] == 'Other_out2_1':
                            code += '1001'
                            num_b = bin(conf['num'])
                            code += num_b[2:].zfill(width-4)
                    else:
                        code += ''.zfill(width)

            elif config_name == 'In_4':
                if hasattr(self, 'In_4'):
                    conf = self.In_4

                    if conf['type'] == None:
                        code += ''.zfill(width)

                    elif conf['type'] == 'Other_0':
                        code += '10'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-2)

                    elif conf['type'] == 'Other_1':
                        code += '11'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-2)

                    elif conf['type'] == 'My_0':
                        code += '00'
                        code += ''.zfill(width-2)

                    elif conf['type'] == 'My_1':
                        code += '01'
                        code += ''.zfill(width-2)

                else:
                    code += ''.zfill(width)

            elif config_name == 'Imm':
                if self.Imm['type']:
                    code += '1'
                else:
                    code += '0'

            elif (config_name == 'Out_1') or (config_name == 'Out_2'):
                is_hasattr = False
                if (config_name == 'Out_1') and hasattr(self, 'Out_1'):
                    conf = self.Out_1
                    is_hasattr = True

                elif (config_name == 'Out_2') and hasattr(self, 'Out_2'):
                    conf = self.Out_2
                    is_hasattr = True

                if is_hasattr:
                    if conf['type'] == None:
                        code += ''.zfill(width)
                    elif conf['type'] == 'LR':
                        code += '00'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-2)
                    elif conf['type'] == 'GR':
                        code += '01'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-2)
                    elif conf['type'] == 'NR':
                        code += '10'
                        code += ''.zfill(width-2)
                else:
                    code += ''.zfill(width)

            elif config_name == 'Out_3':
                if hasattr(self, 'Out_3'):
                    if self.Out_3:
                        code += '1'
                    else:
                        code += '0'
                else:
                    code += '0'

            elif config_name == 'Iteration':

                if self.Iteration['Iter_type'] == 'IMM':
                    code += '00'

                    iter_num_b = bin(self.Iteration['Iter_num'])
                    code += iter_num_b[2:].zfill(width-5)

                    iter_ii_b = bin(self.Iteration['Iter_Interval'])
                    code += iter_ii_b[2:].zfill(3)

                elif self.Iteration['Iter_type'] == 'GR':
                    code += '01'

                    iter_num_b = bin(self.Iteration['Iter_num'])
                    code += iter_num_b[2:].zfill(width-5)

                    code += ''.zfill(3)

                elif self.Iteration['Iter_type'] == 'LR':
                    code += '10'

                    iter_num_b = bin(config.Iteration['Iter_num'])
                    code += iter_num_b[2:].zfill(width-5)

                    code += ''.zfill(3)

            elif config_name == 'OP_code':
                OP = ['Nop',
                      'Rout',
                      'Add',
                      'Sub',
                      'Uadd',
                      'Usub',
                      'And',
                      'Or',
                      'Xor',
                      'Abs',
                      'Sel',
                      'Sll',
                      'Srl',
                      'Arl',
                      'All',
                      'Clz',
                      'Mul',
                      'Mac',
                      'Umul',
                      'Umac']
                idx = OP.index(self.OP_code)
                idx_b = bin(idx)
                code += idx_b[2:].zfill(width)

            else:
                print(config_name)
                assert False, 'PE Config Name Error'

        return code

    ################  LSU code  ################
    def get_LSU_code(self):
        config_list = [('Config_extend', 1),      # 1:  [63]
                       ('Func', 2),               # 2:  [62:61]
                       ('Addr_mem', 8),           # 8:  [60:53]
                       ('Direct_addr_mem', 8),    # 8:  [52:45]
                       ('In_mem', 8),             # 8:  [44:37]
                       ('Offset', 4),             # 4:  [36:33]
                       ('R', 3),                  # 3:  [32:30]
                       ('Out_1', 7),              # 7:  [29:23]
                       ('R', 7),                  # 7:  [22:16]
                       ('R', 1),                  # 1:  [15]
                       ('Iteration', 10),         # 10: [14:5]
                       ('R', 3),                  # 3:  [4:2]
                       ('OP_code', 2)]            # 2:  [1:0]
        # 'Config_extend'
        code = '0'
        # other keywords
        for config_name, width in config_list[1:]:

            code += '_'

            if config_name == 'Func':
                code += '10'

            elif config_name == 'Addr_mem':

                if hasattr(self, 'Addr_mem'):
                    conf = self.Addr_mem

                    if conf['type'] == 'LR':
                        code += '000'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-3)
                    elif conf['type'] == 'GR':
                        code += '001'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-3)
                    elif conf['type'] == 'Other_PE_0':
                        code += '0100'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-4)
                    elif conf['type'] == 'Other_PE_1':
                        code += '0101'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-4)
                    elif conf['type'] == 'My_PE_0':
                        code += '0110'
                        code += ''.zfill(width-4)
                    elif conf['type'] == 'My_PE_1':
                        code += '0111'
                        code += ''.zfill(width-4)
                    elif conf['type'] == 'IMM_0':
                        code += '1000'
                        direct_num_b = bin(self.Direct_addr_mem)
                        direct_num_b_all = direct_num_b[2:].zfill(12)
                        # print(direct_num_b_all)
                        code += direct_num_b_all[-4:]
                        # print(code)
                    elif conf['type'] == 'IMM_1':
                        code += '1001'
                        direct_num_b = bin(self.Direct_addr_mem)
                        direct_num_b_all = direct_num_b[2:].zfill(12)
                        code += direct_num_b_all[-4:]
                else:
                    print('Do not find Addr_mem')
                    code += ''.zfill(width)

            elif config_name == 'Direct_addr_mem':
                if ((self.Addr_mem['type'] != 'IMM_0') or
                    (self.Addr_mem['type'] != 'IMM_1') or
                    (not hasattr(self, 'Direct_addr_mem'))):
                    code += ''.zfill(width)
                else:
                        direct_num_b = bin(self.Direct_addr_mem)
                        direct_num_b_all = direct_num_b[2:].zfill(12)
                        code += direct_num_b_all[:-4]

            elif config_name == 'In_mem':
                if hasattr(self, 'In_mem'):
                    conf = self.In_mem

                    if conf['type'] == None:
                        code += ''.zfill(width)
                    elif conf['type'] == 'LR':
                        code += '000'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-3)
                    elif conf['type'] == 'GR':
                        code += '001'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-3)
                    elif conf['type'] == 'Other_out1_0':
                        code += '0100'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-4)
                    elif conf['type'] == 'Other_out1_1':
                        code += '0101'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-4)
                    elif conf['type'] == 'My_out1_0':
                        code += '01100'
                        code += ''.zfill(width-5)

                    elif conf['type'] == 'My_out1_1':
                        code += '01110'
                        code += ''.zfill(width-5)

                    elif conf['type'] == 'My_out2_0':
                        code += '01101'
                        code += ''.zfill(width-5)

                    elif conf['type'] == 'My_out2_1':
                        code += '01111'
                        code += ''.zfill(width-5)

                    elif conf['type'] == 'Other_out2_0':
                        code += '1000'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-4)

                    elif conf['type'] == 'Other_out2_1':
                        code += '1001'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-4)
                else:
                    code += ''.zfill(width)

            elif config_name == 'Offset':
                if hasattr(self, 'Offset'):
                    offset_b = bin(self.Offset)
                    code += offset_b[2:].zfill(width)
                else:
                    code += ''.zfill(width)

            elif config_name == 'Out_1':
                if hasattr(self, 'Out_1'):
                    conf = self.Out_1

                    if conf['type'] == None:
                        code += ''.zfill(width)
                    elif conf['type'] == 'LR':
                        code += '00'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-2)
                    elif conf['type'] == 'GR':
                        code += '01'
                        num_b = bin(conf['num'])
                        code += num_b[2:].zfill(width-2)
                    elif conf['type'] == 'NR':
                        code += '10'
                        code += ''.zfill(width-2)
                else:
                    code += ''.zfill(width)

            elif config_name == 'Iteration':
                if self.Iteration['Iter_type'] == 'IMM':
                    code += '00'

                    iter_num_b = bin(self.Iteration['Iter_num'])
                    code += iter_num_b[2:].zfill(width-5)

                    iter_ii_b = bin(self.Iteration['Iter_Interval'])
                    code += iter_ii_b[2:].zfill(3)

                elif self.Iteration['Iter_type'] == 'GR':
                    code += '01'

                    iter_num_b = bin(self.Iteration['Iter_num'])
                    code += iter_num_b[2:].zfill(width-5)

                    iter_ii_b = bin(self.Iteration['Iter_Interval'])
                    code += iter_ii_b[2:].zfill(3)

                elif self.Iteration['Iter_type'] == 'LR':
                    code += '10'

                    iter_num_b = bin(self.Iteration['Iter_num'])
                    code += iter_num_b[2:].zfill(width-5)

                    iter_ii_b = bin(self.Iteration['Iter_Interval'])
                    code += iter_ii_b[2:].zfill(3)

            elif config_name == 'R':
                code += ''.zfill(width)

            elif config_name == 'OP_code':
                OP = ['GL',
                      'GS',
                      'LL',
                      'LS']
                idx = OP.index(self.OP_code)
                idx_b = bin(idx)
                code += idx_b[2:].zfill(width)

            else:
                print(config_name)
                assert False, 'PE Config Name Error'

        return code


########################################
#               PE Class               #
########################################
class PE:
    def __init__(self, PE_dict, Package_num, Package_index, Iteration_PEA=0):

        self.Iteration_PEA = Iteration_PEA
        self.Iteration_PE = 0
        self.Iteration_line = 0
        self.Initial_idle = 0
        self.Task_packageNum = Package_num
        self.Package_index = Package_index

        for key, value in PE_dict.items():
           setattr(self, key, value)

        try:
            self.num_config = len(self.PE_config)
            config_list = []
            for one_config in self.PE_config:
                conf = Config(one_config)
                config_list.append(conf)
        except AttributeError:
            print('Input PE need PE_config, but I do not find')
        else:
            self.PE_config = config_list

    def get_TOP_code(self):
        TOP_list = [('Config_extend', 1),      # 1:  [63]
                    ('Func', 2),               # 2:  [62:61]
                    ('Task_packageNum', 5),    # 5:  [60:56]
                    ('R', 3),                  # 3:  [55:53]
                    ('Package_index', 5),      # 5:  [52:48]
                    ('Bit_width', 3),          # 3:  [47:45]
                    ('Index_PE', 8),           # 8:  [44:37]
                    ('Iteration_PEA', 7),      # 7:  [36:30]
                    ('Iteration_PE', 7),       # 7:  [29:23]
                    ('Initial_idle', 6),       # 6:  [22:17]
                    ('Iteration_line', 2),     # 2:  [16:15]
                    ('R0', 3),                 # 3:  [14:12]
                    ('R', 6),                  # 6:  [11:6]
                    ('Count', 6)]              # 6:  [5:0]
        # 'Config_extend'
        TOP_code = '0'
        # other keywords
        for config_name, width in TOP_list[1:]:

            TOP_code += '_'

            if config_name == 'Func':
                TOP_code += '00'

            elif config_name == 'Task_packageNum':
                num_b = bin(self.Task_packageNum)
                TOP_code += num_b[2:].zfill(width)

            elif config_name == 'Package_index':
                num_b = bin(self.Package_index)
                TOP_code += num_b[2:].zfill(width)

            elif config_name == 'Bit_width':
                TOP_code += '000'

            elif config_name == 'Index_PE':
                id_b = bin(self.Index_PE)
                TOP_code += id_b[2:].zfill(width)

            elif config_name == 'Iteration_PEA':
                iter_PEA_b = bin(self.Iteration_PEA)
                TOP_code += iter_PEA_b[2:].zfill(width)

            elif config_name == 'Iteration_PE':
                # [7] in R0, [6:0] in here
                iter_PE_b = bin(self.Iteration_PE)
                iter_PE_code = iter_PE_b[2:].zfill(8)
                TOP_code += iter_PE_code[1:]

            elif config_name == 'Initial_idle':
                # [7:6] in R0, [5:0] in here
                init_idle_b = bin(self.Initial_idle)
                init_idel_code = init_idle_b[2:].zfill(8)
                TOP_code += init_idel_code[2:]

            elif config_name == 'Iteration_line':
                iter_line_b = bin(self.Iteration_line)
                TOP_code += iter_line_b[2:].zfill(width)

            elif config_name == 'Count':
                num_b = bin(self.num_config) # +1 -1
                TOP_code += num_b[2:].zfill(width)

            elif config_name == 'R0':
                iter_PE_b = bin(self.Iteration_PE)
                iter_PE_code = iter_PE_b[2:].zfill(8)

                init_idle_b = bin(self.Initial_idle)
                init_idel_code = init_idle_b[2:].zfill(8)

                TOP_code += init_idel_code[:2] + iter_PE_code[:1]

            elif config_name == 'R':
                TOP_code += ''.zfill(width)

            else:
                assert False, 'PE Config Name Error'

        return TOP_code

    def get_Config_code(self):
        code = ''

        for one_config in self.PE_config:
            if one_config.PE_type == 'LSU':
                code += one_config.get_LSU_code()
            else:
                code += one_config.get_ALU_code()

            code += '\n'
        return code

    def PE_config_2_machineCode(self):
        # TOP code
        TOP_code = self.get_TOP_code()

        # config code
        Config_code = self.get_Config_code()

        return TOP_code + '\n' + Config_code


########################################
#              PEA Class               #
########################################
class PEA:
    def __init__(self, PEA_TOP):

        self.Iteration_PEA = 0

        for key, value in PEA_TOP.items():
           setattr(self, key, value)

        try:
            self.num_PE = len(self.PE)
            PE_list = []
            for one_PE in self.PE:
                PE_i = PE(one_PE, self.Task_packageNum, self.Package_index, self.Iteration_PEA)
                PE_list.append(PE_i)
        except AttributeError:
            print('Input PEA need PE, but I do not find it')
        else:
            self.PE = PE_list

    def PEA_config_2_machineCode_print(self):
        print('//Package[{}]\n'.format(self.Package_index))
        for PE_i in self.PE:
            print('//PE[{}]:'.format(PE_i.Index_PE))
            print(PE_i.PE_config_2_machineCode())
            print('\n')

    def PEA_config_2_machineCode(self, file):
        file.write('//Package[{}]\n'.format(self.Package_index))
        for PE_i in self.PE:
            file.write('//PE[{}]:\n'.format(PE_i.Index_PE))
            file.write(PE_i.PE_config_2_machineCode())
            file.write('\n')
"""
Copyright  :  -- This confidential and proprietary software may be used
              -- only as authorized by a licensing agreement from
              -- Research Center for Mobile Computing (RCMC), Tsinghua University.
              -- (C) COPYRIGHT 2019 RCMC, Tsinghua University
              -- ALL RIGHTS RESERVED
              -- The entire notice above must be reproduced on all authorized copies.
File Name  :  makefile.py
Module Name:  assembly2machine
Author     :  Lie Luo
Version    :  1.0

Description:  used to integrate all the assembly2machine code
"""

import os
import shutil
import sys

input_path = sys.argv[1]
output_path = sys.argv[2]

current_path = sys.argv[3]
print(current_path)
#os.system('cd /home/hujb/source')
os.chdir(''+current_path)
if os.path.exists('./input.txt'):
    os.remove('./input.txt')

shutil.copy(input_path, './input.txt')

os.system('python ./src/txt2json.py')
os.system("python ./src/json2config.py")
os.system("python ./src/clear_code.py")

if os.path.exists('./code.bin'):
    os.remove('./code.bin')

shutil.copy('./conf/code.bin', './')

if os.path.exists(output_path):
    os.remove(output_path)
    
shutil.copy('./conf/code.bin', output_path)

#!/usr/bin/python
import json
import pygraphviz as pyg
from tool import relative_location, init_CGRA
#open file & parse
import os
from  sys import argv
os.chdir(argv[1])
f = open('mapper1.json','r')
mapper = json.load(f)
f = open('loop1.json','r')
loop_info = json.load(f)
total_iter = (loop_info['loop_end'] - loop_info['loop_begin'])/loop_info['loop_offset']
total_iter = int(total_iter)
g = pyg.AGraph('add1.dot')

#init
context = ''
CGRA = init_CGRA(8,8)
in_degree = {}
index = 0
for node in g.nodes():
	in_degree[node] = g.in_degree()[index]
	index += 1

node_num = {}
index = 0
for node in g.nodes():
	node_num[node] = index
	index += 1

map_node = {}
for item in mapper['map_node']:
	map_node[item['name']] = item['node']

node_to_reg = {}
for item in mapper['map_reg']:
	node_name = mapper['map_node'][item['node_src']]['name']
	node_to_reg[node_name] = item['id']


#bfs
readylist = []
for node,indegree in in_degree.items():
	if not indegree:
		readylist.append(node)

idle = {}
address = 0
while len(readylist):
	top_para = [0,1,1,0,0,0,0,0,32,0,0]
	alu_para = ['','','','','NR','NR','0','IMM_0_5']
	lsu_para = ['','','0','NR','IMM_0_0','','','','']
	cur_node = readylist.pop()
	for node in g.successors(cur_node):
		in_degree[node] -= 1
		if not in_degree[node]:
			readylist.append(node)
	#write context

	#top context
	top_para[0] = map_node[cur_node] #index_pe

	if cur_node.attr['iterator'] == '1':
		top_para[1] = 1 #count

	max_idle = 0
	for parent in g.predecessors(cur_node):
		delay = idle[parent]
		if parent in node_to_reg and not CGRA[map_node[parent]][map_node[cur_node]]:
			delay += 1
		max_idle = max(max_idle, delay)
	top_para[3] = max_idle #TODO:initial_idle, need to be verified.
	idle[cur_node] = max_idle
	if cur_node.attr['opcode'] in ['mac','mul']:
		idle[cur_node] += 3
		alu_para[-1] = 'IMM_0_4'
	elif cur_node.attr['opcode'] == 'load':
		idle[cur_node] += 7
	else:
		idle[cur_node] += 2
		lsu_para[4] = 'IMM_0_5'

	top_para[4] = total_iter - 1 #TODO: Iteration_PE, how about total_iter > 255?
	#TODO: put then into reg.
	print('\\Top(' + str(top_para)[1:-1] + ')')

	#detailed context
	if cur_node.attr['opcode'] in ['load', 'store']:
		#LSU
		for parent in g.predecessors(cur_node):
			edge = g.get_edge(parent, cur_node)
			operand = int(edge.attr['operand'])
			if parent in node_to_reg and not CGRA[map_node[parent]][map_node[cur_node]]:
				lsu_para[operand] = 'GR_' + str(node_to_reg[parent])
			else:
				lsu_para[operand] = 'Route_1_0' + relative_location(map_node[cur_node], map_node[parent])
		#TODO: Test
		lsu_para[0] = 'IMM_0_' + str(address)
		address += 256
		lsu_para[2] = str(loop_info['loop_offset'])
		if cur_node in node_to_reg:
			lsu_para[3] = 'GR_' + str(node_to_reg[cur_node])
		print('\t\\' + cur_node.attr['opcode'].capitalize() 
				+ '(' + ', '.join(lsu_para) + ')')
	else:
		#ALU
		#TODO: iterator's context
		for parent in g.predecessors(cur_node):
			edge = g.get_edge(parent, cur_node)
			operand = int(edge.attr['operand'])
			if parent in node_to_reg and not CGRA[map_node[parent]][map_node[cur_node]]:
				lsu_para[operand] = 'GR_' + str(node_to_reg[parent])
			else:
				alu_para[operand] = 'Route_1_0' + relative_location(map_node[cur_node], map_node[parent])
		if cur_node.attr['imm']:
			alu_para[1] = 'IMM_' + cur_node.attr['imm']
		if cur_node in node_to_reg:
			alu_para[4] = 'GR_' + str(node_to_reg[cur_node])
		print('\t\\' + cur_node.attr['opcode'].capitalize() 
				+ '(' + ', '.join(alu_para) + ')')

import pydot


def dfg_to_admatrix(filename):
    graph = pydot.graph_from_dot_file(filename)[0]
    nodelist = graph.get_nodes()
    l = len(nodelist)
    edgelist = graph.get_edges()
    admatrix = [[0 for i in range(l)] for j in range(l)]
    for edge in edgelist:
        points = edge.obj_dict['points']
        source = graph.get_node(points[0])[0].get_sequence() - 1
        dst = graph.get_node(points[1])[0].get_sequence() - 1
        admatrix[source][dst] = 1
    LS = [0 for i in range(l)]
    for node in nodelist:
        nodename = node.get_name()
        if nodename[:5] == 'store' or nodename[:4] == 'load':
            LS[node.get_sequence() - 1] = 1
    return admatrix, LS


def init_CGRA(n, m):  # n rows, m cols
    CGRA = [[0 for i in range(n * m)] for j in range(n * m)]
    for row in range(n):
        for col in range(m):
            i = row * n + col

            if row < n / 2:  # up
                CGRA[i][col] = 1
                CGRA[col][i] = 1
            else:  # down
                CGRA[i][(n - 1) * m + col] = 1
                CGRA[(n - 1) * m + col][i] = 1

            if col < n / 2:  # left
                CGRA[i][row * n] = 1
                CGRA[row * n][i] = 1
            else:  # right
                CGRA[i][row * n + m - 1] = 1
                CGRA[row * n + m - 1][i] = 1

            CGRA[i][(row - 1) % n * n + col] = 1
            CGRA[i][(row + 1) % n * n + col] = 1
            CGRA[i][row * n + (col - 1) % m] = 1
            CGRA[i][row * n + (col + 1) % m] = 1

            CGRA[i][i] = 0

    return CGRA


def init_AM(n, m):
    AM = [0 for i in range(n * m)]
    for i in range(n):
        AM[i * m] = 1
        AM[i * m + n - 1] = 1
    for j in range(m):
        AM[j] = 1
        AM[(n - 1) * m + j] = 1
    return AM


# src and dst are both int.
# src and dst must be interconnected! If not, return undefined result.
def relative_location(src, dst, col=8, row=8):
    loc = ''
    ornt = ''
    src_row = int(src / col)
    src_col = src % col
    dst_row = int(dst / col)
    dst_col = dst % col
    row_distance = dst_row - src_row
    col_distance = dst_col - src_col

    def row_relative():
        if dst_col == 0:
            return 'le'
        elif dst_col == 7:
            return 're'
        elif col_distance == 1:
            return 'r'
        elif col_distance == -1:
            return 'l'
        return 'ERROR'

    def col_relative():
        if dst_row == 0:
            return 'ue'
        elif dst_row == 7:
            return 'de'
        elif row_distance == 1:
            return 'd'
        elif row_distance == -1:
            return 'u'
        return 'ERROR'

    if src == 0:
        loc = 'LUC'
        if dst_row == src_row:
            ornt = 'r' + str(abs(col_distance))
        else:
            ornt = 'd' + str(abs(row_distance))
    elif src == 7:
        loc = 'RUC'
        if dst_row == src_row:
            ornt = 'l' + str(abs(col_distance))
        else:
            ornt = 'd' + str(abs(row_distance))
    elif src == 56:
        loc = 'LDC'
        if dst_row == src_row:
            ornt = 'r' + str(abs(col_distance))
        else:
            ornt = 'u' + str(abs(row_distance))
    elif src == 63:
        loc = 'RDC'
        if dst_row == src_row:
            ornt = 'l' + str(abs(col_distance))
        else:
            ornt = 'u' + str(abs(row_distance))
    elif src_row == 0:
        loc = 'U'
        if dst_row == src_row:
            ornt = row_relative()
        else:
            ornt = 'd' + str(abs(row_distance))
    elif src_row == 7:
        loc = 'D'
        if dst_row == src_row:
            ornt = row_relative()
        else:
            ornt = 'u' + str(abs(row_distance))
    elif src_col == 0:
        loc = 'L'
        if dst_row == src_row:
            ornt = 'r' + str(abs(col_distance))
        else:
            ornt = col_relative()
    elif src_col == 7:
        loc = 'R'
        if dst_row == src_row:
            ornt = 'l' + str(abs(col_distance))
        else:
            ornt = col_relative()
    elif src_col < col / 2 and src_row < row / 2:
        loc = 'LU'
        if dst_row == src_row:
            ornt = row_relative()
        else:
            ornt = col_relative()
    elif src_col >= col / 2 and src_row < row / 2:
        loc = 'RU'
        if dst_row == src_row:
            ornt = row_relative()
        else:
            ornt = col_relative()
    elif src_col < col / 2 and src_row >= row / 2:
        loc = 'LD'
        if dst_row == src_row:
            ornt = row_relative()
        else:
            ornt = col_relative()
    elif src_col >= col / 2 and src_row >= row / 2:
        loc = 'RD'
        if dst_row == src_row:
            ornt = row_relative()
        else:
            ornt = col_relative()

    return '_' + loc + '_' + ornt


#!/usr/bin/python

from gurobipy import *
from tool import *
import sys
#---------------------------------
#AccessMem = init_AM(2,2)
#CGRA = init_CGRA(2,2)
AccessMem = init_AM(8,8)
CGRA = init_CGRA(8,8)

DFG = ( (0,1,1),
        (0,0,0),
        (0,0,0),)
LS = [1,0,0] # load/store operation
DFG, LS = dfg_to_admatrix(sys.argv[1])
N = len(LS) # N nodes in DFG
M = len(AccessMem) # M PEs
K = 32 # K global register

#---------------------------------

# Create a new model
m = Model("CGRA mapping")

# Create variables
F = m.addVars(N, M, vtype=GRB.BINARY, name='F')
R = m.addVars(K, N, vtype=GRB.BINARY, name='R')

# Set objective
m.setObjective(1, GRB.MINIMIZE)
#m.setObjective(sum([R[n,i] for i in range(N) for n in range(K)]), GRB.MINIMIZE)


# Add constraint: 
m.addConstrs((sum([F[i,j] for j in range(M)]) == 1\
            for i in range(N)),
            name = 'Map')

m.addConstrs((sum([F[i,j] for i in range(N)]) <= 1\
            for j in range(M)),
            name = 'PE')

m.addConstrs((sum([F[i,a]*F[j,b] for a in range(M) for b in range(M) if CGRA[a][b]]) + \
                    sum([R[n,i] for n in range(K)]) >= 1\
            for i in range(N) for j in range(N) if DFG[i][j]), 
            name='Interconnection') 


m.addConstrs((sum([AccessMem[j]*F[i,j] for j in range(M)]) == 1\
            for i in range(N) if LS[i]==1), 
            name='Mem')

m.addConstrs((sum([R[n,i] for i in range(N)]) <= 1\
            for n in range(K)), 
            name='Register to Node') 

m.addConstrs((sum([R[n,i] for n in range(K)]) <= 1\
            for i in range(N)), 
            name='Node to Register') 

m.addConstr(sum([R[n,i] for i in range(N) for n in range(K)]) <= K,
            name='Register Count') 

# Optimize model
m.optimize()


for v in m.getVars():
    if v.x:
        print('%s' % (v.VarName.replace('[', ',')[:-1]))

print('Obj: %g' % m.getObjective().getValue())

